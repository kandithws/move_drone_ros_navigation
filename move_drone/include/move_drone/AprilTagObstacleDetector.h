//
// Created by kandithws on 28/5/2561.
//

#ifndef MOVE_DRONE_APRILTAGDETECTOR_H
#define MOVE_DRONE_APRILTAGDETECTOR_H


#include <Eigen/Eigen>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <Eigen/Geometry>
#include <move_drone/utils/print_utils.h>
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag16h5.h>
#include <AprilTags/Tag25h7.h>
#include <AprilTags/Tag25h9.h>
#include <AprilTags/Tag36h9.h>
#include <AprilTags/Tag36h11.h>
#include <boost/make_shared.hpp>
#include <move_drone/CameraInfo.h>
#include <chrono>

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

// Note use ROS visualization_msgs Type CUBE to publish
namespace move_drone {
  class AprilTagObstacle {
   public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef boost::shared_ptr<AprilTagObstacle> Ptr;
    AprilTagObstacle();
    AprilTagObstacle(int tag_id, double tag_size, double sx, double sy, double sz);
    //Eigen::Matrix4d pose_; // originate at (x/2, y/2, z/2) of object, wrt camera_optical_frame (z-axis front)
    Eigen::Affine3d pose_; // originate at (x/2, y/2, z/2) of object, wrt camera_optical_frame (z-axis front)
    int tag_id_;
    double tag_size_;
    // Bounding box size
    double size_x_ = 0.0;
    double size_y_ = 0.0;
    double size_z_ = 0.0;

    // void getPointCloudBBox(double cloud_resolution = 0.25); // local method, should use at AprilTagObjectDetector
  };

  class AprilTagObstacleDetector{

   public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    AprilTagObstacleDetector(std::string config_file); // init cache pointcloud here
    ~AprilTagObstacleDetector();

    void setCameraParams(CameraInfo cam_info); // Projection Matrix
    void setCameraParams(double fx, double fy, double px, double py); // Projected focal length and principle point

    void detect(cv::Mat& img, std::vector<AprilTagObstacle>& out_obs, bool draw_output=false);

    // Should be our future unified interface for All object Detector
    void detect(cv::Mat& img, PointCloudT& out_cloud);

    // For debugging purpose
    void getObstaclesCacheCloud(int tag_id, PointCloudT &out_cloud);
    // TODO: 1. createBBox cloud around its origin (should be precache at AprilTagObjectDetector)
    //       2. Transform PointCloud (using PCL)

   private:
    double cloud_granularity_ = 0.05;
    std::string tag_family_;
    std::map<int, PointCloudT::Ptr> obs_cloud_cache_;
    std::map<int, AprilTagObstacle::Ptr> obs_config_; // ignoring pose
    boost::shared_ptr<AprilTags::TagDetector> tag_detector_;
    bool camera_params_init_ = false;
    double fx_;
    double fy_;
    double px_;
    double py_;

    void parseConfig(std::string config);
    void cacheObstaclesCloud(double cloud_granularity = 0.1);

  };
}

#endif //MOVE_DRONE_ROS_APRILTAGDETECTOR_H
