//
// Created by kandithws on 23/7/2561.
//

#ifndef PROJECT_BASECAMERAMANAGER_H
#define PROJECT_BASECAMERAMANAGER_H

#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>
#include <chrono>
#include <move_drone/utils/time_conversion.h>
#include <boost/algorithm/string.hpp>
#include <move_drone/utils/print_utils.h>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>

#include <move_drone/CameraInfo.h>

namespace move_drone {

class ImageStamped {
 public:
  typedef boost::shared_ptr<ImageStamped> Ptr;
  typedef boost::shared_ptr<const ImageStamped> ConstPtr;

  enum Encoding{
    BGR8,
    RGB8,
    MONO8
  };

  static Encoding encodingFromString(std::string encoding_str);

  cv::Mat image_;
  std::chrono::nanoseconds stamp_;
  //std::string encoding_ = "bgr8";
  Encoding encoding_ = Encoding::BGR8;
  ImageStamped();
  ImageStamped(cv::Mat img, Encoding encoding = Encoding::BGR8);
  ImageStamped(cv::Mat img, std::chrono::nanoseconds stamp, Encoding encoding = Encoding::BGR8);
  void setTimeNow();
  void convertToEncoding(const Encoding& encoding);

  std::string getStringEncoding() const;


  // TODO : create a util function to do encoding conversion (instead of using camera manager)
};

/*
 * class BaseCameraManager is an abstract class for libmovedrone to interface with various framework, i.e.
 * real camera/ simulation
 * */

class BaseCameraManager {
 public:
  CameraInfo camera_params_; // Lazy to implement getter setter, put as public instead
  void setImageQueueSize(size_t queue_size);
  void clearImageQueue();
  size_t getImageQueueSize(size_t queue_size);
  virtual void loadCameraInfo(std::string filename);
  bool getImage(ImageStamped::Ptr& img_stamped_ptr);
  void registerCallback(const std::function<void (const ImageStamped::ConstPtr&)>& cb);
  void startStreaming();
  void stopStreaming();
  ~BaseCameraManager();

 protected:
  //bool convert_to_rgb_ = false; // convert image from bgr8 encoding to rgb8 encoding
  BaseCameraManager(size_t queue_size);
  ImageStamped::Encoding target_encoding_;
  int image_width_ = 640;
  int image_height_ = 480;
  bool is_shutdown_= true;

  std::function<void (const ImageStamped::ConstPtr&)> image_cb_;
  boost::shared_ptr<boost::thread> image_cb_dispatch_thread_;
  boost::circular_buffer<ImageStamped::Ptr> img_queue_;
  boost::mutex img_queue_mutex_;
  boost::condition_variable img_queue_cv_;

  virtual void initStartStreaming() = 0;
  virtual void initStopStreaming() = 0; // This should join all neccesary threads
  // All derived classes must call this in order to push into a queue.
  void pushImageQueue(ImageStamped::Ptr& frame_stamped);
  void imageCallbackDispatchThread();
};
}

#endif //PROJECT_BASECAMERAMANAGER_H
