//
// Created by kandithws on 20/7/2561.
//

#ifndef PROJECT_BASEMAVDATAINTERFACE_H
#define PROJECT_BASEMAVDATAINTERFACE_H

#include <mavconn/interface.h>
#include <Eigen/Eigen>
#include <Eigen/Geometry>
#include <functional>
#include <chrono>
#include <thread>


namespace move_drone {
  namespace mavlink_msgs = mavlink::common::msg;
  class BaseMAVDataInterface{
   public:
    /*Human readable statuses*/
    typedef struct {
      uint8_t frame_type;
      uint8_t autopilot_type;
      bool arm; // Arm or Disarm : MAV_AUTOPILOT ENUM
      bool in_hil;
      bool manual_remote_enabled;
      bool guided_enable;
      bool auto_enable;
      std::string system_status; // MAV_STATE ENUM
      uint8_t mav_version;
    }mav_heartbeat_status;

    typedef struct {
      float batt_voltage;
      float batt_percent;
      float load_percent;
      // TODO -- Get Other sensors statuses as string
    }mav_system_status;

   public:
    void registerHeartBeatCallback(const std::function<void (const mav_heartbeat_status&,
                                                             const std::chrono::nanoseconds&)>& cb);
    void registerSystemStatusCallback(const std::function<void (const mav_system_status&,
                                                                const std::chrono::nanoseconds&)>& cb);
    void registerLocalPositionNEDCallback(const std::function<void (const mavlink_msgs::LOCAL_POSITION_NED&,
                                                                    const std::chrono::nanoseconds&)>& cb);
    void registerGlobalPositionINTCallback(const std::function<void (const mavlink_msgs::GLOBAL_POSITION_INT&,
                                                                     const std::chrono::nanoseconds&)>& cb);
    void registerAttituteCallback(const std::function<void (const mavlink_msgs::ATTITUDE&,
                                                            const std::chrono::nanoseconds&)>& cb);
    void registerHighResIMUCallback(const std::function<void (const mavlink_msgs::HIGHRES_IMU& ,
                                                              const std::chrono::nanoseconds& )>& cb);

    void registerPoseCallback(const std::function<void (const Eigen::Vector3d& ,
                                                        const Eigen::Quaterniond&,
                                                        const std::chrono::nanoseconds& )>& cb);

    void registerVelocityCallback(const std::function<void (const Eigen::Vector3d& ,
                                                            const Eigen::Vector3d&,
                                                            const std::chrono::nanoseconds& )>& cb);

    // pose vel callback, synchronize using altitude msg and Local position ned msg based on Local position ned stamp
    // @param out_local_position, out_local_orientation, out_linear_vel, out_angular_vel
    void registerPoseVelCallback(const std::function<void (const Eigen::Vector3d& ,
                                                           const Eigen::Quaterniond&,
                                                           const Eigen::Vector3d&,
                                                           const Eigen::Vector3d&,
                                                           const std::chrono::nanoseconds& )>& cb);


    // Keep for backward compatibility
    virtual bool getRobotFrameTransform(Eigen::Vector3d &position, Eigen::Quaterniond &orientation){}
    virtual bool getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular){}
    virtual bool getRobotFrameTransform(Eigen::Vector3d &position, Eigen::Quaterniond &orientation, std::chrono::nanoseconds& stamp){}
    virtual bool getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular, std::chrono::nanoseconds& stamp){}

   protected:

    /*---- User callback interface----*/
    std::function<void (const mav_heartbeat_status&, const std::chrono::nanoseconds&)> heart_beat_cb_;
    std::function<void (const mav_system_status&, const std::chrono::nanoseconds&)> sys_status_cb_;
    std::function<void (const mavlink_msgs::LOCAL_POSITION_NED&, const std::chrono::nanoseconds&)> local_position_cb_;
    std::function<void (const mavlink_msgs::GLOBAL_POSITION_INT&, const std::chrono::nanoseconds&)> global_position_cb_;
    std::function<void (const mavlink_msgs::ATTITUDE&, const std::chrono::nanoseconds&)> attitute_cb_;
    std::function<void (const mavlink_msgs::HIGHRES_IMU& , const std::chrono::nanoseconds& )> imu_cb_;
    std::function<void (const Eigen::Vector3d& , const Eigen::Quaterniond&, const std::chrono::nanoseconds& )> pose_cb_;
    std::function<void (const Eigen::Vector3d& , const Eigen::Vector3d&, const std::chrono::nanoseconds& )> vel_cb_;
    std::function<void (const Eigen::Vector3d& , const Eigen::Quaterniond&,
                        const Eigen::Vector3d& , const Eigen::Vector3d&, const std::chrono::nanoseconds& )> pose_vel_cb_;
  };
}

#endif //PROJECT_BASEMAVDATAINTERFACE_H
