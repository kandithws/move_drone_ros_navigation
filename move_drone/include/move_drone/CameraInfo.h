#ifndef MOVE_DRONE_CAMERA_INFO_H
#define MOVE_DRONE_CAMERA_INFO_H

#include <opencv2/opencv.hpp>
#include <string>
namespace move_drone{


class CameraInfo {
 public:
  // path to ROS's style yaml file
  CameraInfo(std::string filename);
  CameraInfo();
  cv::Mat K;
  cv::Mat P;
  cv::Mat R;
  cv::Mat D;
  int width;
  int height;
  std::string camera_name;
  std::string distortion_model;
  bool undistort = false;
  std::string encoding; // "bgr8" or "rgb8"

  double getProjectedfx() const;
  double getProjectedfy() const;
  double getProjectedpx() const;
  double getProjectedpy() const;
  void unDistortImage(const cv::Mat& in, cv::Mat& out);
  void print();

 private:
  void printMat(cv::Mat mat);

};

}

#endif