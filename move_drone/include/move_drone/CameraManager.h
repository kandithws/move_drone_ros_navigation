//
// Created by kandithws on 5/4/2561.
//

#ifndef MOVE_DRONE_CAMERAMANAGER_H
#define MOVE_DRONE_CAMERAMANAGER_H


#include <move_drone/BaseCameraManager.h>

// #define CAMERAMANAGER_VERBOSE_FLUSH
namespace move_drone{

/*
 *  class CameraManager manages images stream based on OpenCV's VideoCapture
 *
 *
 *  For now CameraManager Support Only SingleWriter SingleReader;
 *
 *  TODO:
 *    Make this extensible to Multiple Readers (i.e. 1 visualization, 2. SLAM, 3. Obstacle detection etc)
 *    Ideas Note:
 *    1. Add number of readers that user want to have, and create multiple image queues (channels) with that number
 *    2. when read the image, need to broadcast (push) image pointers to every image queues (channels)
 *    3. for each reader will get Image based on his/her index
 *    This way will optimize the memory since we used a shared pointers and this image will automatically destory when no one using it
 *
 * */

class CameraManager : public BaseCameraManager {
 public:
  /*
   * @params device:
   * if video: "video::/path/to/video/file"
   * if camera: "camera::/dev/video*"
   *
   * */
  CameraManager(std::string device, int queue_size = 1, double fps=24.0);

 private:
  boost::shared_ptr<boost::thread> camera_thread_;
  boost::shared_ptr<cv::VideoCapture> cap_;
  bool device_type_video_=false; // false = camera
  std::string device_path_ = "/dev/video0";
  double fps_rate_;

  void initStartStreaming();
  void initStopStreaming();
  void cameraThread();
  int normalizedVideoDevice(std::string device_path);
};
}

#endif //MOVE_DRONE_ROS_CAMERAMANAGER_H
