//
// Created by kandithws on 19/2/2561.
//

#ifndef MOVE_DRONE_MAV_CONTROLLER_H
#define MOVE_DRONE_MAV_CONTROLLER_H


#include <mavconn/interface.h>
#include <move_drone/BaseMAVDataInterface.h>
#include "utils/print_utils.h"
#include "utils/timer.h"
#include "utils/time_conversion.h"
#include <cmath>
#include <sys/time.h>
#include <thread>
#include "frame_tf_utils.h"
#include <boost/circular_buffer.hpp>
#include <mutex>
#include <condition_variable>
#include <queue>




// This controller controls 1 robot only
// It will keep updating for the robot statuses,


namespace move_drone{


namespace mavlink_msgs = mavlink::common::msg;
/*
 * TODO -- Fix callback thread for sensitive data (i.e. IMU)
 *
 * */
class MAVController : public BaseMAVDataInterface {

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef std::shared_ptr<MAVController> Ptr;

//  typedef struct {
//    float percent;
//  }mav_battery_status;

  MAVController(std::string port_url="/dev/ttyACM0:115200");
  ~MAVController();
  void setPoseVelUpdateRate(double update_rate){ pose_vel_update_rate_ = update_rate; }
  void startController(); // Start poll thread for parameters/statuses


  /*
  std::function<void (Eigen::Vector3d position, Eigen::Quaterniond orientation,
                      Eigen::Vector3d vel_linear, Eigen::Vector3d vel_angular)> poseVelDataCallback;
  */

  /* User Command */

  void commandLocalPositionENU(double x, double y, double z, double yaw=NAN);
  void commandLocalPositionENU(const Eigen::Vector3d& position);
  void commandLocalPositionENU(const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation);
  //void commandRelativePosition(double dx, double dy, double dz, double dyaw, bool stop_and_turn=false);
  void commandVelocity(double vx, double vy, double vz, double vyaw=NAN);

  // Both for position and velocity control, should not be set during the flight
  void commandSetMaxXYVelocity(double limit);
  void commandResetMaxXYVelocity();

  void commandBreak();

  // Will block until landed
  bool commandLandAtCurrentLocation(double desired_speed= 0.3, double land_speed_bound=0.05,
                                    double land_check_duration=5.0, double timeout=90.0, bool auto_disarm=true );

  void commandReturnToLaunch();
  void commandTakeOff(double desired_altitude= 5.0);
  // For moving command, user may start the command thread manually
  bool startCommand(); // Need to start command before setting local position ned
  void stopCommand();

  bool commandArm(bool arm);
  bool isArmed();
  bool isAutoGuidedModeEnable();
  bool isCommandEnable();

  void setCurrentHomePosition(); // For return to launch, TODO -- Call this at constructor

  /* User Data */
  // Transform base_link -> initial frame (ENU)
  /*
   *  // Alternative method to get pose/vel data
   * */

  bool getRobotFrameTransform(Eigen::Vector3d &position, Eigen::Quaterniond &orientation);
  bool getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular);
  bool getRobotFrameTransform(Eigen::Vector3d &position, Eigen::Quaterniond &orientation, std::chrono::nanoseconds& stamp);
  bool getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular, std::chrono::nanoseconds& stamp);

  // Adhoc interface to clean up the queue
  void getIMUMessagesUpToStamp(std::vector< std::shared_ptr<mavlink_msgs::HIGHRES_IMU> >& imu_msgs ,
                               std::vector< std::shared_ptr<std::chrono::nanoseconds> >& stamp_msgs,
  std::chrono::nanoseconds stamp_limit);

 private:
  mavconn::MAVConnInterface::Ptr connect_;
  void msgCallBack(const mavlink::mavlink_message_t *message, const mavconn::Framing framing);
  bool is_init_ = false;
  bool is_local_pose_init_ = false;
  bool is_shutdown_ = false;
  std::shared_ptr<std::thread> pose_vel_sync_thread_;
  std::shared_ptr<std::thread> cmd_thread_;
  //point3d local_init_loc_;
  uint8_t sys_id_;
  uint8_t autopilot_id_;
  std::string port_url_;

  mav_heartbeat_status hb_st_;


  // Latest Command local ned buffer, we need to maintaining command loop at rate 2 hz
  bool cmd_enable_ = false;
  double cmd_rate_ = 10.0; // Hz, minimum 2hz
  std::mutex cmd_local_ned_mutex_;
  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED cmd_local_ned_;

  double pose_vel_update_rate_ = 50.0;
  std::mutex pose_vel_ned_mutex_;
  Eigen::Vector3d position_ned_;
  std::chrono::nanoseconds position_ned_msg_stamp_;
  std::chrono::nanoseconds attitute_msg_stamp_;
  Eigen::Quaterniond orientation_vehicle_ned_; // by mavlink is a vehicle carry ned

  Eigen::Vector3d linear_vel_ned_;
  Eigen::Vector3d angular_vel_vehicle_ned_; // by mavlink is a vehicle carry ned
  bool local_ned_update_ = false;
  bool attitude_ned_update_ = false;

  // Robot Frame (X front, Y left, Z up : ROS base_link)
  bool robot_data_ready_ = false;
  std::mutex robot_tf_mutex_;
  //Eigen::Transform pose_robot_;
  Eigen::Vector3d position_robot_;
  Eigen::Quaterniond orientation_robot_;
  std::chrono::nanoseconds tf_robot_stamp_;
  std::mutex robot_vel_mutex_;
  Eigen::Vector3d vel_linear_robot_;
  Eigen::Vector3d vel_angular_robot_;
  std::chrono::nanoseconds vel_robot_stamp_;


  /*------- Message Parser ------*/
  void parseHeartBeat(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  void parseSysStatus(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  //void parseBattery(mavlink::MsgMap& msg_map);
  //void parseIMU(mavlink::MsgMap& msg_map);
  void parseIMU(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  void parseGlobalPositionINT(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  void parseLocalPosition(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  void parseAttitude(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp);
  void poseVelSyncThread();
//  Eigen::Quaterniond getQuaternionFromRPY(double roll, double pitch, double yaw);
  void getQuaternionFromRPY(double roll, double pitch, double yaw, Eigen::Quaterniond& q);

  uint64_t get_time_usec() {
    static struct timeval _time_stamp;
    gettimeofday(&_time_stamp, NULL);
    return _time_stamp.tv_sec*1000000 + _time_stamp.tv_usec;
  }

  /* Command */
  void commandThread();
  bool writeCurrentLocalNEDCommand();
  void writeManualBreak(); // Set all velocity = 0
  void setCurrentLocalNEDCommand(mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED &m);
  bool writeOffBoardControl(bool enable);

  //bool writeLocalPositionNED(double x_ned, double y_ned, double z_ned);
  //void writeLocalPositionYawNED(double x_ned, double y_ned, double z_ned, double yaw_ned);
  //void writeVelocityVehicleNED(double vx, double vy, double vz, double vyaw);

  bool writeMessage(mavlink::Message &m);

  /*For now will only do message queue for some sensitive data i.e. IMU, later will added up to other messages*/
  // HIGHRES_IMU Messages
  std::shared_ptr<std::thread> imu_cb_dispatch_thread_;
  boost::circular_buffer< std::shared_ptr<mavlink_msgs::HIGHRES_IMU> > imu_msg_queue_;
  boost::circular_buffer< std::shared_ptr<std::chrono::nanoseconds> > imu_stamped_queue_;
  std::mutex imu_msg_queue_mutex_;
  std::condition_variable imu_msg_cv_;

  void dispatchIMUMsgThread();

};
}
#endif //MOVE_DRONE_ROS_MAV_CONTROLLER_H
