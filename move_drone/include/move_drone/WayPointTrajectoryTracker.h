//
// Created by kandithws on 21/3/2561.
//

#ifndef MOVE_DRONE_WAYPOINTTRAJECTORYTRACKER_H
#define MOVE_DRONE_WAYPOINTTRAJECTORYTRACKER_H
#include <move_drone/MAVController.h>


// #include <move_drone_ros/utils/math_utils.h>
#include <cmath>
/* WayPointTrajectoryTracker is a local planner that strict to the given path
 * Using Follow the Carrot Algorithm, based on non-holonomic approach
 * */
namespace move_drone{


class WayPointTrajectoryTracker {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  enum MODE {
    POSITION_COMMAND = 0,
    VELOCITY_COMMAND = 1
  };

  typedef std::pair<Eigen::Vector3d, Eigen::Quaterniond> Pose;
  //typedef std::vector<Pose, Eigen::aligned_allocator<Pose> > Path;
  //typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > PositionVector;
  //typedef std::vector< Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond> > OrientationVector;
  //typedef std::pair<PositionVector, OrientationVector> Path;
  //typedef std::pair< std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > ,
  //                              std::vector< Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond> > Path2;

  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > PositionVector;
  typedef std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond> > OrientationVector;

  class Path{
   public:
    Path();
    Path(size_t s);
    Pose getPose(size_t index);
    void setPose(size_t index, Pose& pose);
    PositionVector p;
    OrientationVector q;
  };

  WayPointTrajectoryTracker(MAVController::Ptr& controller, MODE mode = MODE::POSITION_COMMAND);
  ~WayPointTrajectoryTracker();

  // Set Path and invoke tracking thread
  void trackPath(Path& desired_path);

  // Turn off tracking thread if it is already started
  void stopTrackPath();
  bool isTracking(); // Goal of the path is Reached?
  bool isTrackingTimeout();
  bool isGoalReached();
  // False if not tracking
  //bool getCurrentSetPoint(Pose &out);
  bool getCurrentSetPoint(Eigen::Vector3d& outpose, Eigen::Quaterniond& outq);

  Pose getCurrentPathGoal();

 private:

  MAVController::Ptr controller_;
  MODE mode_;
  std::mutex path_mutex_;
  // move_drone_ros::Path path_; // Local Copy of the path
  Path path_;
  // move_drone_ros::Path::iterator curr_waypoint_it_;
  // move_drone_ros::Path::iterator goal_waypoint_it_;
  size_t curr_waypoint_it_;
  size_t goal_waypoint_it_;
  std::shared_ptr<std::thread> tracking_thread_;
  double calcDistance2D(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2);
  double calcDistance3D(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2);
  double calcYawError(Eigen::Quaterniond& p1, Eigen::Quaterniond& p2);
  bool is_tracking_ = false; // Command State Change Variable
  bool is_shutdown_ = true;
  bool holonomic_ = false;
  // TODO -- Assert control_rate_ <= controller_->command_rate_;
  double control_rate_ = 10.0;

  bool rotate_to_goal_;

  // bool state_tracking_ = false; // Real state variable, ensure to prevent race condition

  int tracking_state_ = 0;

  // Non-holonomic Constraints
  double initial_tolerance_dist_= 0.5;
  double initial_tolerance_heading_ = M_PI/6.0; // 30 deg

  double max_x_vel_ = 5.0;
  double max_rot_vel_ = M_PI/3.0; // 60 deg/sec
  // For position control, this should be very low; For velocity control this should be approx 1 to 2 seconds
  double sim_time_ = 1.0;
  bool rotate_to_global_plan_ = false;
  double goal_tolerance_dist_ = 0.2;

  // Abort if curr_waypoint_it_ is not changed for some duration
  bool timeout_status_ = false;
  double tracking_timeout_ = 30.0; // seconds
  uint64_t timeout_increment_;


  void trackingThread();


  // Path::iterator checkMaxDistance(Eigen::Vector3d &current_position);
  size_t checkMaxDistance(Eigen::Vector3d &current_position);
  // Path::iterator checkMaxAngle(Path::iterator init_wp, Eigen::Vector3d& current_position,
  //                             Eigen::Quaterniond& current_orientation);
  size_t checkMaxAngle(size_t init_wp, Eigen::Vector3d& current_position,
                               Eigen::Quaterniond& current_orientation);

  // Path::iterator driveToward(Eigen::Vector3d& current_position, Eigen::Quaterniond& current_orientation);
  size_t driveToward(Eigen::Vector3d& current_position, Eigen::Quaterniond& current_orientation);
  bool rotateToOrientation(Eigen::Quaterniond& goal_orientation, Eigen::Vector3d& current_position,
                           Eigen::Quaterniond& current_orientation,double accuracy);

  double normalize(double z);
  double angle_diff(double a, double b);  


};
}
#endif //MOVE_DRONE_ROS_WAYPOINTTRAJECTORYTRACKER_H
