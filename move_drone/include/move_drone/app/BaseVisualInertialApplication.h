//
// Created by kandithws on 25/7/2561.
//

#ifndef PROJECT_BASEVISUALINTERTIALAPPLICATION_H
#define PROJECT_BASEVISUALINTERTIALAPPLICATION_H

#include "../BaseCameraManager.h"
#include "../BaseMAVDataInterface.h"
#include <boost/pointer_cast.hpp>

namespace move_drone{


class BaseVisualInertialApplication {

 protected:
  BaseVisualInertialApplication(boost::shared_ptr<BaseCameraManager> cam_manager_ptr,
                                boost::shared_ptr<BaseMAVDataInterface> mav_data_interface_ptr):
  cam_manager_(cam_manager_ptr),
  mav_data_interface_(mav_data_interface_ptr)
  {};

  boost::shared_ptr<BaseCameraManager> cam_manager_;
  boost::shared_ptr<BaseMAVDataInterface> mav_data_interface_;
};

}
#endif //PROJECT_BASEVISUALINTERTIALAPPLICATION_H
