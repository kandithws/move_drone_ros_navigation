//
// Created by kandithws on 5/9/2561.
//

#ifndef MOVE_DRONE_BASEEXTERNALIMU_H
#define MOVE_DRONE_BASEEXTERNALIMU_H

#include <functional>
#include <chrono>
#include <move_drone/utils/print_utils.h>

namespace move_drone {

class IMUData{
 public:
  IMUData();
  double accel[3]; //x y z
  double gyro[3]; //x y z
  double mag[3]; // x y z
  double q[4] = {0,0,0,1};
  std::chrono::nanoseconds stamp;
};

class BaseExternalIMU{
 public:
  void registerCallback(const std::function<void (const IMUData& msg)>& cb);
  virtual void startStreaming() = 0;
  virtual void stopStreaming() = 0;
  //virtual void resetGyro();

 protected:
  std::function<void (const IMUData& msg)> imu_cb_;
  //BaseExternalIMU();


};


}
#endif //MOVE_DRONE_BASEEXTERNALIMU_H
