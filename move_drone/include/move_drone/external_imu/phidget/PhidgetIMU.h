//
// Created by kandithws on 11/9/2561.
//

#ifndef MOVE_DRONE_PHIDGET_IMU_H
#define MOVE_DRONE_PHIDGET_IMU_H

#define PHIDGETS_FLIP_ACCEL_SYMBOL

#include <move_drone/external_imu/BaseExternalIMU.h>
#include <boost/circular_buffer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <move_drone/utils/time_conversion.h>
#include <phidgets_api/imu.h>
#include <string>

namespace move_drone{

class PhidgetIMU : public BaseExternalIMU, public phidgets::Imu {
 public:
  PhidgetIMU(int queue_size=600);
  //void calibrate();
  bool setZeroGyroscope();
  void setStreamingRate(int period);
  bool isConnected() const;
  void startStreaming();
  void stopStreaming();
  double angular_velocity_stdev_ = 0.000349056; // 0.02 deg/s
  double linear_acceleration_stdev_ = 0.002943; //0.0003g
  double magnetic_field_stdev_ = 0.001658; // 0.095 deg/s

 private:
  std::chrono::nanoseconds stamp_init_;
  int stream_period_ = 8; // in ms, minimum 4ms = 250 Hz
  int serial_number_ = -1; // -1 => auto detect
  double max_stamp_diff_ = 0.1; // reset initial stamp if time is not synchornized
  bool is_connected_;
  const double g_ = 9.80665;
  int error_number_ = 0;
  boost::shared_ptr<boost::thread> user_cb_dispatch_thread_;
  boost::condition_variable q_cv_;
  boost::mutex q_mutex_;
  boost::circular_buffer< boost::shared_ptr<IMUData> > q_;

  void dataHandler(CPhidgetSpatial_SpatialEventDataHandle* data, int count);
  void dispatchUserCallbackThread();
  void pushDataQueue(CPhidgetSpatial_SpatialEventDataHandle& data);
  void attachHandler();
  void detachHandler();
  void errorHandler(int error);


};

}

#endif //MOVE_DRONE_PHIDGET_IMU_H
