//
// Created by kandithws on 25/7/2561.
//

#ifndef PROJECT_UM6DRIVER_H
#define PROJECT_UM6DRIVER_H

//TODO -- refactor to implement BaseIMU
#include <move_drone/external_imu/um6/registers.h>
#include <move_drone/external_imu/um6/comms.h>

#include <move_drone/utils/time_conversion.h>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <boost/circular_buffer.hpp>
#include <chrono>
#include <functional>

namespace move_drone {
class UM6Driver {
 public:
  UM6Driver(std::string port, uint32_t baud_rate, size_t cb_queue_size = 30);

  typedef struct {
    double q[4] = {0,0,0,1}; // orientation -- quarternion: q0,q1,q2,q3 = {q.x,q.y,q.z,q.w}
    double q_cov[9]; // orientation -- vectorized 3x3 cov uncertainty around x y z
    //double euler[3]; // rpy
    double gyro[3];
    double accel[3];
    double mag[3];
    double temperature;
    std::chrono::nanoseconds stamp;
  }IMUData;

  typedef boost::shared_ptr<IMUData> IMUDataPtr;
  typedef boost::shared_ptr<IMUData const> IMUDataConstPtr;

  void startStreaming(bool reset_gyro=true);
  void stopStreaming();
  bool resetEKF();

  // Return true if start streaming
  bool enableCovEstimate(bool st);
  bool setZeroGyroscopes();
  bool setMagnetometerReference(double x, double y, double z);
  bool setAccelerometerReference(double x, double y, double z);
  bool setMagnetometerBias(double x, double y, double z);
  bool setAccelerometerBias(double x, double y, double z);
  bool setGyroscopesBias(double x, double y, double z);


  // 20 Hz to 300 Hz -> true; false otherwise
  bool setBoardcastRate(int rate=200);
  // UM6 Driver lib manages data by register buffer that will continuously update the data
  // We should call a callback for every data update cycle
  void registerIMUCallback(const std::function<void (IMUDataPtr)> &cb);
  void setCallbackEventTriggerPacket(uint8_t trigger_pkt);
  bool getIMUData(IMUDataPtr& ptr);
  //void setAccelStdDev(float val=0.06f);
  //void setGyroStdDev(float val=0.005f);

 private:
  serial::Serial serial_;
  boost::mutex data_queue_mutex_;
  boost::circular_buffer< IMUDataPtr > data_queue_;
  //int32_t baud_rate_;
  //std::string port_;
  //boost::shared_ptr<um6::Comms> sensor_;
  bool is_shutdown_ = false;
  boost::shared_ptr<boost::thread> read_thread_;
  boost::shared_ptr<boost::thread> callback_thread_;
  int board_cast_rate_ = 50;
  std::function<void (IMUDataPtr)> imu_cb_;
  //float accel_cov_ = 0.0036;
  //float gyro_cov_ = 0.000025;
  //bool tf_ned_to_enu_ = false;
  int16_t trigger_packet_ = UM6_TEMPERATURE;
  void configureSensor(um6::Comms* sensor);
  void readThread();
  void callbackThread();

  template<typename RegT>
  bool sendCommand(um6::Comms* sensor, const um6::Accessor<RegT> &reg);

  /*-------- Internal Parameter--------*/
  bool reset_gyro_on_start_ = true;
  bool required_cov_estimate_ = true;

  void pushIMUDataQueue(IMUDataPtr& data_ptr);


};

#include "move_drone/external_imu/um6/impl/UM6Driver.hpp"

}

#endif //PROJECT_UM6DRIVER_H
