//
// Created by kandithws on 26/7/2561.
//

#ifndef MOVE_DRONE_UM6DRIVER_HPP
#define MOVE_DRONE_UM6DRIVER_HPP

#include <exception>

template<typename RegT>
bool UM6Driver::sendCommand(um6::Comms* sensor, const um6::Accessor<RegT> &reg) {
  return (!sensor->sendWaitAck(reg));
}

#endif //MOVE_DRONE_UM6DRIVER_HPP
