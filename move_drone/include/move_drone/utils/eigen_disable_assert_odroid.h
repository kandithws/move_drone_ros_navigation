#ifndef EIGEN_DISABLE_ASSERT_ODROID_H
#define EIGEN_DISABLE_ASSERT_ODROID_H

/* This header should be include before every file 
*  to solve odriod runtime vectorize issue for Eigen3
*  see:: http://eigen.tuxfamily.org/dox-devel/group__TopicUnalignedArrayAssert.html
 * also:: https://eigen.tuxfamily.org/dox/group__TopicFixedSizeVectorizable.html
*/

//#define EIGEN_DONT_ALIGN_STATICALLY
#define EIGEN_DONT_VECTORIZE
#define EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT
 
#endif
