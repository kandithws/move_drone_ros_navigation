//
// Created by kandithws on 5/4/2561.
//

#ifndef MOVE_DRONE_TIME_CONVERSION_H
#define MOVE_DRONE_TIME_CONVERSION_H

#include <chrono>

namespace move_drone{

namespace utils{

static inline uint64_t get_raw_time_since_epoch(){
  return static_cast<uint64_t >(std::chrono::duration_cast<std::chrono::nanoseconds>( std::chrono::system_clock::now().time_since_epoch()).count());
}

static inline std::chrono::nanoseconds get_std_time_from_raw(uint64_t t){
  return std::chrono::nanoseconds(t);
}
static inline std::chrono::nanoseconds get_std_time_now_since_epoch(){
  return std::chrono::duration_cast<std::chrono::nanoseconds>( std::chrono::system_clock::now().time_since_epoch() );
}


}

}




#endif //MOVE_DRONE_ROS_TIME_CONVERSION_H
