//
// Created by kandithws on 14/3/2561.
//

#ifndef MOVE_DRONE_TIMER_H
#define MOVE_DRONE_TIMER_H
#include <chrono>
namespace move_drone {
namespace utils {

class Timer {
 public:
  void start() {
    m_StartTime = std::chrono::system_clock::now();
    m_bRunning = true;
  }

  void stop() {
    m_EndTime = std::chrono::system_clock::now();
    m_bRunning = false;
  }

  double elapsedMilliseconds() {
    std::chrono::time_point<std::chrono::system_clock> endTime;

    if (m_bRunning) {
      endTime = std::chrono::system_clock::now();
    } else {
      endTime = m_EndTime;
    }

    return std::chrono::duration_cast<std::chrono::milliseconds>(endTime - m_StartTime).count();
  }

  double elapsedSeconds() {
    return elapsedMilliseconds() / 1000.0;
  }

  bool isStart() {
    return m_bRunning;
  }

 private:
  std::chrono::time_point<std::chrono::system_clock> m_StartTime;
  std::chrono::time_point<std::chrono::system_clock> m_EndTime;
  bool m_bRunning = false;
};

}
}

#endif //MOVE_DRONE_ROS_TIMER_H
