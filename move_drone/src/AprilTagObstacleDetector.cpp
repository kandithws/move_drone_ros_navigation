//
// Created by kandithws on 28/5/2561.
//

#include <move_drone/AprilTagObstacleDetector.h>
namespace move_drone {

AprilTagObstacle::AprilTagObstacle() {}



AprilTagObstacle::AprilTagObstacle(int tag_id, double tag_size, double sx, double sy, double sz) {
  tag_id_ = tag_id;
  tag_size_ = tag_size;
  size_x_ = sx;
  size_y_ = sy;
  size_z_ = sz;
}

AprilTagObstacleDetector::~AprilTagObstacleDetector() {}

AprilTagObstacleDetector::AprilTagObstacleDetector(std::string config_file) {
  std::cout << "Reading OBS CONFIG: " << config_file << std::endl;
  
  parseConfig(config_file);
  cacheObstaclesCloud(cloud_granularity_);

  // Initial AprilTags Detector
  const AprilTags::TagCodes* tag_codes;
  if(tag_family_ == "16h5"){
    tag_codes = &AprilTags::tagCodes16h5;
  }
  else if(tag_family_ == "25h7"){
    tag_codes = &AprilTags::tagCodes25h7;
  }
  else if(tag_family_ == "25h9"){
    tag_codes = &AprilTags::tagCodes25h9;
  }
  else if(tag_family_ == "36h9"){
    tag_codes = &AprilTags::tagCodes36h9;
  }
  else if(tag_family_ == "36h11"){
    tag_codes = &AprilTags::tagCodes36h11;
  }
  else{
    LOGPRINT_WARN_NAMED("APRIL OBSTACLE DETECTOR","Invalid tag family specified; defaulting to 16h5");
    tag_codes = &AprilTags::tagCodes16h5;
  }

  tag_detector_ = boost::make_shared<AprilTags::TagDetector>(*tag_codes);


}

void AprilTagObstacleDetector::detect(cv::Mat &img, std::vector<AprilTagObstacle> &out_obs, bool draw_output) {
  // Ensure output
  out_obs.clear();

  if (!camera_params_init_){
    LOGPRINT_WARN_NAMED("APRIL OBSTACLE DETECTOR","Camera Params is not set, Abort!");
    exit(-1);
  }


  cv::Mat gray;
  cv::cvtColor(img, gray, CV_BGR2GRAY);
  std::vector<AprilTags::TagDetection>	detections = tag_detector_->extractTags(gray);

  for (auto &detection : detections){
    std::map<int, AprilTagObstacle::Ptr>::const_iterator config_itr = obs_config_.find(detection.id);
    if(config_itr == obs_config_.end()){
      // Skip tag that not in the config file
      // LOGPRINT_WARN_NAMED("APRIL OBSTACLE DETECTOR", "Found tag: %d, but no description was found for it", detection.id);
      continue;
    }

    AprilTagObstacle::Ptr obj = config_itr->second;

    if(draw_output){
      detection.draw(img);
    }

    obj->pose_ = detection.getRelativeTransform(obj->tag_size_, fx_, fy_, px_, py_);
    out_obs.push_back(*obj);
  }
}


void AprilTagObstacleDetector::detect(cv::Mat &img, PointCloudT &out_cloud) {
  std::vector<AprilTagObstacle> obs;   
  //auto start = std::chrono::system_clock::now();
  detect(img, obs, true);
  //auto end = std::chrono::system_clock::now();
  //std::chrono::duration<double> diff = end-start;
  //LOGPRINT_DEBUG_NAMED("OBSTACLE DETECTOR", "Total Pure detection Time= %lf", diff.count());

  //auto start2 = std::chrono::system_clock::now();
  for(auto &o : obs){
    PointCloudT o_tf_cloud;
    pcl::transformPointCloud(*obs_cloud_cache_[o.tag_id_], o_tf_cloud, o.pose_.cast<float>());
    out_cloud += o_tf_cloud;
  }

  //auto end2 = std::chrono::system_clock::now();

  //std::chrono::duration<double> diff2 = end2-start2;

  //LOGPRINT_DEBUG_NAMED("OBSTACLE DETECTOR", "Point Cloud Detection time= %lf", diff2.count());

}


void AprilTagObstacleDetector::parseConfig(std::string config_file) {
  try
  {
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(config_file, pt);
    tag_family_ = pt.get<std::string>("tag_family");
    int count = 0;
    BOOST_FOREACH(boost::property_tree::ptree::value_type& ob, pt.get_child("obstacles."))
          {
            int tag_id = ob.second.get<int>("tag_id");
            double tag_size = ob.second.get<double>("tag_size");
            double x = ob.second.get<double>("obstacle_size.x");
            double y = ob.second.get<double>("obstacle_size.y");
            double z = ob.second.get<double>("obstacle_size.z");
            // DO NOT USE MAKESHARED, please use other 2 below
            //obs_config_[tag_id] = boost::make_shared<AprilTagObstacle>(tag_id, tag_size, x, y, z);
            //obs_config_[tag_id] = boost::shared_ptr<AprilTagObstacle>( new AprilTagObstacle(tag_id, tag_size, x, y, z) );
            obs_config_[tag_id] = boost::allocate_shared<AprilTagObstacle>(Eigen::aligned_allocator<AprilTagObstacle>(),tag_id, tag_size, x, y, z);
            count++;
            /*
            std::cout << " Obs Config Valuess! : " << std::endl
                      << "\ttag id: " << tag_id << std::endl
                      << "\ttag size: " << tag_size << std::endl
                      << "\t x, y, z: " << x << "," << y << "," << z << std::endl
                      << "--------------------------------------" << std::endl ;

            */


          }
    LOGPRINT_INFO_NAMED("APRIL OBSTACLE DETECTOR", "Total obstacle config read: %d", count);


    // exit(-1);

  }
  catch (std::exception const& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

void AprilTagObstacleDetector::setCameraParams(double fx, double fy, double px, double py) {
  fx_ = fx;
  fy_ = fy;
  px_ = px;
  py_ = py;
  camera_params_init_ = true;
}


void AprilTagObstacleDetector::setCameraParams(CameraInfo cam_info) {
  fx_ = cam_info.getProjectedfx();
  fy_ = cam_info.getProjectedfy();
  px_ = cam_info.getProjectedpx();
  py_ = cam_info.getProjectedpy();
  camera_params_init_ = true;
}


void AprilTagObstacleDetector::getObstaclesCacheCloud(int tag_id, PointCloudT &out_cloud) {
  out_cloud = *obs_cloud_cache_[tag_id];
}

void AprilTagObstacleDetector::cacheObstaclesCloud(double cloud_granularity) {

  for (auto const& obs : obs_config_){
    // obs.first = key, obs.second = value
    PointCloudT::Ptr ob_cloud = PointCloudT::Ptr(new PointCloudT);
    // TODO -- fix some odd number interpolation when divided by 2.0,
    // need to round up to some where to get a perfect shape
    double min_x = -obs.second->size_x_/2.0;
    double max_x = obs.second->size_x_/2.0;
    double min_y = -obs.second->size_y_/2.0;
    double max_y = obs.second->size_y_/2.0;
    //double min_z = -obs.second.size_z_/2.0;
    //double max_z = obs.second.size_z_/2.0;
    // Assume we will put AprilTag on the floor and using 45 degree camera to see it
    double min_z = 0.0;
    double max_z = obs.second->size_z_;
    size_t resol_x = (size_t) ( (obs.second->size_x_) / cloud_granularity );
    size_t resol_y = (size_t) ( (obs.second->size_y_) / cloud_granularity );
    size_t resol_z = (size_t) ( (obs.second->size_z_) / cloud_granularity ) + 1;

    // TODO -- No need to fill inside the box just outside planar only
    // Create Rectangle @ 6 planes x=min_x, max_x , y = min_y, max_y, z = min_z, max_z
    // AprilTag TF
    /*
     *     ---------------
     *     |     ^       |
     *     |     |       |
     *     |     y       |
     *     |     0--x--> |  //center, z point out
     *     |             |
     *     |------------ |
     *
     * */

     for (size_t k = 0; k <= resol_z; k++){
       for (size_t i = 0; i <= resol_x; i++ ){
         // Construct y = min_y, max_y plane at height z = k (along x-axis)
          ob_cloud->points.push_back(pcl::PointXYZ(min_x + (double)i  * cloud_granularity ,
                                                   min_y, (double)k * cloud_granularity) );
          ob_cloud->points.push_back(pcl::PointXYZ(min_x + (double)i  * cloud_granularity,
                                                   max_y, (double)k * cloud_granularity)  );
       }

       for (size_t j = 1; j <= resol_y - 1; j++ ){
         // Construct x = min_x, max_x plane at height z = k (along x-axis)
         ob_cloud->points.push_back(pcl::PointXYZ(min_x, min_y + (double)j  * cloud_granularity,
                                                  (double)k * cloud_granularity) );

         ob_cloud->points.push_back(pcl::PointXYZ(max_x, min_y + (double)j  * cloud_granularity,
                                                  (double)k * cloud_granularity) );
       }

     }

     // Top lid
    for (size_t i = 0; i <= resol_x; i++){
      for (size_t j = 1; j <= resol_y-1; j++){
        ob_cloud->points.push_back(pcl::PointXYZ(min_x + ( (double)i  * cloud_granularity),
                                                 min_y + ( (double)j  * cloud_granularity),
                                                 max_z) );
      }
    }
    obs_cloud_cache_[obs.second->tag_id_] = ob_cloud;
  } // end obstcle for

}

}
