//
// Created by kandithws on 23/7/2561.
//
#include <move_drone/BaseCameraManager.h>

namespace move_drone {
ImageStamped::ImageStamped() {

}

ImageStamped::ImageStamped(cv::Mat img, Encoding encoding) {
  //deep copy
  img.copyTo(image_);
  stamp_ = utils::get_std_time_now_since_epoch(); //get time now
  encoding_ = encoding;
}

ImageStamped::ImageStamped(cv::Mat img, std::chrono::nanoseconds stamp, Encoding encoding) {
  img.copyTo(image_);
  stamp_ = stamp;
  encoding_ = encoding;
}

void ImageStamped::setTimeNow() {
  stamp_ = utils::get_std_time_now_since_epoch();
}

void ImageStamped::convertToEncoding(const Encoding &encoding) {
  if ((encoding_ == encoding) || (encoding_ == Encoding::MONO8))
    return;

  //LOGASSERT_MSG();
  if (encoding_ == Encoding::BGR8){
    cv::Mat img_tmp;
    if (encoding == Encoding::RGB8){
      cv::cvtColor(image_, image_, cv::COLOR_BGR2RGB);
      encoding_ = Encoding::RGB8;
    }
    else if (encoding == Encoding::MONO8){
      cv::cvtColor(image_, image_, cv::COLOR_BGR2GRAY);
      encoding_ = Encoding::MONO8;
    }



  }
  else if (encoding_ == Encoding::RGB8){

    if (encoding == Encoding::BGR8){
      cv::cvtColor(image_, image_, cv::COLOR_RGB2BGR);
      encoding_ = Encoding::RGB8;
    }
    else if (encoding == Encoding::MONO8){
      cv::cvtColor(image_, image_, cv::COLOR_RGB2GRAY);
      encoding_ = Encoding::MONO8;
    }


  }

}

std::string ImageStamped::getStringEncoding() const {
  if (encoding_ == Encoding::BGR8){
    return "bgr8";
  }
  else if (encoding_ == Encoding::RGB8){
    return "rgb8";
  }
  else if (encoding_ == Encoding::MONO8){
    return "mono8";
  }
}

ImageStamped::Encoding ImageStamped::encodingFromString(std::string encoding_str) {
  if (encoding_str == "bgr8"){
    return Encoding::BGR8;
  }
  else if (encoding_str == "rgb8"){
    return Encoding::RGB8;
  }
  else if (encoding_str == "mono8" || encoding_str == "gray"){
    return Encoding::MONO8;
  }

  LOGASSERT_MSG( (encoding_str == "bgr8") || (encoding_str == "rgb8") ||
      (encoding_str == "mono8") || (encoding_str == "gray"), "[ImageStamped] Encoding Type Error");
}


//-----------------------------------------------------------------------

BaseCameraManager::BaseCameraManager(size_t queue_size):
  img_queue_(queue_size)
{}

BaseCameraManager::~BaseCameraManager() {
  if(!is_shutdown_)
    stopStreaming();
}

void BaseCameraManager::setImageQueueSize(size_t queue_size) {
  boost::mutex::scoped_lock lock(img_queue_mutex_);
  img_queue_.resize(queue_size);
}

void BaseCameraManager::clearImageQueue() {
  boost::mutex::scoped_lock lock(img_queue_mutex_);
  img_queue_.clear();
}

void BaseCameraManager::loadCameraInfo(std::string filename) {
  camera_params_ = CameraInfo(filename);
  image_width_ = camera_params_.width;
  image_height_ = camera_params_.height;
  target_encoding_ = ImageStamped::encodingFromString(camera_params_.encoding);

//  if(camera_params_.encoding == "rgb8")
//    convert_to_rgb_ = true;
  // camera_params_.print();
}

void BaseCameraManager::pushImageQueue(ImageStamped::Ptr &frame_stamped) {
  LOGASSERT(frame_stamped);
  if(!frame_stamped->image_.empty()){

    if(camera_params_.undistort){
      cv::Mat out;
      camera_params_.unDistortImage(frame_stamped->image_, out);
      frame_stamped->image_ = out;
    }

    frame_stamped->convertToEncoding(target_encoding_);

//    if(convert_to_rgb_){
//      cv::cvtColor(frame_stamped->image_, frame_stamped->image_, cv::COLOR_BGR2RGB);
//      frame_stamped->encoding_ = "rgb8";
//    }
    {
      boost::mutex::scoped_lock lock(img_queue_mutex_);
      img_queue_.push_back(frame_stamped);
    }
  
  // if imageCallback is register, then invoke callback thread
  if(image_cb_)
    img_queue_cv_.notify_all();
  
  }
  
}

void BaseCameraManager::imageCallbackDispatchThread() {
  assert(image_cb_);
  while(!is_shutdown_){
    boost::unique_lock<boost::mutex> lock(img_queue_mutex_); // first try to lock the mutex: unique_lock = scoped_lock
    img_queue_cv_.wait(lock, [this]{ return !img_queue_.empty() || is_shutdown_; }); // unlock until !img_queue_.empty()
    // mutex lock again
    if(!img_queue_.empty()){
      // copying stuffs out of q
      ImageStamped::Ptr img = img_queue_.front();
      img_queue_.pop_front();
      lock.unlock();
      // unlock mutex while processing callbacks, (then read thread will continue inserting the queue)
      image_cb_(img);
    }
  }

}



bool BaseCameraManager::getImage(ImageStamped::Ptr& img_stamped_ptr) {
  if(!img_queue_.empty()){
    boost::mutex::scoped_lock lock(img_queue_mutex_);
    img_stamped_ptr = img_queue_.front();
    img_queue_.pop_front();
    return true;
  }
  else{
    return false;
  }
}

void BaseCameraManager::registerCallback(const std::function<void(const ImageStamped::ConstPtr &)> &cb) {
  image_cb_ = cb;
}

void BaseCameraManager::startStreaming() {
  is_shutdown_ = false;
  clearImageQueue();
  initStartStreaming();
  // handle callbackthread if registered
  if(image_cb_){
    image_cb_dispatch_thread_ = boost::make_shared<boost::thread>(
        boost::bind(&BaseCameraManager::imageCallbackDispatchThread, this));

  }
  boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
}

void BaseCameraManager::stopStreaming() {
  is_shutdown_ = true;
  initStopStreaming();
  if(image_cb_){
    img_queue_cv_.notify_all();
    image_cb_dispatch_thread_->join();
    image_cb_dispatch_thread_.reset();
  }
  // We shouldn't clear the queue here, allow sometime for user to clean up
}




}

