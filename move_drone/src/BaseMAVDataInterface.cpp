//
// Created by kandithws on 20/7/2561.
//

#include <move_drone/BaseMAVDataInterface.h>

namespace move_drone {

void BaseMAVDataInterface::registerHeartBeatCallback(const std::function<void(const mav_heartbeat_status &,
                                                                             const std::chrono::nanoseconds &)> &cb) {
  //if (!heart_beat_cb_)
    heart_beat_cb_ = cb;
}

void BaseMAVDataInterface::registerSystemStatusCallback(const std::function<void(const mav_system_status &,
                                                                                const std::chrono::nanoseconds &)> &cb) {
  //if (!sys_status_cb_)
    sys_status_cb_ = cb;
}

void BaseMAVDataInterface::registerLocalPositionNEDCallback(const std::function<void(const mavlink_msgs::LOCAL_POSITION_NED &,
                                                                                    const std::chrono::nanoseconds &)> &cb) {
  //if (!local_position_cb_)
    local_position_cb_ = cb;
}

void BaseMAVDataInterface::registerGlobalPositionINTCallback(const std::function<void(const mavlink_msgs::GLOBAL_POSITION_INT &,
                                                                                     const std::chrono::nanoseconds &)> &cb) {
  //if (!global_position_cb_)
    global_position_cb_ = cb;
}

void BaseMAVDataInterface::registerAttituteCallback(const std::function<void(const mavlink_msgs::ATTITUDE &,
                                                                            const std::chrono::nanoseconds &)> &cb) {
  //if (!attitute_cb_)
    attitute_cb_ = cb;
}

void BaseMAVDataInterface::registerHighResIMUCallback(const std::function<void(const mavlink_msgs::HIGHRES_IMU &,
                                                                              const std::chrono::nanoseconds &)> &cb) {
  //if (!imu_cb_)
    imu_cb_ = cb;

}

void BaseMAVDataInterface::registerPoseCallback(const std::function<void(const Eigen::Vector3d &,
                                                                        const Eigen::Quaterniond &,
                                                                        const std::chrono::nanoseconds &)> &cb) {
  //if(!pose_cb_)
    pose_cb_ = cb;
}

void BaseMAVDataInterface::registerVelocityCallback(const std::function<void(const Eigen::Vector3d &,
                                                                            const Eigen::Vector3d &,
                                                                            const std::chrono::nanoseconds &)> &cb) {
  //if(!vel_cb_)
    vel_cb_ = cb;
}

void BaseMAVDataInterface::registerPoseVelCallback(const std::function<void(const Eigen::Vector3d &,
                                                                            const Eigen::Quaterniond &,
                                                                            const Eigen::Vector3d &,
                                                                            const Eigen::Vector3d &,
                                                                            const std::chrono::nanoseconds &)> &cb) {
    pose_vel_cb_ = cb;
}




}