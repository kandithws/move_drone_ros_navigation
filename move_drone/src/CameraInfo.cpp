#include <move_drone/CameraInfo.h>
namespace move_drone{


CameraInfo::CameraInfo() {}

CameraInfo::CameraInfo(std::string filename){
    //std::cout << "Reading Camera Calibration File from: " << filename << std::endl;
    cv::FileStorage fs;
    fs.open(filename, cv::FileStorage::READ);
    assert(fs.isOpened() );
    fs["camera_matrix"] >> K;
    assert( (K.rows == 3) && (K.cols == 3) );
    fs["distortion_coefficients"] >> D;

    fs["rectification_matrix"] >> R;
    assert( (R.rows == 3) && (R.cols == 3) );
    fs["projection_matrix"] >> P;

    camera_name = (std::string)fs["camera_name"]; 
    distortion_model = (std::string)fs["distortion_model"];
    width = (int)fs["image_width"];
    height = (int)fs["image_height"];
    std::string ud_str = (std::string)fs["undistort"];
    undistort = (ud_str == "true");

    encoding = (std::string)fs["encoding"];
    if( (encoding != "bgr8" ) && ( encoding != "rgb8") && ( encoding != "mono8") ){
      std::cout << "[CameraInfo]: " << "No encoding for camera_name=" << camera_name << " force to \"bgr8\" " << std::endl;
      encoding = "bgr8";
    }

}

void CameraInfo::print(){
    std::cout << "camera_name: " << camera_name << std::endl;
    std::cout << "resolution (width, height): " << width << "," << height << std::endl;
    std::cout << "distortion_model: " << distortion_model << std::endl;
    std::cout << "K: ";
    printMat(K);
    std::cout << "R: ";
    printMat(R);
    std::cout << "P: ";
    printMat(P);
}

void CameraInfo::unDistortImage(const cv::Mat &in, cv::Mat &out) {
  cv::undistort(in, out, K, D);
}

double CameraInfo::getProjectedfx() const{
    return P.at<double>(0,0);
}

double CameraInfo::getProjectedfy() const{
    return P.at<double>(1,1);
}

double CameraInfo::getProjectedpx() const{
    return P.at<double>(0,2);
}

double CameraInfo::getProjectedpy() const{
    return P.at<double>(1,2);
}

void CameraInfo::printMat(cv::Mat mat) {
    for(int i=0; i<mat.size().height; i++)
    {
      std::cout << "[";
      for(int j=0; j<mat.size().width; j++)
      {
        std::cout  << mat.at<double>(i,j);
        if(j != mat.size().width-1)
          std::cout << ", ";
        else
          std::cout << "]" << std::endl;
      }
    }
  }

}