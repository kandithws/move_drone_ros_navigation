//
// Created by kandithws on 5/4/2561.
//

#include <move_drone/CameraManager.h>

namespace move_drone{


CameraManager::CameraManager(std::string device, int queue_size, double fps) :
    BaseCameraManager(queue_size){
  fps_rate_ = fps;
  size_t cut = device.find("::");
  std::string device_type = device.substr(0, cut);
  device_path_ = device.substr(cut+2);


  if (device_type == "camera"){
    device_type_video_ = false;

  }
  else if (device_type == "video"){
    device_type_video_ = true;
  }
  else{
    LOGASSERT_MSG( false, "CameraManager: Wrong device format!");
  }

  is_shutdown_ = false;
  if(device_type_video_){
    cap_ = boost::make_shared<cv::VideoCapture>(device_path_);
  }
  else{

    //cap_ = boost::make_shared<cv::VideoCapture>(device_path_, cv::CAP_V4L);
    cap_ = boost::make_shared<cv::VideoCapture>(normalizedVideoDevice(device_path_)) ;
  }

}

//void CameraManager::startStreaming() {
//  camera_thread_ = boost::make_shared<boost::thread>(boost::bind(&CameraManager::cameraThread, this));
//  boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
//}

void CameraManager::initStartStreaming() {
  if (!cap_->isOpened()){
    std::string out_msg = "[CameraManager] Device: " + device_path_ + "cannot be opened.";
    throw std::runtime_error(out_msg);
   }
    
  camera_thread_ = boost::make_shared<boost::thread>(boost::bind(&CameraManager::cameraThread, this));

}

//void CameraManager::stopStreaming() {
//  if (!is_shutdown_ )
//    is_shutdown_ = true;
//
//  if(camera_thread_){
//    camera_thread_->join();
//    camera_thread_.reset();
//  }
//
//  clearImageQueue();
//}
void CameraManager::initStopStreaming() {
  if(camera_thread_){
    camera_thread_->join();
    camera_thread_.reset();
  }
}

int CameraManager::normalizedVideoDevice(std::string device_path){
    // This is implement temporarily because odroid opencv still not support
    // V4L, should be remove later after fix opencv
    return device_path.back() - '0';
}



//CameraManager::~CameraManager() {
//  if (!is_shutdown_ )
//    is_shutdown_ = true;
//
//  if(camera_thread_){
//    camera_thread_->join();
//  }
//}

void CameraManager::cameraThread() {
  //boost::chrono::milliseconds loop_period(  (uint64_t)(1./fps_rate_ * 1000.0) );
  std::string assert_msg = "Cannot open video device/file: ";
  assert_msg.append(device_path_);
  LOGASSERT_MSG(cap_->isOpened(), assert_msg.c_str() );
  cap_->set(CV_CAP_PROP_FRAME_WIDTH, image_width_);
  cap_->set(CV_CAP_PROP_FRAME_HEIGHT,image_height_);
  cap_->set(CV_CAP_PROP_FPS, fps_rate_);
  while (!is_shutdown_){
    ImageStamped::Ptr frame_stamped = boost::make_shared<ImageStamped>();
    *cap_ >> frame_stamped->image_;
    frame_stamped->setTimeNow();
    pushImageQueue(frame_stamped);
  }
}



}

