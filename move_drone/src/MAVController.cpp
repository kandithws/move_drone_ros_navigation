//
// Created by kandithws on 19/2/2561.
//

#include <move_drone/MAVController.h>



// ------- ENUM FOR SET_TARGET_LOCAL_NED --------
// bit number  876543210987654321
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_POSITION     0b0000110111111000
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_VELOCITY     0b0000110111000111
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_ACCELERATION 0b0000110000111111
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_FORCE        0b0000111000111111
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_YAW_ANGLE    0b0000100111111111
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_YAW_RATE     0b0000010111111111


//0b0000110111000111
//0b0000010111111111
//0b0000000111000111
//0b0000010111000111
// These are special bit type mask for px4
//#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_TAKE_OFF     0x1000
//#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LANDING      0x2000
//#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LOITER       0x3000

// Ref: http://discuss.px4.io/t/px4-hidden-flight-bitmasks/1371

// controls height, and velocity on takeoff
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_TAKE_OFF 0b0001111111111000

// Select desired landing x,y position
//#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LANDING  0b0010111111111000

#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LANDING  0b0010111111000111

// Loiter will ignore any command bits
#define MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LOITER   0b0011110111111111

#define MAV_FRAME_LOCAL_NED 1
#define MAV_FRAME_LOCAL_OFFSET_NED 7
#define MAV_FRAME_BODY_NED 8
#define MAV_FRAME_BODY_OFFSET_NED 9

// Alias name for mavlink message
//namespace mavlink_msg = mavlink::common::msg;

using mavlink::mavlink_message_t;
using namespace mavlink::common;
using namespace mavconn;
namespace move_drone{



MAVController::MAVController(std::string port_url) : imu_stamped_queue_(200), imu_msg_queue_(200) {
  port_url_ = port_url;
}

void MAVController::startController() {
  is_shutdown_ = false;
  if(!is_init_){
    connect_ = mavconn::MAVConnInterface::open_url(port_url_); // Factory to build dynamic connection

    LOGASSERT_MSG(connect_, "MAVController fails to connect to specify port" );
    LOGPRINT_WARN("Start Connecting to %s", port_url_.c_str());
    // Set msg callback
    if(imu_cb_){
      imu_cb_dispatch_thread_ = std::make_shared<std::thread>(std::bind(&MAVController::dispatchIMUMsgThread, this));
    }

    connect_->message_received_cb = std::bind(&MAVController::msgCallBack, this,
                                              std::placeholders::_1, std::placeholders::_2);

    pose_vel_sync_thread_ = std::make_shared<std::thread>(&MAVController::poseVelSyncThread, this);
  }
  else{
    LOGPRINT_WARN_NAMED("MavController", "Connection has already established");
  }

}


MAVController::~MAVController() {
  is_shutdown_ = true;
  pose_vel_sync_thread_->join();
  pose_vel_sync_thread_.reset();
  stopCommand();
}

void MAVController::msgCallBack(const mavlink_message_t *message, const Framing framing) {

  if(framing == Framing::ok){
    //LOGPRINT_INFO("MSG RECEIVED!----");
    if( (!is_init_) && (message->msgid == mavlink_msgs::HEARTBEAT::MSG_ID)){
      // Initial with Heartbeat
      autopilot_id_ = message->compid;
      //sys_id_ = message->sysid;
      sys_id_ = message->sysid;

      LOGPRINT_INFO("Connecting with system with id (sys: %d, comp: %d)", message->sysid, message->compid);
      //connect_->set_system_id(sys_id_);
      //connect_->set_component_id(0);
      is_init_ = true;
      LOGPRINT_DEBUG("Initialization Complete");
      return;
    }
    else{
      // Copy msg as a message map
      // TODO -- Update all call back function to have timestamp !,
      std::chrono::nanoseconds stamp = utils::get_std_time_now_since_epoch();
      mavlink::MsgMap msg_map(message);

      //LOGPRINT_INFO("", )
      switch(message->msgid){
        case mavlink_msgs::HEARTBEAT::MSG_ID:{
          parseHeartBeat(msg_map, stamp);
          break;

        }
        case mavlink_msgs::SYS_STATUS::MSG_ID: {
          parseSysStatus(msg_map, stamp);
          break;
        }
//        case mavlink_msgs::BATTERY_STATUS::MSG_ID:{
//          parseBattery(msg_map);
//          break;
//        }
        case mavlink_msgs::HIGHRES_IMU::MSG_ID:{
          //parseIMU(msg_map);
          parseIMU(msg_map, stamp);
          break;
        }
        case mavlink_msgs::GLOBAL_POSITION_INT::MSG_ID:{
          parseGlobalPositionINT(msg_map, stamp);
          break;
        }
        case mavlink_msgs::LOCAL_POSITION_NED::MSG_ID:{
          parseLocalPosition(msg_map, stamp);
          break;
        }
        case mavlink_msgs::ATTITUDE::MSG_ID: {
          parseAttitude(msg_map, stamp);
          break;
        }
        default:
          // LOGPRINT_INFO("In comming msg id %u", message->msgid);
          break;
      }
    }
  }
  else{
    std::string msg_status;
    if(framing == Framing::bad_crc) msg_status="BAD_CRC";
    else if(framing == Framing::bad_signature) msg_status="BAD_SIGNATURE";
    else msg_status="INCOMPLETE";
    LOGPRINT_WARN("Error in MAVPackage %s, discard", msg_status.c_str());
  }
}

void MAVController::parseHeartBeat(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp) {
  mavlink_msgs::HEARTBEAT m;
  m.deserialize(msg_map);


  mav_heartbeat_status st;
  st.frame_type = m.type;
  st.autopilot_type = m.autopilot;
  st.arm = m.base_mode & static_cast<uint8_t > (MAV_MODE_FLAG::SAFETY_ARMED);
  st.in_hil = m.base_mode & static_cast<uint8_t >(MAV_MODE_FLAG::HIL_ENABLED);
  st.manual_remote_enabled = m.base_mode & static_cast<uint8_t >(MAV_MODE_FLAG::MANUAL_INPUT_ENABLED);
  st.auto_enable = m.base_mode & static_cast<uint8_t >(MAV_MODE_FLAG::AUTO_ENABLED);
  st.guided_enable = m.base_mode & static_cast<uint8_t >(MAV_MODE_FLAG::GUIDED_ENABLED);
  st.mav_version = m.mavlink_version;

  if(m.system_status == static_cast<uint8_t >(MAV_STATE::STANDBY) ) st.system_status = "STANDBY";
  else if (m.system_status == static_cast<uint8_t >(MAV_STATE::ACTIVE) ) st.system_status = "ACTIVE";
  else if (m.system_status == static_cast<uint8_t >(MAV_STATE::CRITICAL) ) st.system_status = "CRITICAL";
  else if (m.system_status == static_cast<uint8_t >(MAV_STATE::EMERGENCY) ) st.system_status = "EMERGENCY";
  else if (m.system_status == static_cast<uint8_t >(MAV_STATE::BOOT) ) st.system_status = "BOOTING";
  else st.system_status = "OTHER";
  // LOGPRINT_INFO("HEARTBEAT=> name:%s, type:%u", m.get_name().c_str(),  m.type );
  // invoking user callback

  // Cache heartbeat status for other control usage
  hb_st_ = st;
  if(heart_beat_cb_)
    heart_beat_cb_(st, stamp);
}

void MAVController::parseSysStatus(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp){
  mavlink_msgs::SYS_STATUS m;
  m.deserialize(msg_map);
  mav_system_status st;
  st.batt_voltage = ( (float) m.voltage_battery) * 0.001f;
  st.batt_percent = ( (float) m.battery_remaining);
  st.load_percent = ( (float) m.load) * 0.1f;
  // TODO -- implement sensors status

  if(sys_status_cb_)
    sys_status_cb_(st, stamp);
  // LOGPRINT_INFO("Sys=> name:%s, volt:%u ", m.get_name().c_str(),  m.voltage_battery );
}

//void MAVController::parseBattery(mavlink::MsgMap &msg_map) {
//  mavlink_msgs::BATTERY_STATUS m;
//  m.deserialize(msg_map);
//  // LOGPRINT_INFO("IN MSG=> name:%s, ", m.get_name().c_str() );
//}



void MAVController::parseIMU(mavlink::MsgMap &msg_map, std::chrono::nanoseconds& stamp) {
  mavlink_msgs::HIGHRES_IMU m;
  m.deserialize(msg_map);

  //if(imu_cb_)
  //  imu_cb_(m, stamp);
  if(imu_cb_){
    // push to Queue
    std::shared_ptr<mavlink_msgs::HIGHRES_IMU> msg_ptr = std::make_shared<mavlink_msgs::HIGHRES_IMU>(m);
    std::shared_ptr<std::chrono::nanoseconds> stamp_ptr = std::make_shared<std::chrono::nanoseconds>(stamp);

    std::lock_guard<std::mutex> lock(imu_msg_queue_mutex_);
    imu_msg_queue_.push_back(msg_ptr);
    imu_stamped_queue_.push_back(stamp_ptr);
    // notify all
    imu_msg_cv_.notify_all();
  }
  // LOGPRINT_INFO("IN MSG=> name:%s, ", m.get_name().c_str() );
}

void MAVController::dispatchIMUMsgThread() {
  assert(imu_cb_);
  while(!is_shutdown_){
    //imu_stamped_queue_.empty
    std::unique_lock<std::mutex> lock(imu_msg_queue_mutex_);
    imu_msg_cv_.wait(lock, [this]{return (!imu_stamped_queue_.empty()) && (!imu_msg_queue_.empty());});

    // pop queue
    std::shared_ptr<mavlink_msgs::HIGHRES_IMU> msg_ptr = imu_msg_queue_.front();
    std::shared_ptr<std::chrono::nanoseconds> stamped_ptr = imu_stamped_queue_.front();
    imu_msg_queue_.pop_front();
    imu_stamped_queue_.pop_front();
    lock.unlock();
    imu_cb_(*msg_ptr, *stamped_ptr);

  }
}

void MAVController::getIMUMessagesUpToStamp(std::vector<std::shared_ptr<mavlink_msgs::HIGHRES_IMU> >& imu_msgs,
                                            std::vector<std::shared_ptr<std::chrono::nanoseconds> >& stamp_msgs,
                                            std::chrono::nanoseconds stamp_limit) {
  imu_msgs.clear();
  stamp_msgs.clear();
  size_t max_idx;
  std::lock_guard<std::mutex> lock(imu_msg_queue_mutex_);
  for(max_idx = 0; max_idx < stamp_msgs.size(); max_idx++){
    if (*imu_stamped_queue_[max_idx] > stamp_limit) break;
  }

  imu_msgs.insert(imu_msgs.end(), imu_msg_queue_.begin(), imu_msg_queue_.end());
  stamp_msgs.insert(stamp_msgs.end(), imu_stamped_queue_.begin(), imu_stamped_queue_.end());
  // copy vector upto index

}

void MAVController::parseGlobalPositionINT(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp) {
  mavlink_msgs::GLOBAL_POSITION_INT m;
  m.deserialize(msg_map);
  if (global_position_cb_)
    global_position_cb_(m, stamp);
}

void MAVController::parseLocalPosition(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp) {
  mavlink_msgs::LOCAL_POSITION_NED m;
  m.deserialize(msg_map);

  {
    std::lock_guard<std::mutex> lock(pose_vel_ned_mutex_);
    position_ned_[0] = m.x;
    position_ned_[1] = m.y;
    position_ned_[2] = m.z;
    linear_vel_ned_[0] = m.vx;
    linear_vel_ned_[1] = m.vy;
    linear_vel_ned_[2] = m.vz;
    position_ned_msg_stamp_ = stamp;
    local_ned_update_ = true;
  }

  if(local_position_cb_)
    local_position_cb_(m, stamp);
}

void MAVController::parseAttitude(mavlink::MsgMap& msg_map, std::chrono::nanoseconds& stamp) {
  mavlink_msgs::ATTITUDE m;
  m.deserialize(msg_map);

  {
    std::lock_guard<std::mutex> lock(pose_vel_ned_mutex_);
    //orientation_vehicle_ned_ = getQuaternionFromRPY(m.roll, m.pitch, m.yaw);
    getQuaternionFromRPY(m.roll, m.pitch, m.yaw, orientation_vehicle_ned_);
    //orientation_vehicle_ned_ = ftf::quaternion_from_rpy(Eigen::Vector3d(m.roll, m.pitch, m.yaw));
    // LOGPRINT_WARN("OUTPUT EULER RPY= %lf, %lf, %lf", euler(0), euler(1), euler(2));
    angular_vel_vehicle_ned_[0] = m.rollspeed;
    angular_vel_vehicle_ned_[1] = m.pitchspeed;
    angular_vel_vehicle_ned_[2] = m.yawspeed;
    attitute_msg_stamp_ = stamp;
    attitude_ned_update_ = true;
  }

  if(attitute_cb_) // re check if user bind the callback
    attitute_cb_(m,stamp);
  // LOGPRINT_INFO("IN MSG=> name:%s, ", m.get_name().c_str() );
}

bool MAVController::getRobotFrameTransform(Eigen::Vector3d &position, Eigen::Quaterniond &orientation){

  std::lock_guard<std::mutex> lock(robot_tf_mutex_);
  position = position_robot_;
  orientation = orientation_robot_;
  return robot_data_ready_;
}

bool MAVController::getRobotFrameTransform(Eigen::Vector3d &position,
                                                 Eigen::Quaterniond &orientation, std::chrono::nanoseconds &stamp){

  std::lock_guard<std::mutex> lock(robot_tf_mutex_);
  position = position_robot_;
  orientation = orientation_robot_;
  stamp = tf_robot_stamp_;
  return robot_data_ready_;
}

bool MAVController::getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular){
  std::lock_guard<std::mutex> lock(robot_vel_mutex_);
  linear = vel_linear_robot_;
  angular = vel_angular_robot_;
  return robot_data_ready_;
}

bool MAVController::getRobotFrameVelocity(Eigen::Vector3d &linear, Eigen::Vector3d &angular,
                                                std::chrono::nanoseconds &stamp){
  std::lock_guard<std::mutex> lock(robot_vel_mutex_);
  linear = vel_linear_robot_;
  angular = vel_angular_robot_;
  stamp = vel_robot_stamp_;
  return robot_data_ready_;
}

void MAVController::poseVelSyncThread() {

  std::chrono::milliseconds sync_period_millis((int)((1.0/pose_vel_update_rate_)*1000.0)); // update rate in hz
  LOGPRINT_DEBUG("START SYNC THEARDDD!!!!!!!!!!!!!!!!!!!!!!!!!");
  while(!is_shutdown_){
    if (attitude_ned_update_){ // update based on attitute
      // TODO -- Change this to condition variable based! -- remove rate!
      Eigen::Quaterniond tmp_q;
      Eigen::Vector3d tmp_position;
      Eigen::Vector3d tmp_linear_vel;
      Eigen::Vector3d tmp_angular_vel;
      std::chrono::nanoseconds tmp_stamp;
      {
        std::lock_guard<std::mutex> lock(pose_vel_ned_mutex_);

        tmp_q = orientation_vehicle_ned_;
        tmp_position = position_ned_;
        tmp_linear_vel = linear_vel_ned_;
        tmp_angular_vel = angular_vel_vehicle_ned_;
        tmp_stamp = attitute_msg_stamp_; // Rely on Local attitute because it is faster by default ...
      }

      /* Conversion from ned to enu goes here !!!! */
      Eigen::Vector3d enu_position = ftf::transform_frame_ned_enu(tmp_position);
      Eigen::Vector3d enu_linear_vel = ftf::transform_frame_ned_enu(tmp_linear_vel);     // Change Quaternion as ros
      // Get Attitute pose from aircraft ned to ros base link
      Eigen::Quaterniond baselink_q = ftf::transform_orientation_aircraft_baselink(ftf::transform_orientation_ned_enu(tmp_q));
      Eigen::Vector3d baselink_angular = ftf::transform_frame_aircraft_baselink(tmp_angular_vel);
      /* For odometry message, twist must be specified in the robot frame not the map frame*/
      auto baselink_linear =  ftf::transform_frame_enu_baselink(enu_linear_vel, baselink_q.inverse()); // convert to ROS's base_link

      // TODO -- Timestamp interpolation
      {
        // Update Transform
        std::lock_guard<std::mutex> lock(robot_tf_mutex_);
        position_robot_ = enu_position; // enu = baselink for position case
        orientation_robot_ = baselink_q;
        tf_robot_stamp_ = tmp_stamp;
      }

      {
        // Update Velocity
        std::lock_guard<std::mutex> lock(robot_vel_mutex_);
        vel_linear_robot_ = baselink_linear;
        vel_angular_robot_ = baselink_angular;
        vel_robot_stamp_ = tmp_stamp;
      }

      // TODO -- Spin callback in a seperate Thread

      if(pose_cb_)
        pose_cb_(enu_position, baselink_q, tmp_stamp);

      if(vel_cb_)
        vel_cb_(baselink_linear, baselink_angular, tmp_stamp);

      if(pose_vel_cb_)
        pose_vel_cb_(enu_position, baselink_q, baselink_linear, baselink_angular, tmp_stamp);

      robot_data_ready_ = true;
      local_ned_update_ = false;
      attitude_ned_update_ = false;

    }
    std::this_thread::sleep_for(sync_period_millis);
  }

  LOGPRINT_DEBUG("END SYNC THREADDD!!!!!!!!!!!!!!!!!!!!!!!!!");

}

// ROS Compatible Quaternion (Z-X-Y convention), Note there is still numerical difference between ROS vs Eigen
//Eigen::Quaterniond MAVController::getQuaternionFromRPY(double roll, double pitch, double yaw){
//  Eigen::Quaterniond q =
//      Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ())
//          * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX())
//          * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY());
//  return q;
//}

void MAVController::getQuaternionFromRPY(double roll, double pitch, double yaw, Eigen::Quaterniond& q){
  q =
      Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ())
          * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX())
          * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY());
}

//---------------------------- Commanding Interface ---------------------------------------
void MAVController::commandLocalPositionENU(double x, double y, double z, double yaw) {
  if(!cmd_enable_){
    if(!startCommand())
      return;
  }

  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
  if (std::isnan(yaw)){
    Eigen::Vector3d ned_position = ftf::transform_frame_enu_ned( Eigen::Vector3d(x,y,z) );
    m.coordinate_frame = MAV_FRAME_LOCAL_NED;
    m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_POSITION;
    m.x = ned_position[0];
    m.y = ned_position[1];
    m.z = ned_position[2];
  }
  else{
    //LOGASSERT_MSG(!std::isnan(yaw), "Not implemented yet!");
    m.coordinate_frame = MAV_FRAME_LOCAL_NED;
    m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_POSITION
        & MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_YAW_ANGLE;
    Eigen::Vector3d ned_position = ftf::transform_frame_enu_ned( Eigen::Vector3d(x,y,z) );
    m.x = (float)ned_position[0];
    m.y = (float)ned_position[1];
    m.z = (float)ned_position[2];
    //Eigen::Quaterniond q_ned = ftf::transform_orientation_enu_ned(ftf::transform_orientation_baselink_aircraft(getQuaternionFromRPY(0,0,yaw)) );
    Eigen::Quaterniond q_tmp;
    getQuaternionFromRPY(0,0,yaw, q_tmp);
    Eigen::Quaterniond q_ned = ftf::transform_orientation_enu_ned(ftf::transform_orientation_baselink_aircraft(q_tmp) );
    m.yaw = (float)ftf::quaternion_get_yaw(q_ned);
    LOGPRINT_DEBUG("Set position x,y,z,yaw: %f, %f, %f, %f", m.x, m.y, m.z, m.yaw);
  }

  setCurrentLocalNEDCommand(m);

}

void MAVController::commandLocalPositionENU(const Eigen::Vector3d& position) {
  commandLocalPositionENU(position[0], position[1], position[2]);
}

void MAVController::commandLocalPositionENU(const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation) {
  commandLocalPositionENU(position[0], position[1], position[2], ftf::quaternion_get_yaw(orientation) );
}


void MAVController::commandVelocity(double vx, double vy, double vz, double vyaw) {
  if(!cmd_enable_){
    if(!startCommand())
      return;
  }

  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
  if (std::isnan(vyaw)){

    m.coordinate_frame = MAV_FRAME_BODY_NED;
    m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_VELOCITY;
    Eigen::Vector3d aircraft_vel = ftf::transform_frame_baselink_aircraft( Eigen::Vector3d(vx,vy,vz) );
    //LOGPRINT_DEBUG("Velocity Command After Transform: %lf %lf %lf",aircraft_vel[0], aircraft_vel[1], aircraft_vel[2]);
    m.vx = (float)aircraft_vel[0];
    m.vy = (float)aircraft_vel[1];
    m.vz = (float)aircraft_vel[2];
  }
  else{
    // LOGASSERT_MSG(!std::isnan(vyaw), "Not implemented yet!");
    //TODO -- Frame might not correct
    m.coordinate_frame = MAV_FRAME_BODY_NED;
    m.type_mask = 0b0000010111000111; // from mav ros
    Eigen::Vector3d aircraft_vel = ftf::transform_frame_baselink_aircraft( Eigen::Vector3d(vx,vy,vz) );
    m.vx = (float)aircraft_vel[0];
    m.vy = (float)aircraft_vel[1];
    m.vz = (float)aircraft_vel[2];
    m.yaw_rate = ftf::transform_frame_baselink_aircraft( Eigen::Vector3d(0,0,vyaw) )[2];
    //m.yaw_rate = -vyaw;
    LOGPRINT_DEBUG("Velocity+Yaw Command: %lf %lf %lf %lf", aircraft_vel[0], aircraft_vel[1],
                   aircraft_vel[2], (double)m.yaw_rate);
  }

  setCurrentLocalNEDCommand(m);
}

void MAVController::commandSetMaxXYVelocity(double limit) {
  mavlink_msgs::PARAM_SET m;

  m.target_system = sys_id_;
  m.target_component = autopilot_id_;
  std::array<char, 16> tmp = {"MPC_XY_VEL_MAX"};
  m.param_id = tmp;
  m.param_value = (float)limit;
  m.param_type = 9; // FLOAT 32
  writeMessage(m);
}

void MAVController::commandResetMaxXYVelocity() {
  mavlink_msgs::PARAM_SET m;

  m.target_system = sys_id_;
  m.target_component = autopilot_id_;
  std::array<char, 16> tmp = {"MPC_XY_VEL_MAX"};
  m.param_id = tmp;
  m.param_value = 12.0f;
  m.param_type = 9; // FLOAT 32
  writeMessage(m);
}


void MAVController::commandBreak() {
  commandVelocity(0,0,0);
}

bool MAVController::commandArm(bool arm){
  mavlink_msgs::COMMAND_LONG cmd;
  cmd.target_system = sys_id_;
  cmd.target_component = autopilot_id_;
  cmd.command = static_cast<uint16_t >(MAV_CMD::COMPONENT_ARM_DISARM);
  cmd.confirmation = 1;
  if(hb_st_.arm == arm){
    LOGPRINT_WARN("Warning, the system is already %s, abort this command", (arm?"ARMED":"DISARMED") );
    return false;
  }
  else{
    cmd.param1 = (float) arm;
    return writeMessage(cmd);
  }
}

void MAVController::commandReturnToLaunch() {
  mavlink_msgs::COMMAND_LONG cmd;
  cmd.target_system = sys_id_;
  cmd.target_component = autopilot_id_;
  cmd.command = static_cast<uint16_t >(MAV_CMD::NAV_RETURN_TO_LAUNCH);
  cmd.confirmation = 1;
  writeMessage(cmd);
}

void MAVController::commandTakeOff(double desired_altitude) {
  if(!cmd_enable_){
    if(!startCommand())
      return;
  }
  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
  m.coordinate_frame = MAV_FRAME_LOCAL_NED;
  m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_TAKE_OFF;
  //m.vz = -desired_velocity;
  {
    std::lock_guard<std::mutex> lock(pose_vel_ned_mutex_);
    m.x = position_ned_[0];
    m.y = position_ned_[1];
  }

  m.z = -desired_altitude;

  setCurrentLocalNEDCommand(m);
}

bool MAVController::commandLandAtCurrentLocation(double desired_speed, double land_speed_bound,
                                                 double land_check_duration, double timeout, bool auto_disarm) {
  if(!cmd_enable_){
    if(!startCommand())
      return false;
  }

  std::chrono::milliseconds period_millis(200);
  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
  m.coordinate_frame = MAV_FRAME_BODY_NED;
  m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LANDING;
  m.vx = 0;
  m.vy = 0;
  m.vz = desired_speed;
  setCurrentLocalNEDCommand(m);
  // TODO -- if landded should disable offboard control automatically
  // Otherwise if we left this for a while  the drone will crash

  // Blocking until Land

  utils::Timer timer;

  utils::Timer timeout_timer;
  timeout_timer.start();
  while (cmd_enable_){
    if(timeout_timer.elapsedSeconds() > timeout){
      LOGPRINT_ERROR("TIMEOUT Abort Landing !");
      commandBreak();
      return false;
    }

    // Get current going down velocity
    double vz = vel_linear_robot_[2];
    // if -land_speed_bound < vz=0 < land_speed_bound for some duration will stop commanding
    if ( (std::abs(vz) <= std::abs(land_speed_bound) ) && !timer.isStart() ){
      timer.start();
    }
    else if ( (std::abs(vz) <= std::abs(land_speed_bound) ) && timer.isStart()){
      if(timer.elapsedSeconds() > land_check_duration){
        stopCommand();
      }
    }
    std::this_thread::sleep_for(period_millis);
  }

  // Delay for a bit
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  if(auto_disarm){
    commandArm(false);
  }

  return true;
}


//bool MAVController::writeLocalPositionNED(double x_ned, double y_ned, double z_ned) {
//  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
//  m.x = (float)x_ned;
//  m.y = (float)y_ned;
//  m.z = (float)z_ned;
//  m.coordinate_frame = MAV_FRAME_LOCAL_NED;
//
//  m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_POSITION;
//  return writeMessage(m);
//}

bool MAVController::writeCurrentLocalNEDCommand() {
  std::lock_guard<std::mutex> lock(cmd_local_ned_mutex_);

  if(is_init_ && cmd_enable_){
    //cmd_local_ned_.target_system = sys_id_;
    //cmd_local_ned_.coordinate_frame = comp_id_;

    //LOGPRINT_DEBUG("Writing LOCAL NED Command: %s", cmd_local_ned_.to_yaml().c_str()  );
    if (!writeMessage(cmd_local_ned_)){
      LOGPRINT_WARN("Cannot Write Local NED Command");
      return false;
    }
    return true;
  }
  else{
    LOGPRINT_WARN("Cannot Write Local NED Command: Command Thread not enable");
    return false;
  }


}

void MAVController::setCurrentLocalNEDCommand(mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED &m) {
//  if(!cmd_enable_){
//    LOGPRINT_WARN("Command Thread is not start -> This setpoint action might take no effect");
//  }
  std::lock_guard<std::mutex> lock(cmd_local_ned_mutex_);

  m.target_component = autopilot_id_;
  m.target_system = sys_id_;
  //m.time_boot_ms = (uint32_t) (get_time_usec()/1000);
  cmd_local_ned_ = m;
  //LOGPRINT_DEBUG("There %s", cmd_local_ned_.to_yaml().c_str());
}




bool MAVController::isArmed(){
  return hb_st_.arm;
}

bool MAVController::isAutoGuidedModeEnable() {
  return hb_st_.auto_enable;
}

bool MAVController::isCommandEnable() {
  return cmd_enable_;
}

bool MAVController::writeMessage(mavlink::Message &m) {
  try{
    connect_->send_message(m);
    return true;
  }
  catch (const std::exception& e){
    LOGPRINT_WARN("TX buffer overflow");
    return false;
  }
}

bool MAVController::startCommand() {
  //if (!cmd_enable_){
    if (is_init_ && (!cmd_enable_) ) {
      // TODO -- Send offboard enable

      //if(!hb_st_.auto_enable)
      writeOffBoardControl(true);

      if(hb_st_.arm){
        LOGPRINT_INFO("Start Command Thread");
        cmd_enable_ = true;
        cmd_thread_ = std::make_shared<std::thread>(&MAVController::commandThread, this);
      }
      else{
        LOGPRINT_WARN("The Component is not armed");
        cmd_enable_ = false;
      }

      // return true;
    }
    else {
      cmd_enable_ = false;
      LOGPRINT_WARN("Could not Start Command thread");
      // return false;
    }
  //}

  return cmd_enable_;
}

void MAVController::stopCommand() {
  if(cmd_enable_){
    cmd_enable_ = false;
    cmd_thread_->join();
    cmd_thread_.reset(); // delete thread
    writeManualBreak();
    if(!writeOffBoardControl(false))
      LOGPRINT_ERROR("Cannot disable offboard control properly");

  }
}

bool MAVController::writeOffBoardControl(bool enable) {
  mavlink_msgs::COMMAND_LONG cmd;
  cmd.target_system = sys_id_;
  cmd.target_component = autopilot_id_;
  cmd.command = static_cast<uint16_t >(MAV_CMD::NAV_GUIDED_ENABLE);
  cmd.confirmation = 1;
  cmd.param1 = (float) enable;
  //LOGPRINT_WARN("Send request offboard control %s", cmd.to_yaml().c_str());
  return writeMessage(cmd);
}

void MAVController::commandThread(){
  std::chrono::milliseconds cmd_period_millis((int)((1.0/cmd_rate_)*1000.0)); //command rate in hz
  //Ensure zero velocity before connect

  //writeManualBreak();

  while(!is_shutdown_ && cmd_enable_){
    // LOGPRINT_WARN("HELLO WORLD");
    writeCurrentLocalNEDCommand();
    // TODO -- Automatically destroy this thread when disarming, now throws exception when reinitialize new thread
//    if(!hb_st_.arm){
//      LOGPRINT_WARN("Disarming will Stop Command Thread");
//      cmd_enable_ = false;
//    }
    std::this_thread::sleep_for(cmd_period_millis);
  }

  //Ensure zero velocity before disconnect
  // TODO -- Change this to loiter if disconnecting

}

void MAVController::writeManualBreak(){
  mavlink_msgs::SET_POSITION_TARGET_LOCAL_NED m;
  m.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_VELOCITY;
  m.coordinate_frame = MAV_FRAME_LOCAL_NED;
  m.vx = 0;
  m.vy = 0;
  m.vz = 0;
  setCurrentLocalNEDCommand(m);
  writeCurrentLocalNEDCommand();
}


// Example Taking off
//printf("Mode TAKEOFF\n");
//float precision_distance = 0.1; // [m]
//mavlink_set_position_target_local_ned_t sp_target;
//sp_target.vx = 0;
//sp_target.vy = 0;
//sp_target.vz = -velocity;
//sp_target.z = -height;
//sp_target.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_TAKEOFF;
//sp_target.coordinate_frame = MAV_FRAME_LOCAL_OFFSET_NED;
}