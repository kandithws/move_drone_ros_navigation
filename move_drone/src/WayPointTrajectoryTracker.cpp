//
// Created by kandithws on 21/3/2561.
//

#include <move_drone/WayPointTrajectoryTracker.h>

#define WPTT_NAME "WPTRACKER"

namespace move_drone{




WayPointTrajectoryTracker::Path::Path() {

}

WayPointTrajectoryTracker::Path::Path(size_t s) :
p(s), q(s)
{

}

WayPointTrajectoryTracker::Pose WayPointTrajectoryTracker::Path::getPose(size_t i) {
  return Pose( p[i], q[i]);
}

void WayPointTrajectoryTracker::Path::setPose(size_t i, Pose &pose) {
  p[i] = pose.first;
  q[i] = pose.second;
}


WayPointTrajectoryTracker::WayPointTrajectoryTracker(MAVController::Ptr &controller, MODE mode) {
  LOGASSERT_MSG(controller, "Controller is not initialized");
  LOGPRINT_WARN_NAMED(WPTT_NAME, "START!");
  if (mode == MODE::VELOCITY_COMMAND) {
    throw std::logic_error("Velocity Not yet implemented");
    assert(false);
  }
  controller_ = controller;
  //holonomic_ = holonomic;

  // curr_waypoint_it_ = path_.end();
  is_shutdown_ = false;

}

WayPointTrajectoryTracker::~WayPointTrajectoryTracker() {
  if (!controller_->isCommandEnable()) {
    controller_->startCommand();
  }

  if (is_tracking_)
    stopTrackPath();

  if (tracking_thread_) {
    is_shutdown_ = true;
    tracking_thread_->join();
  }
}

bool WayPointTrajectoryTracker::isGoalReached() {
  std::lock_guard<std::mutex> lock(path_mutex_);
  //return curr_waypoint_it_ == path_.end();
  // return curr_waypoint_it_ == path_.size();
  return curr_waypoint_it_ == path_.p.size();
}

bool WayPointTrajectoryTracker::isTracking(){
  return is_tracking_;
}

bool WayPointTrajectoryTracker::isTrackingTimeout(){
  return timeout_status_;
}

void WayPointTrajectoryTracker::trackPath(Path &desired_path) {
  // Copy new path
  std::lock_guard<std::mutex> lock(path_mutex_);
  LOGASSERT_MSG(desired_path.p.size() == desired_path.q.size(), "Poses (Position and Orientation) of 2 vectors must be the same");
  path_.p.assign(desired_path.p.begin(), desired_path.p.end());
  path_.q.assign(desired_path.q.begin(), desired_path.q.end());

  assert(path_.p.size() == desired_path.p.size());
  assert(path_.q.size() == desired_path.q.size());
  curr_waypoint_it_ = 0;
  goal_waypoint_it_ = path_.p.size() - 1;
  LOGPRINT_WARN_NAMED(WPTT_NAME, "START TRACKING");

  // Execute tracking
  timeout_status_ = false;
  tracking_state_ = 0;
  timeout_increment_ = 0;
  is_shutdown_ = false;
  is_tracking_ = true;

  // Fire-up Asynchronous thread for trajectory tracking
  if (!tracking_thread_)
    tracking_thread_ = std::make_shared<std::thread>(&WayPointTrajectoryTracker::trackingThread, this);

}

bool WayPointTrajectoryTracker::getCurrentSetPoint(Eigen::Vector3d& outpose, Eigen::Quaterniond& outq)   {
  std::lock_guard<std::mutex> lock(path_mutex_);
  bool goal_reached = (curr_waypoint_it_ == path_.p.size()) && (is_tracking_);
  if(!goal_reached){
    outpose = path_.p[curr_waypoint_it_]; 
    outq    = path_.q[curr_waypoint_it_];
  }

  return !goal_reached;
}

void WayPointTrajectoryTracker::stopTrackPath() {
  // Stop Tracking thread
  if (is_tracking_) {
    is_tracking_ = false;
    controller_->commandBreak();
  }

  if (tracking_thread_ ){
    is_shutdown_ = true;
    tracking_thread_->join();
    tracking_thread_.reset(); // delete thread instance
  }
}

void WayPointTrajectoryTracker::trackingThread() {
  double control_period = 1.0 / control_rate_;
  std::chrono::milliseconds control_period_millis((int) ((control_period) * 1000.0)); // update rate in hz
  while (!is_shutdown_) {
    if (is_tracking_) {
      std::lock_guard<std::mutex> lock(path_mutex_);
      // Get current pose feedback
      Eigen::Vector3d current_position;
      Eigen::Quaterniond current_orientation;
      controller_->getRobotFrameTransform(current_position, current_orientation);
      // This section applied "Follow The Carrot" planner, similar to http://wiki.ros.org/asr_ftc_local_planner
      // Position Mode
      if(mode_ == MODE::POSITION_COMMAND){
        // TODO -- implement time out and interupt
        if(tracking_state_ == 0){
          // Set Position at the initial waypoint
          controller_->commandLocalPositionENU(path_.p[curr_waypoint_it_], path_.q[curr_waypoint_it_]);
          // State Change condition
          if( ( calcDistance2D(path_.p[curr_waypoint_it_], current_position) <= initial_tolerance_dist_ )
              && (calcYawError(path_.q[curr_waypoint_it_], current_orientation) <= initial_tolerance_heading_ ) ){
            tracking_state_++;
          }


        }
        else if(tracking_state_ == 1){
          // Follow the carrot!!!!!!!!!!!!!!!
          // Reference from the link
          size_t sim_it = checkMaxDistance(current_position); //originally checkMaxDistance
          if(rotate_to_global_plan_){
            rotate_to_global_plan_ = rotateToOrientation(path_.q[sim_it], current_position, current_orientation, initial_tolerance_heading_);
          }
          else{
            // Move along the path otherwise
            double distance_to_goal = calcDistance2D(path_.p[goal_waypoint_it_], current_position);
            if(distance_to_goal > goal_tolerance_dist_){
              // Recheck if the rotation still feasible
              if(fabs(calcYawError(path_.q[sim_it], current_orientation) ) > 1.2){
                LOGPRINT_WARN_NAMED(WPTT_NAME, "Starting to loss track orientation, Rotate in place");
                rotate_to_global_plan_ = true;
              }

              // Drive the robot to the desired pose, and desired velocity
              size_t sim_sub_goal = driveToward(current_position, current_orientation);

              // TODO -- Check Collision along the way to the target goal (sim_sub_goal)

              // Collision Checking pass then update current way point, and command

              if(curr_waypoint_it_ == sim_sub_goal ){
                timeout_increment_++;
              }
              else{
                timeout_increment_ = 0;
              }

              if ( (static_cast<double>(timeout_increment_) * control_period ) > tracking_timeout_ ){
                LOGPRINT_ERROR_NAMED(WPTT_NAME, "Abort Tracking, tracker stop changing waypoints for %lf seconds",
                                     tracking_timeout_);
                timeout_status_ = true;
                is_tracking_ = false;
                timeout_increment_ = 0;
              }

              curr_waypoint_it_ = sim_sub_goal;
              controller_->commandLocalPositionENU(path_.p[sim_sub_goal], path_.q[sim_sub_goal]);
            }
            else{
              tracking_state_++;
            }
          }
        }
        else if(tracking_state_ == 2){
          LOGPRINT_INFO_NAMED(WPTT_NAME, "Reaching Goal");
          controller_->commandLocalPositionENU(path_.p[goal_waypoint_it_], path_.q[goal_waypoint_it_]);
          curr_waypoint_it_ = goal_waypoint_it_;
          tracking_state_++;
        }
        else{
          LOGPRINT_INFO_NAMED(WPTT_NAME, "GOAL REACHED!");
          curr_waypoint_it_++;
          tracking_state_ = 0;
          timeout_increment_ = 0;
          is_tracking_ = false;
        }
      } // end if MODE::POSITION_COMMAND

      else if (mode_ == MODE::VELOCITY_COMMAND){
        LOGASSERT_MSG(!(mode_ == MODE::VELOCITY_COMMAND), "Not Implement!");
      } // end if MODE::VELOCITY_COMMAND
    } // end if is_tracking_

    // We have to put it here because thread sleep must be consistency
    std::this_thread::sleep_for(control_period_millis);

  }
  // state_tracking_ = false;
}

// Looking Path forward
size_t WayPointTrajectoryTracker::checkMaxDistance(Eigen::Vector3d& curr_position) {
  // extrapolate path based on sim_time (looking forward time) and max_x_vel
  size_t it = curr_waypoint_it_;
  //Path::iterator it = path_.begin();
  double sim_dist = max_x_vel_ * sim_time_;
  while (it < path_.p.size()){
    double dist = calcDistance2D(path_.p[it], curr_position);
    if(dist <= sim_dist){
      it++;
    }
    else {
      break;
    }
  }
  if ( !(it == 0) ){
    it = it - 1;
  }
  return it;
}


size_t WayPointTrajectoryTracker::checkMaxAngle(size_t init_wp, Eigen::Vector3d& current_position,
                                                Eigen::Quaterniond& current_orientation){
  size_t it = init_wp;
  double sim_angle = max_rot_vel_ * sim_time_;
  while(it >= 0){
    double diff_angle =  fabs(calcYawError(current_orientation, path_.q[it]));
    if(diff_angle > sim_angle){
      it--;
    }
    else {
      break;
    }
  }

  if (it  < 0){
    it = 0;
  }

  return it;
}


size_t WayPointTrajectoryTracker::driveToward(Eigen::Vector3d& current_position, Eigen::Quaterniond& current_orientation){
  size_t sub_goal = checkMaxDistance(current_position);
  size_t sub_goal2 = checkMaxAngle(sub_goal, current_position, current_orientation);
  return sub_goal2;
}


bool WayPointTrajectoryTracker::rotateToOrientation(Eigen::Quaterniond& goal_orientation, Eigen::Vector3d& current_position,
                                                    Eigen::Quaterniond& current_orientation,double accuracy) {
  // Command Rotate in place until reach
  // False if goal reached
  controller_->commandLocalPositionENU(current_position, goal_orientation);
  return !(fabs( calcYawError(goal_orientation, current_orientation) ) < accuracy);
}

double WayPointTrajectoryTracker::calcDistance2D(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2) {
  double dx = p1[0] - p2[0];
  double dy = p1[1] - p2[1];
  return sqrt( dx*dx + dy*dy );
}


double WayPointTrajectoryTracker::calcDistance3D(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2) {
  double dx = p1[0] - p2[0];
  double dy = p1[1] - p2[1];
  double dz = p1[2] - p2[2];
  return sqrt( dx*dx + dy*dy + dz*dz);
}

double WayPointTrajectoryTracker::normalize(double z){
  return atan2(sin(z), cos(z));
}

double WayPointTrajectoryTracker::angle_diff(double a, double b){
  double d1, d2;
  a = normalize(a);
  b = normalize(b);
  d1 = a - b;
  d2 = 2.0 * M_PI - fabs(d1);
  if (d1 > 0)
    d2 *= -1.0;
  if (fabs(d1) < fabs(d2))
    return (d1);
  else
    return (d2);
}
 


double WayPointTrajectoryTracker::calcYawError(Eigen::Quaterniond& p1, Eigen::Quaterniond& p2) {
  return angle_diff(ftf::quaternion_get_yaw(p1), ftf::quaternion_get_yaw(p2));
}

}
