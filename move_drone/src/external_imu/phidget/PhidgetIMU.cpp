//
// Created by kandithws on 11/9/2561.
//

#include <move_drone/external_imu/phidget/PhidgetIMU.h>
using namespace phidgets;

namespace move_drone {

PhidgetIMU::PhidgetIMU(int queue_size) : Imu(), q_(queue_size)
{}

bool PhidgetIMU::setZeroGyroscope() {
  if(is_connected_){
    zero();
  }
  return is_connected_;
}

void PhidgetIMU::setStreamingRate(int period) {
  stream_period_ = period;
  if(is_connected_)
    setDataRate(stream_period_);
}

void PhidgetIMU::startStreaming() {

  if (!imu_cb_){
    LOGPRINT_ERROR_NAMED("PhidgetsIMU", "Callback function is required, currently not support queue interface");
    throw std::runtime_error("Callback not register for PhidgetsIMU");
  }

  open(serial_number_); // attach here
  LOGPRINT_INFO_NAMED("PhidgetsIMU","Waiting for IMU .....");
  int result = waitForAttachment(10000); //in ms
  if(result){
    is_connected_ = false;
    const char *err;
    CPhidget_getErrorDescription(result, &err);
    LOGPRINT_ERROR_NAMED("PhidgetsIMU", "Connection error: %s", err);
    throw std::runtime_error("Phidget IMU Connection Timeout");
  }
  // synchronise time since boot
  zero();
  stamp_init_ = utils::get_std_time_now_since_epoch();
  // Reset zero Gyroscope
  LOGPRINT_INFO_NAMED("PhidgetsIMU","Connecting IMU at %d", stream_period_);

  user_cb_dispatch_thread_ = boost::make_shared<boost::thread>(boost::bind(&PhidgetIMU::dispatchUserCallbackThread,
                                                                           this));

}

void PhidgetIMU::attachHandler() {
  Imu::attachHandler();
  is_connected_ = true;
  if (error_number_ == 13) error_number_ = 0;
  //diag_updater_.force_update()
  setDataRate(stream_period_);
}

void PhidgetIMU::detachHandler() {
  Imu::detachHandler();
  is_connected_ = false;
}

void PhidgetIMU::errorHandler(int error) {
  Imu::errorHandler(error);
  error_number_ = error;
}

void PhidgetIMU::stopStreaming() {
  close();
  is_connected_ = false; // Force because detach handler doesn't invoke properly 
  if(imu_cb_)
    q_cv_.notify_all();
  user_cb_dispatch_thread_->join();
  user_cb_dispatch_thread_.reset();
}

bool PhidgetIMU::isConnected() const {
  return is_connected_;
}

void PhidgetIMU::dataHandler(CPhidgetSpatial_SpatialEventDataHandle *data, int count) {
  for(int i = 0; i < count; i++)
    pushDataQueue(data[i]);
}

void PhidgetIMU::pushDataQueue(CPhidgetSpatial_SpatialEventDataHandle &data) {
  std::chrono::seconds stamp_sec(data->timestamp.seconds);
  std::chrono::microseconds stamp_usec(data->timestamp.microseconds);

  std::chrono::nanoseconds stamp_imu = stamp_sec + stamp_usec;
  std::chrono::nanoseconds stamp_now = stamp_init_ + stamp_imu;

  std::chrono::duration<double> time_diff = stamp_now - utils::get_std_time_now_since_epoch();
  if(fabs(time_diff.count()) > max_stamp_diff_ ){
    LOGPRINT_WARN_NAMED("PhidgetsIMU", "IMU time lags behind by %lf seconds, reseting IMU time offset", time_diff.count());
    stamp_now = utils::get_std_time_now_since_epoch();
    stamp_init_ = stamp_now - stamp_imu;
  }

  boost::shared_ptr<IMUData> msg = boost::make_shared<IMUData>();
  msg->stamp = stamp_now;

#ifdef PHIDGETS_FLIP_ACCEL_SYMBOL
  msg->accel[0] = - data->acceleration[0] * g_; // raw come as g
  msg->accel[1] = - data->acceleration[1] * g_;
  msg->accel[2] = - data->acceleration[2] * g_;
#else
  msg->accel[0] = data->acceleration[0] * g_; // raw come as g
  msg->accel[1] = data->acceleration[1] * g_;
  msg->accel[2] = data->acceleration[2] * g_;
#endif
  msg->gyro[0] = data->angularRate[0] * (M_PI / 180.0); // raw come as deg/s
  msg->gyro[1] = data->angularRate[1] * (M_PI / 180.0);
  msg->gyro[2] = data->angularRate[2] * (M_PI / 180.0);

  if (data->magneticField[0] != PUNK_DBL){
    msg->mag[0] = data->magneticField[0] * 1e-4;
    msg->mag[1] = data->magneticField[1] * 1e-4;
    msg->mag[2] = data->magneticField[2] * 1e-4;
  }
  else{
    double nan = std::numeric_limits<double>::quiet_NaN();
    msg->mag[0] = nan;
    msg->mag[1] = nan;
    msg->mag[2] = nan;
  }

  {
    boost::mutex::scoped_lock lock(q_mutex_);
    q_.push_back(msg);
  }
  //if (imu_cb_)
  //  imu_cb_(msg);
  if(imu_cb_)
    q_cv_.notify_all();
}

void PhidgetIMU::dispatchUserCallbackThread() {
  assert(imu_cb_);
  while (is_connected_){
    boost::unique_lock<boost::mutex> lock(q_mutex_);
    q_cv_.wait(lock, [this]{ return !q_.empty() || !is_connected_; });
    if(!q_.empty()){
      boost::shared_ptr<IMUData> msg = q_.front();
      q_.pop_front();
      lock.unlock();
      imu_cb_(*msg);
    }
  }

}

}
