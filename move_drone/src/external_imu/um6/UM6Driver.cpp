//
// Created by kandithws on 25/7/2561.
//

#include <move_drone/external_imu/um6/UM6Driver.h>
#include <serial/serial.h>

namespace move_drone {

UM6Driver::UM6Driver(std::string port, uint32_t baud_rate, size_t queue_size) :
    data_queue_(queue_size) {
  serial_.setPort(port);
  serial_.setBaudrate(baud_rate);
  serial::Timeout to = serial::Timeout(50, 50, 0, 50, 0);
  serial_.setTimeout(to);
}

void UM6Driver::registerIMUCallback(const std::function<void(IMUDataPtr)> &cb) {
  imu_cb_ = cb;
}

bool UM6Driver::setBoardcastRate(int rate) {
  // Only allow to set on shutdown
  if ((rate >= 20) && (rate <= 300) && (is_shutdown_)) {
    board_cast_rate_ = rate;
    return true;
  }
  return false;
}

//bool UM6Driver::setZeroGyroscopes() {
//  um6::Registers r;
//  return sendCommand(r.cmd_zero_gyros);
//}

void UM6Driver::setCallbackEventTriggerPacket(uint8_t trigger_pkt) {
  trigger_packet_ = trigger_pkt;
}

void UM6Driver::configureSensor(um6::Comms* sensor) {
  um6::Registers r;
  const uint8_t UM6_BAUD_115200 = 0x5;
  uint32_t comm_reg = UM6_BROADCAST_ENABLED |
      UM6_GYROS_PROC_ENABLED | UM6_ACCELS_PROC_ENABLED | UM6_MAG_PROC_ENABLED |
      UM6_QUAT_ENABLED | UM6_EULER_ENABLED | UM6_COV_ENABLED | UM6_TEMPERATURE_ENABLED |
      UM6_BAUD_115200 << UM6_BAUD_START_BIT;

  //if (required_cov_estimate_)
  //  comm_reg |= UM6_COV_ENABLED;
  // set the broadcast rate of the device


  uint32_t rate_bits = (uint32_t) ((board_cast_rate_ - 20) * 255.0 / 280.0);
  comm_reg |= (rate_bits & UM6_SERIAL_RATE_MASK);
  serial_.flush();
  r.communication.set(0, comm_reg);
  if (!sensor->sendWaitAck(r.communication)) {
    throw std::runtime_error("Unable to set communication register.");
  }
  uint32_t misc_config_reg = UM6_QUAT_ESTIMATE_ENABLED | UM6_MAG_UPDATE_ENABLED | UM6_ACCEL_UPDATE_ENABLED;
  serial_.flush();
  r.misc_config.set(0, misc_config_reg);
  if (!sensor->sendWaitAck(r.misc_config)) {
    throw std::runtime_error("Unable to set misc config register.");
  }


  if (reset_gyro_on_start_) sendCommand(sensor, r.cmd_zero_gyros);


  // TODO -- Configure references and Bias

}

void UM6Driver::startStreaming(bool reset_gyro) {
  // TODO -- Seperate UM6 connection as a seperate thread + retry connection every 1 sec
  // Then notify readThread() and callbackThread() through condition variable
  reset_gyro_on_start_ = reset_gyro;
  try {
    serial_.open();
  }
  catch (const serial::IOException &e) {
    LOGPRINT_ERROR_NAMED("UM6Driver", "Cannot Open Serialport: %s", e.what());
    throw e;
  }

  boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
  if (serial_.isOpen()) {
    LOGPRINT_INFO_NAMED("UM6Driver", "Successfully Open Serial Port");
    is_shutdown_ = false;

    // Start thread
    read_thread_.reset();
    read_thread_ = boost::make_shared<boost::thread>(boost::bind(&UM6Driver::readThread, this));
    if(imu_cb_){
      callback_thread_.reset();
      callback_thread_ = boost::make_shared<boost::thread>(boost::bind(&UM6Driver::callbackThread, this));
    }
    else{
      LOGPRINT_WARN_NAMED("UM6Driver", "Callback is not registered!");
    }



  } else {
    LOGPRINT_ERROR_NAMED("UM6Driver", "Fail to open serial port");
    throw std::runtime_error("Fail to open serial port");
  }

}


void UM6Driver::stopStreaming() {
  if(!is_shutdown_){
    is_shutdown_ = true;
    read_thread_->join();
    serial_.close();
    // consumer thread -> join()
  }
  boost::mutex::scoped_lock lock(data_queue_mutex_);
  data_queue_.clear();
}

void UM6Driver::readThread() {
  // Maybe, don't have to sleep the thread
  //boost::chrono::milliseconds update_duration(static_cast<int>((1.0 / board_cast_rate_) * 1000));
  um6::Registers r;
  //sensor_ = boost::make_shared<um6::Comms>(&serial_);
  um6::Comms sensor(&serial_);
  configureSensor(&sensor);
  LOGPRINT_INFO_NAMED("UM6", "Start Connection to UM6");
  while (!is_shutdown_) {
    //LOGPRINT_DEBUG_NAMED("UM6", "I AM HERE");
    int16_t tmp_pkt = sensor.receive(&r);
    std::cout << tmp_pkt << std::endl;
    if (tmp_pkt == trigger_packet_) {

      LOGPRINT_DEBUG_NAMED("UM6", "I Got Data!");
      std::chrono::nanoseconds stamp = utils::get_std_time_now_since_epoch();
      IMUDataPtr data = boost::make_shared<IMUData>();
      data->stamp = stamp;

      data->q[0] = r.quat.get_scaled(1); //q.x
      data->q[1] = r.quat.get_scaled(2); //q.y
      data->q[2] = r.quat.get_scaled(3); //q.z
      data->q[3] = r.quat.get_scaled(0); //q.w

      data->gyro[0] = r.gyro.get_scaled(0); //x
      data->gyro[1] = r.gyro.get_scaled(1); //y
      data->gyro[2] = r.gyro.get_scaled(2); //z

      data->accel[0] = r.accel.get_scaled(0);
      data->accel[1] = r.accel.get_scaled(1);
      data->accel[2] = r.accel.get_scaled(2);

      data->mag[0] = r.mag.get_scaled(0);
      data->mag[1] = r.mag.get_scaled(1);
      data->mag[2] = r.mag.get_scaled(2);

      data->temperature = r.temperature.get_scaled(0);

      if (required_cov_estimate_) {
        data->q[0] = r.covariance.get_scaled(5);
        data->q[1] = r.covariance.get_scaled(6);
        data->q[2] = r.covariance.get_scaled(7);
        data->q[3] = r.covariance.get_scaled(9);
        data->q[4] = r.covariance.get_scaled(10);
        data->q[5] = r.covariance.get_scaled(11);
        data->q[6] = r.covariance.get_scaled(13);
        data->q[7] = r.covariance.get_scaled(14);
        data->q[8] = r.covariance.get_scaled(15);
      }

      // TODO -- push to callback queue
      pushIMUDataQueue(data);
    }

    //boost::this_thread::sleep_for(update_duration);
  }
}

void UM6Driver::callbackThread() {
  //boost::chrono::milliseconds update_duration(static_cast<int>((1.0 / (board_cast_rate_)) * 1000));
  //boost::chrono::microseconds update_duration(static_cast<int>((1.0 / (board_cast_rate_)) * 1000));
  while (!is_shutdown_){
    IMUDataPtr ptr;
    if (getIMUData(ptr)){
      data_queue_.pop_front();
      imu_cb_(ptr);
    }

    //boost::this_thread::sleep_for(update_duration);

  }

}

bool UM6Driver::getIMUData(IMUDataPtr &ptr) {
  boost::mutex::scoped_lock lock(data_queue_mutex_);
  if (!data_queue_.empty()){
    //Pop Queue
    ptr = data_queue_.front();
    if(!imu_cb_){
      // in case we use this with callback, the data will be pop based on callback event, otherwise get the latest
      data_queue_.pop_front();
    }
    return true;
  }
  else{
    return false;
  }
}

void UM6Driver::pushIMUDataQueue(IMUDataPtr &data_ptr) {
  boost::mutex::scoped_lock lock(data_queue_mutex_);
  data_queue_.push_back(data_ptr);
}

}
