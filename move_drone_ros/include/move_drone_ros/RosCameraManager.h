//
// Created by kandithws on 25/7/2561.
//

#ifndef PROJECT_ROSCAMERAMANAGER_H
#define PROJECT_ROSCAMERAMANAGER_H

#include <move_drone/BaseCameraManager.h>
#include <move_drone_ros/common_utils.h>
#include <ros/ros.h>
#include <boost/shared_ptr.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>


namespace move_drone {

/*
 *  Simple ROS Image Subscriber (not using image transport)
 *  This will not create ROS spinner in a instance
 *  User should globally spin ROS node
 *
 * */

class RosCameraManager : public BaseCameraManager {
 public:

  RosCameraManager(ros::NodeHandle& nh, size_t queue_size=10,
                   bool is_compressed=false,
                   std::string topic="/camera/image_raw");
  void setCameraInfoTopic(std::string topic);
  void loadCameraInfo(std::string filename); // overiding
  // TODO -- add static method to convert from ImageStamped -> ROSMSG
//  void startStreaming();
//  void stopStreaming();

 private:
  ros::NodeHandle nh_;
  ros::Subscriber img_sub_;
  size_t ros_queue_size_;
  bool is_compressed_;
  bool camera_info_via_ros_=false;
  std::string camera_info_topic_;
  std::string image_topic_;
  //bool is_streaming_ = false;

  void imageCallback(const sensor_msgs::ImagePtr& msg);
  void imageCompressedCallback(const sensor_msgs::CompressedImagePtr& msg);
  void initStartStreaming();
  void initStopStreaming();


  //ros::AsyncSpinner spinner_;


};

}

#endif //PROJECT_ROSCAMERAMANAGER_H
