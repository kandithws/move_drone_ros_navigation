//
// Created by kandithws on 5/7/2561.
//

#ifndef PROJECT_COMMON_UTILS_H
#define PROJECT_COMMON_UTILS_H
#include <ros/time.h>
#include <chrono>

namespace move_drone_ros {
  static inline ros::Time std_to_ros_time(std::chrono::nanoseconds std_t){
    ros::Time t;
    return t.fromNSec(static_cast<uint64_t >(std_t.count()));
  }

  static inline std::chrono::nanoseconds ros_time_to_std(ros::Time& rt){
    return  std::chrono::nanoseconds(rt.toNSec());
  }

}


#endif //PROJECT_COMMON_UTILS_H
