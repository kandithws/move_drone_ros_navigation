//
// Created by kandithws on 2/6/2561.
//

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/filesystem.hpp>
#include <move_drone/CameraManager.h>
#include <move_drone/MAVController.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/MagneticField.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <queue>

/*
 *  This node strictly use to record a rosbag for move_drone
 *  We will not use rosbag record cli, in order to optimize memory/processing unit (esp. images)
 *
 *
 * */
using namespace move_drone;
static inline ros::Time std_to_ros_time(std::chrono::nanoseconds std_t){
  ros::Time t;
  return t.fromNSec(static_cast<uint64_t >(std_t.count()));
}

class BagRecorderNode{
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  BagRecorderNode():
   nh_("~")
  {
    nh_.param<std::string>("save_path",save_dir_, "");
    save_dir_ = normalizePath(save_dir_);

    if (!boost::filesystem::exists(save_dir_)){
      ROS_ERROR("Directory is not exist!");
      exit(-1);
    }

    std::string port_url;
    nh_.param<std::string>("port_url",port_url, "/dev/ttyUSB0:921600");
    controller_ = boost::make_shared<MAVController>(port_url);
    double pose_update_rate;
    nh_.param("pose_update_rate", pose_update_rate, 30.0);
    controller_->setPoseVelUpdateRate(pose_update_rate);

    std::string cam_device, cam_params_file;
    nh_.param<std::string>("camera_device", cam_device, "camera::/dev/video0");
    int image_q_size;
    nh_.param("image_queue_size", image_q_size ,10);
    cam_manager_ = boost::make_shared<CameraManager>(cam_device, image_q_size);

    if(nh_.hasParam("camera_config_file")){
      nh_.param<std::string>("camera_config_file", cam_params_file, "");
      cam_manager_->loadCameraInfo(cam_params_file);
    }
    else{
      ROS_WARN("No Camera Config Loaded!");
    }

    nh_.param("compress_image", compress_image_, true);
    nh_.param("publish_data", publish_data_, false);



    if (publish_data_){
      odom_pub_ = nh_.advertise<nav_msgs::Odometry>(odom_topic_,1);
      if(compress_image_){
        img_pub_ = nh_.advertise<sensor_msgs::CompressedImage>(compressed_camera_topic_,1);
      }else{
        img_pub_ = nh_.advertise<sensor_msgs::Image>(camera_topic_,1);
      }

    }

  }


  ~BagRecorderNode(){
    stopRecording();
  }

  void execute(){
    toggle_bag_recorder_srv_ = nh_.advertiseService("/toggle_recorder", &BagRecorderNode::toggleRecorderCallback, this);
    ros::spin();
  }

 private:
  ros::NodeHandle nh_;
  bool publish_data_ = false; // For debugging purpose
  std::string camera_topic_ = "/camera/image_raw";
  std::string compressed_camera_topic_ = "/camera/image/compressed";
  std::string odom_topic_ = "/odom";
  std::string save_dir_ = "~";
  bool is_start_recording_ = false;

  ros::ServiceServer toggle_bag_recorder_srv_;


  boost::shared_ptr<boost::thread> camera_thread_; // no need to read, CamManager will manage for us
  boost::shared_ptr<boost::thread> controller_read_thread_; // query at 30 Hz
  boost::shared_ptr<boost::thread> controller_write_thread_;
  boost::shared_ptr<boost::thread> imu_write_thread_; // no need to read, it will come as a Callback

  boost::shared_ptr<MAVController> controller_;
  boost::shared_ptr<CameraManager> cam_manager_;

  ros::Publisher odom_pub_;
  ros::Publisher img_pub_;

  // rosbag::Bag image_bag_;
  rosbag::Bag bag_;
  boost::mutex bag_mutex_;

  std::queue<nav_msgs::OdometryPtr> status_queue_;
  boost::mutex status_queue_mutex_;

  std::queue<sensor_msgs::ImuPtr> imu_queue_;
  std::queue<sensor_msgs::MagneticFieldPtr> mag_queue_;
  boost::mutex imu_mag_queue_mutex_;


  bool compress_image_ = false;

  bool toggleRecorderCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res){
    if(is_start_recording_){
      // Cleaning Up
      ROS_INFO("Clearing Messages Queue Please wait");
      stopRecording();
      ROS_INFO("Done");
    }
    else{
      uint64_t t_stamp = (uint64_t)(ros::Time::now().toBoost() - boost::posix_time::from_time_t(0)).total_nanoseconds();
      std::string out_dir = save_dir_ + "/drone_data_" + std::to_string(t_stamp) + ".bag";
      //boost::filesystem::create_directory(out_dir);
      ROS_INFO("Saving Bagfile at: %s", out_dir.c_str());
      //image_bag_.open( out_dir + "/image.bag", rosbag::bagmode::Write);
      //status_bag_.open( out_dir + "/status.bag", rosbag::bagmode::Write);
      bag_.open( out_dir, rosbag::bagmode::Write);
      is_start_recording_ = true;
      //if (!controller_->imuCallback)
      //  controller_->imuCallback = std::bind(&BagRecorderNode::mavIMUCB, this, std::placeholders::_1, std::placeholders::_2);
      controller_->registerHighResIMUCallback(std::bind(&BagRecorderNode::mavIMUCB, this,
                                                        std::placeholders::_1, std::placeholders::_2));
      camera_thread_ = boost::make_shared<boost::thread>(boost::bind(&BagRecorderNode::cameraThread, this));
      imu_write_thread_ = boost::make_shared<boost::thread>(boost::bind(&BagRecorderNode::imuWriteThread, this));
      controller_read_thread_ = boost::make_shared<boost::thread>(boost::bind(&BagRecorderNode::controllerReadThread, this));
      controller_write_thread_ = boost::make_shared<boost::thread>(boost::bind(&BagRecorderNode::controllerWriteThread, this));
      ROS_INFO(" ----------- Start Recording --------");
    }
    return true;
  }

  void imuWriteThread(){
    ros::Rate rate(100);
    sensor_msgs::ImuPtr m_imu;
    sensor_msgs::MagneticFieldPtr m_mag;
    while (ros::ok() && is_start_recording_){
      bool get_data = false;
      {
        boost::mutex::scoped_lock lock(imu_mag_queue_mutex_);
        if ( !imu_queue_.empty() && !mag_queue_.empty() ){
          m_imu = imu_queue_.front();
          imu_queue_.pop();
          m_mag = mag_queue_.front();
          mag_queue_.pop();
          get_data = true;
        }
      }

      if(get_data)
      {

        boost::mutex::scoped_lock lock(bag_mutex_);
        bag_.write("/imu", m_imu->header.stamp, m_imu);
        bag_.write("/mag", m_mag->header.stamp, m_mag);
        // get_data = false;
      }
      rate.sleep();
    }

    boost::mutex::scoped_lock lock(imu_mag_queue_mutex_);
    // Cleaning up Queue
    while(!imu_queue_.empty() && !mag_queue_.empty()){
      m_imu = imu_queue_.front();
      m_mag = mag_queue_.front();
      imu_queue_.pop();
      mag_queue_.pop();
      {
        boost::mutex::scoped_lock lock(bag_mutex_);
        bag_.write("/imu", m_imu->header.stamp, m_imu);
        bag_.write("/mag", m_mag->header.stamp, m_mag);
      }
    }


  }

  void mavIMUCB(const mavlink_msgs::HIGHRES_IMU& m, const std::chrono::nanoseconds& stamp){

    sensor_msgs::ImuPtr m_imu = boost::make_shared<sensor_msgs::Imu>();
    // body frame, x-front, y-right, z-axis down
    //m_imu->header.stamp = ros::Time::now();
    m_imu->header.stamp = std_to_ros_time(stamp);
    m_imu->header.frame_id = "imu_link";
    m_imu->orientation_covariance[0] = -1; // standard for telling that, imu is not providing the data
    m_imu->angular_velocity.x = m.xgyro;
    m_imu->angular_velocity.y = m.ygyro;
    m_imu->angular_velocity.z = m.zgyro;
    m_imu->linear_acceleration.x = m.xacc;
    m_imu->linear_acceleration.y = m.yacc;
    m_imu->linear_acceleration.z = m.zacc;

    sensor_msgs::MagneticFieldPtr m_mag = boost::make_shared<sensor_msgs::MagneticField>();
    m_mag->header = m_imu->header;
    m_mag->magnetic_field.x = m.xmag;
    m_mag->magnetic_field.y = m.ymag;
    m_mag->magnetic_field.z = m.zmag;
    if(is_start_recording_)
    {
      boost::mutex::scoped_lock lock(imu_mag_queue_mutex_);
      imu_queue_.push(m_imu);
      mag_queue_.push(m_mag);
    }

    //imu_pub_.publish(m_imu);
    // mag_pub_.publish(m_mag);
  }


  void writeImageBag(ImageStamped::Ptr img, bool publish_data){
    std_msgs::Header header;
    header.stamp = std_to_ros_time(img->stamp_);
    if(compress_image_){
      sensor_msgs::CompressedImagePtr msg;
      //msg = cv_bridge::CvImage(header, "bgr8", img->image_).toCompressedImageMsg(cv_bridge::JPG);
      msg = cv_bridge::CvImage(header, img->getStringEncoding(), img->image_).toCompressedImageMsg(cv_bridge::PNG);
      // image_bag_.write(compressed_camera_topic_,ros::Time::now(), msg);
      {
        // We don't care much because CameraManager already manage queue for us
        boost::mutex::scoped_lock lock(bag_mutex_);
        // Delay Publish duration a bit = 30 Hz
        bag_.write(compressed_camera_topic_, msg->header.stamp, msg);
      }

      if(publish_data) img_pub_.publish(msg);
    }
    else{
      sensor_msgs::ImagePtr msg;
      msg = cv_bridge::CvImage(header, img->getStringEncoding(), img->image_).toImageMsg();
      // image_bag_.write(camera_topic_,ros::Time::now(), msg);
      {
        boost::mutex::scoped_lock lock(bag_mutex_);
        bag_.write(camera_topic_, msg->header.stamp, msg);
      }

      if(publish_data) img_pub_.publish(msg);
    }
  }


  void cameraThread(){
    cam_manager_->startStreaming();
    // Camera Manager is implemented as a queue, -> less dataloss if writing to the bagfile is too slow
    ros::Rate rate(30);
    while (ros::ok() && is_start_recording_){

      ImageStamped::Ptr img;
      if(cam_manager_->getImage(img)){
        writeImageBag(img, publish_data_);
      }
      rate.sleep();
    }

    // Cleaning Up Message Queue
    cam_manager_->stopStreaming();
    while(ros::ok()){
      ImageStamped::Ptr img;
      if(cam_manager_->getImage(img)){
        writeImageBag(img, false);
      }
      else{
        break;
      }
    }

  }


  void controllerWriteThread(){
    // TODO
    ros::Rate rate(30);
    while (ros::ok() && is_start_recording_){
      nav_msgs::OdometryPtr odom;
      bool get_data = false;

      {
        boost::mutex::scoped_lock lock(status_queue_mutex_);
        if(!status_queue_.empty()){
          odom = status_queue_.front();
          get_data = true;
          status_queue_.pop();
        }
      }

      if(get_data){
        boost::mutex::scoped_lock lock(bag_mutex_);
        bag_.write(odom_topic_, odom->header.stamp, odom);
      }

      rate.sleep();
    }

    while(!status_queue_.empty()){
      nav_msgs::OdometryPtr odom;
      odom = status_queue_.front();
      status_queue_.pop();
      {
        boost::mutex::scoped_lock lock(bag_mutex_);
        bag_.write(odom_topic_, odom->header.stamp, odom);
      }
    }

  }

  void controllerReadThread(){

    // we wil trust that this thread is fast enough for data writing
    // Naively, write directly to the bagfile for now
    ros::Rate rate(30);
    controller_->startController();

    while (ros::ok() && is_start_recording_){
      Eigen::Vector3d baselink_position, baselink_linear, baselink_angular;
      Eigen::Quaterniond baselink_q;
      controller_->getRobotFrameVelocity(baselink_linear, baselink_angular);
      bool st = controller_->getRobotFrameTransform(baselink_position, baselink_q);
      if(st){
        nav_msgs::OdometryPtr odom = boost::make_shared<nav_msgs::Odometry>();

        odom->header.stamp = ros::Time::now();
        odom->header.frame_id = "/map_enu";
        odom->child_frame_id = "/base_link";



        tf::vectorEigenToMsg(baselink_linear, odom->twist.twist.linear);
        tf::vectorEigenToMsg(baselink_angular, odom->twist.twist.angular);
        tf::pointEigenToMsg(baselink_position, odom->pose.pose.position);
        tf::quaternionEigenToMsg(baselink_q, odom->pose.pose.orientation);

        for (int i = 0; i < 3; i++) {
          // linear velocity
          odom->twist.covariance[i + 6 * i] = 1e-4;
          // angular velocity
          odom->twist.covariance[(i + 3) + 6 * (i + 3)] = 1e-4;
          // position/ attitude
          if (i == 2) {
            // z
            odom->pose.covariance[i + 6 * i] = 1e-6;
            // yaw
            odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
          } else {
            // x, y
            odom->pose.covariance[i + 6 * i] = 1e-6;
            // roll, pitch
            odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
          }
        }

        //status_bag_.write(odom_topic_,odom->header.stamp, odom);
        {
          boost::mutex::scoped_lock lock(status_queue_mutex_);
          status_queue_.push(odom);
        }

        if (publish_data_) odom_pub_.publish(odom);
      }
      rate.sleep();
    }



  }

  void stopRecording(){
    if(is_start_recording_){
      is_start_recording_ = false;
      camera_thread_->join();
      controller_read_thread_->join();
      controller_write_thread_->join();
      imu_write_thread_->join();
      bag_.close();

      camera_thread_.reset();
      controller_read_thread_.reset();
      controller_write_thread_.reset();
      imu_write_thread_.reset();
    }
  }

  std::string normalizePath(std::string path)
  {
    if(path[0] == '~')
    {
      char* home = getenv("HOME");
      return home + path.substr(1);
    }
    else
    {
      return path;
    }

  }

};


int main(int argc, char** argv){
  ros::init(argc, argv, "bag_recorder_node");
  BagRecorderNode node;
  node.execute();
  return 0;
}
