//
// Created by kandithws on 2/6/2561.
//

#include <ros/ros.h>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <move_drone/AprilTagObstacleDetector.h>
#include <move_drone/CameraInfo.h>
#include <boost/thread/mutex.hpp>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_listener.h>




/*
 *  Todo -- Implement this as 2 inter changeable versions
 *  1. ros::Subscriber based
 *  2. direct read with rosbag::View
 *
 *
 * */

using namespace move_drone;

class BagReplayNode{
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  BagReplayNode(ros::NodeHandle& nh): nh_(nh){
    std::string obs_config_file;
    nh_.param<std::string>("obstacle_config_file", obs_config_file, "");
    detector_ = boost::make_shared<AprilTagObstacleDetector>(obs_config_file);
    //CameraInfo cam_info(obs_config_file);
    std::string camera_info_file;
    nh_.param<std::string>("camera_info_file", camera_info_file, "");
    cam_info_ = CameraInfo(camera_info_file);
    detector_->setCameraParams(cam_info_);
    cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/obstacle_cloud", 1);
    out_img_pub_ = nh_.advertise<sensor_msgs::Image>("/detection_out", 1);
    tf::Quaternion q;
    q.setEuler(0,0,0);
    tf_.setRotation(q);

  }

  void execute(){
    initExecute();
    tf_publisher_thread_ = boost::make_shared<boost::thread>(boost::bind(&BagReplayNode::tfThread, this));
    ros::spin();
  }

 protected:
  ros::NodeHandle nh_;
  boost::shared_ptr<AprilTagObstacleDetector> detector_;
  boost::shared_ptr<boost::thread> tf_publisher_thread_;
  boost::mutex tf_eigen_mutex_;
  Eigen::Affine3d tf_eigen_;
  tf::StampedTransform tf_;
  boost::mutex tf_mutex_;
  ros::Publisher cloud_pub_;
  ros::Publisher out_img_pub_;
  tf::TransformBroadcaster tf_br_;
  tf::TransformListener tf_lis_;
  CameraInfo cam_info_;


  virtual void initExecute() = 0;

  void processImage(cv::Mat& img, ros::Time stamp){
    PointCloudT out_cloud;
    cv::Mat undistort;

    //Listen to TF at current time
    Eigen::Affine3d cam_to_map_mat;
    tf::StampedTransform cam_to_map;
    try{
      tf_lis_.lookupTransform("/map_enu", "/camera_optical_link",
                               ros::Time(0), cam_to_map);
    }
    catch (tf::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      //ros::Duration(1.0).sleep();
    }

    tf::transformTFToEigen(cam_to_map, cam_to_map_mat);




    if (cam_info_.undistort){

      cam_info_.unDistortImage(img, undistort);
      detector_->detect(undistort, out_cloud);

    }
    else{
      detector_->detect(img, out_cloud);
    }

    // Need to transform cloud to the map coordinate because calculation could run slow,
    // Especially when run in embeded system
    pcl::transformPointCloud(out_cloud, out_cloud, cam_to_map_mat);
    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(out_cloud, cloud_msg);
    // TODO -- Transform cloud to baselink here, without using ROS
    //cloud_msg.header.frame_id = "/camera_optical_link";
    cloud_msg.header.frame_id = "/map_enu";
    cloud_msg.header.stamp = ros::Time::now();

    ROS_INFO("Stamp Time Difff: %lf", (cloud_msg.header.stamp - stamp).toSec() );



    //cloud_msg.header.stamp = stamp;
    cloud_pub_.publish(cloud_msg);


    std_msgs::Header header;
    header.stamp = ros::Time::now();
    sensor_msgs::ImagePtr img_msg;
    if(cam_info_.undistort){
      img_msg = cv_bridge::CvImage(header, "bgr8", undistort).toImageMsg();
    }
    else{
      img_msg = cv_bridge::CvImage(header, "bgr8", img).toImageMsg();
    }

    out_img_pub_.publish(img_msg);

  }

  void processOdomMessage(const nav_msgs::OdometryPtr& odom){
    // 1. process tf from base_link -> world as a TF mattrix;
    tf::Transform tf;
    tf.setOrigin(tf::Vector3(odom->pose.pose.position.x,
                             odom->pose.pose.position.y, odom->pose.pose.position.z));
    tf.setRotation(tf::Quaternion(odom->pose.pose.orientation.x,
                                   odom->pose.pose.orientation.y,
                                   odom->pose.pose.orientation.z, odom->pose.pose.orientation.w));
    {
      boost::mutex::scoped_lock lock(tf_mutex_);
      //tf_ = tf::StampedTransform(tf, ros::Time::now(), odom->header.frame_id, odom->child_frame_id);
      tf_ = tf::StampedTransform(tf, odom->header.stamp, odom->header.frame_id, odom->child_frame_id);
    }


  }

  void tfThread(){
    ros::Rate rate(20);
    while(ros::ok()){

      tf::StampedTransform tf;
      {
        boost::mutex::scoped_lock lock(tf_mutex_);
        tf = tf_;
        //tf_pub_stamp_ = tf.stamp_;
      }
      tf.stamp_ = ros::Time::now() + ros::Duration(1.0/20.0);
      tf_br_.sendTransform(tf);
      ros::spinOnce();
      rate.sleep();
    }
  }






};

class BagReplaySubscriberNode : public BagReplayNode{
 public:
  BagReplaySubscriberNode(ros::NodeHandle& nh) : BagReplayNode(nh){
      // Only copy data from subscriber, then calling logic in Superclass again
    nh_.param("compress_image", compress_image_, false);


    if(compress_image_){
      img_sub_ = nh_.subscribe("/camera/image/compressed", 1, &BagReplaySubscriberNode::imgCompressedCallback, this);
    }
    else{
      img_sub_ = nh_.subscribe("/camera/image", 1, &BagReplaySubscriberNode::imgCallback, this);
    }

    odom_sub_ = nh_.subscribe("/odom", 1, &BagReplaySubscriberNode::odomCallback, this);
  }

 protected:
  void initExecute(){

  }


 private:
  ros::Subscriber img_sub_; // No need Image Transport
  ros::Subscriber odom_sub_;
  bool compress_image_;


  void odomCallback(const nav_msgs::OdometryPtr& msg){
    processOdomMessage(msg);
  }

  void imgCallback(const sensor_msgs::ImagePtr& msg){
    cv_bridge::CvImagePtr img_tmp = cv_bridge::toCvCopy(msg, "bgr8");
    processImage(img_tmp->image, msg->header.stamp);
  }

  void imgCompressedCallback(const sensor_msgs::CompressedImagePtr& msg){
    cv_bridge::CvImagePtr img_tmp = cv_bridge::toCvCopy(msg, "bgr8");
    processImage(img_tmp->image, msg->header.stamp);
  }

};

/*
class BagReplayDirectNode{

};
*/


int main(int argc, char** argv){
  ros::init(argc, argv, "bag_replay_node");
  ros::NodeHandle nh("~");
  bool subscribe_from_ros;
  nh.param("subscribe_from_ros", subscribe_from_ros, true);
  boost::shared_ptr<BagReplayNode> node;

  if (subscribe_from_ros){
    node = static_cast< boost::shared_ptr<BagReplayNode> >(boost::make_shared<BagReplaySubscriberNode>(nh)) ;
  }
  else{
    ROS_ERROR("Direct Bag File Not Implemented yet");
    return -1;
  }

  node->execute();

  return 0;

}