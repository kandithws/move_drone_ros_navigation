//
// Created by kandithws on 23/8/2561.
//
#include <move_drone_ros/common_utils.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/filesystem.hpp>
#include <move_drone/BaseCameraManager.h>
#include <move_drone/CameraManager.h>
#include <move_drone_ros/RosCameraManager.h>
#include <move_drone/external_imu/phidget/PhidgetIMU.h>

#include <move_drone/MAVController.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/MagneticField.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <queue>

#define VERBOSE_RATE 30 //in sec

using namespace move_drone;
static inline ros::Time std_to_ros_time(std::chrono::nanoseconds std_t){
  ros::Time t;
  return t.fromNSec(static_cast<uint64_t >(std_t.count()));
}


/*
  * @brief CameraRecorder is an abstraction of a camera in the recording system
  *  We use nested class because it is easier to share resources with log recorder (mutexes...)
  * */

class CameraRecorder{
 public:
  std::string topic_; // Recording topic for rosbag, and pub. for CSV will use camera name only
  std::string cam_type_ = "webcam"; // webcam, airsim, ros
  std::string cam_name_ = "camera0";
  int cam_id_ = 0;
  boost::shared_ptr<BaseCameraManager> cam_manager_;
  bool compress_image_ = false;
  bool publish_data_ = false;
  ros::Publisher img_pub_;
  boost::shared_ptr<boost::mutex> bag_mutex_;
  boost::shared_ptr<rosbag::Bag> bag_writer_;
  bool is_recording_ = false;

  CameraRecorder(int id, std::string type, bool compress_image){
    cam_id_ = id;
    cam_name_ = "camera" + std::to_string(id);
    ROS_ASSERT_MSG( ( (type == "webcam") || (type == "airsim") || (type == "ros") ),
                    "Wrong Camera recorder type");
    cam_type_ = type;
    compress_image_ = compress_image;
    topic_ = "/" + cam_name_ + std::string(compress_image?"/image/compressed":"/image_raw");
    //writing_rate_ = writing_rate;
  }

  void initROSPublisher(ros::NodeHandle& nh){
    publish_data_ = true;
    if (compress_image_){
      img_pub_ = nh.advertise<sensor_msgs::CompressedImage>(topic_, 1);
    }
    else{
      img_pub_ = nh.advertise<sensor_msgs::Image>(topic_, 1);
    }

  }

  std::string startBagRecording(boost::shared_ptr<rosbag::Bag>& bag_writer, boost::shared_ptr<boost::mutex>& bag_mutex){
    bag_mutex_ = bag_mutex;
    bag_writer_ = bag_writer;
    is_recording_ = true;
    cam_manager_->registerCallback(std::bind(&CameraRecorder::imageCallback, this, std::placeholders::_1));
    cam_manager_->startStreaming();
    return cam_name_;
  }

  void stopBagRecording(){
    ROS_INFO("Stop Streamimg for %s", cam_name_.c_str());
    cam_manager_->stopStreaming();
    ROS_INFO("Cleanup Message Queue for %s", cam_name_.c_str());
    is_recording_ = false;
    // TODO -- Cleanup images queue here!
    move_drone::ImageStamped::Ptr img;
    while(cam_manager_->getImage(img)){
      writeImage2Bag(img, false);
    }

    bag_mutex_.reset();
    bag_writer_.reset();

    // Note -- we must join at the main thread otherwise other thread won't call stop streaming
  }

  void imageCallback(const move_drone::ImageStamped::ConstPtr &msg){
    writeImage2Bag(msg, publish_data_);
    // TODO -- implement CSV
  }

  void writeImage2Bag(const ImageStamped::ConstPtr& img, bool publish_data){
    assert(bag_mutex_);
    assert(bag_writer_);
    std_msgs::Header header;
    header.stamp = std_to_ros_time(img->stamp_);
    if(compress_image_){
      sensor_msgs::CompressedImagePtr msg;
      //msg = cv_bridge::CvImage(header, "bgr8", img->image_).toCompressedImageMsg(cv_bridge::JPG);
      msg = cv_bridge::CvImage(header, img->getStringEncoding(), img->image_).toCompressedImageMsg(cv_bridge::PNG);
      // image_bag_.write(compressed_camera_topic_,ros::Time::now(), msg);
      {
        boost::mutex::scoped_lock lock(*bag_mutex_);
        bag_writer_->write(topic_, msg->header.stamp, msg);
      }

      if(publish_data) img_pub_.publish(msg);
    }
    else{
      sensor_msgs::ImagePtr msg;
      msg = cv_bridge::CvImage(header, img->getStringEncoding(), img->image_).toImageMsg();
      {
        boost::mutex::scoped_lock lock(*bag_mutex_);
        bag_writer_->write(topic_, msg->header.stamp, msg);
      }

      if(publish_data) img_pub_.publish(msg);
    }
  }

  void writeImage2CSV(const ImageStamped::Ptr& img, bool publish_data){
    throw std::runtime_error("Not Implemented");
  }

};

/**
 * @brief LogRecorderNode is a ros wrapper node that provide several type of data recording for move_drone library
 * Implementation Requirements:
 *      1. Select drone's data to save 1.camera 2.imu 3.external imu 3.drone's local pose 4.etc.
 *      2. Interchange able saving format: 2.1) direct rosbagAPI 2.2) CSV [EuRoC] (+optional) publish through ROS
 *      3. For external IMU, and camera, should be able to subscribe optinally from ROS
 *      4. Must not include Transform within a bagfile directly !!!!
 *
 *   TODO:
 *      May consider change from fix rate workerthead to condition variable to notify when q not empty
 * */

class LogRecorderNode{
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  /*Code Structure: Bind execute thread with different function for 2.1 and 2.2
     *  CSV vs rosbag API threading model is not the same.
     *  In CSV 1 filewriter per data. In ROSBAG 1 filewriter for every data
     *  Data consumer thread is still the same
     * */
  LogRecorderNode():
      nh_("~")
  {
    nh_.param<std::string>("save_path",save_dir_, "");
    save_dir_ = normalizePath(save_dir_);
    bag_ = boost::make_shared<rosbag::Bag>();
    bag_mutex_ = boost::make_shared<boost::mutex>();
    if (!boost::filesystem::exists(save_dir_)){
      ROS_ERROR("Save Directory is not exist! %s", save_dir_.c_str());
      exit(-1);
    }
    // TODO -- csv:EuRoc, csv:TUM, etc, ...
    nh_.param<std::string>("output_format", output_format_, "rosbag");
    ROS_ASSERT_MSG(output_format_ == "rosbag", "Other output format still not support");

    nh_.param("mavlink_enable", mav_enable_, true);
    // --------------------------------- MAVLINK SETUP ----------------------------
    if (mav_enable_){
      std::string port_url;
      ros::NodeHandle mav_nh(nh_, "mavlink");
      mav_nh.param<std::string>("port_url",port_url, "/dev/ttyUSB0:921600");
      controller_ = boost::shared_ptr<MAVController>(new MAVController(port_url));
      // Register Callbacks
      //controller_->registerHighResIMUCallback();

      mav_nh.param("pose_update_rate", pose_update_rate_, 50.0);
      controller_->setPoseVelUpdateRate(pose_update_rate_);
      //mav_nh.param("imu_write_rate", imu_write_rate_, 200);
      bool publish_mav_data;
      mav_nh.param("publish_data", publish_mav_data, false);
      if(publish_mav_data){
        odom_pub_ = nh_.advertise<nav_msgs::Odometry>(odom_topic_,1);
        imu_pub_ = nh_.advertise<sensor_msgs::Imu>(imu_topic_, 1);
        mag_pub_ = nh_.advertise<sensor_msgs::MagneticField>(mag_topic_, 1);
        publish_mav_data_ = publish_mav_data;
      }
    }
    else{
      ROS_WARN("MAVLINK Data is not recorded!!");
    }

    // -------------------- CAMERA SETUP --------------------------------
    // Extend to N camera System
    nh_.param("num_cameras", num_cameras_, 1);
    initCameraRecorders();
    // ---------------------------- External IMU SETUP ---------------------
    nh_.param("external_imu_enable", external_imu_enable_, false);
    if (external_imu_enable_){
      // Fixed code to Phidget for now
      ROS_WARN("The current support type for external IMU is only Phidgets");
      ros::NodeHandle ext_nh(nh_, "external_imu");

      ext_nh.param("streaming_period", ext_imu_period_, 4);
      ext_nh.param("publish_data", ext_imu_publish_data_, false);

      ext_imu_conn_.setStreamingRate(ext_imu_period_);
      ext_imu_conn_.registerCallback(std::bind(&LogRecorderNode::externalIMUCallback, this, std::placeholders::_1));

      if(ext_imu_publish_data_){
        ext_imu_pub_ = nh_.advertise<sensor_msgs::Imu>(ext_imu_topic_, 10);
        ext_mag_pub_ = nh_.advertise<sensor_msgs::MagneticField>(ext_mag_topic_, 10);
      }
    }

    // Timed Recording
    nh_.param("record_duration", record_duration_, -1);

  }

  void execute(){
    if (record_duration_ <= 0){
      toggle_recorder_srv_ = nh_.advertiseService("/toggle_recorder",
                                                  &LogRecorderNode::toggleRecorderServiceCallback,
                                                  this);
      ros::spin();
    }
    else{

      toggle_recorder_srv_ = nh_.advertiseService("/stop_recorder",
                                                  &LogRecorderNode::stopRecorderServiceCallback,
                                                  this);

      startBagRecording();
      record_init_time_ = ros::Time::now();
      //boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
      ROS_INFO("Start Recording for %d seconds", record_duration_);
      record_timer_thread_ = boost::make_shared<boost::thread>(boost::bind(&LogRecorderNode::recordTimerThread, this));
      ros::spin();
    }

  }

  bool toggleRecorderServiceCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res){
    if(is_start_recording_){
      // Cleaning Up
      ROS_INFO("Clearing Messages Queue Please wait");
      if (output_format_ == "rosbag")
        stopBagRecording();
      ROS_INFO("Done");
    }
    else{
      std::string record_details;
      if (output_format_ == "rosbag")
        record_details = startBagRecording();

      // Verbose recording details, filename blah blah
      ROS_INFO("Recording Details:\r\n%s\r\n", record_details.c_str());
      ROS_INFO("-------- Start Recording ----------");
    }
    return true;
  }

  bool stopRecorderServiceCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res){
    assert(record_duration_ <= 0);
    ROS_INFO("Log Recorder is preempted at %.2lf / %d seconds", (ros::Time::now() - record_init_time_).toSec(),
             record_duration_ );

    record_preempted_ = true;
    //ROS_INFO("Clearing Messages Queue Please wait");
    //if (output_format_ == "rosbag")
    //  stopBagRecording();

    //ROS_INFO("---- DONE ----");
  }

  void recordTimerThread(){
    assert(record_duration_ <= 0);
    boost::chrono::milliseconds rate(100);
    while(is_start_recording_ && (!record_preempted_) && ((ros::Time::now() - record_init_time_).toSec() < record_duration_) ){
      ROS_INFO_THROTTLE( VERBOSE_RATE, "Current Recording time %.2lf / %d seconds",
                           (ros::Time::now() - record_init_time_).toSec(), record_duration_ );
      boost::this_thread::sleep_for(rate);
    }

    ROS_INFO("Cleaning Messages");
    stopBagRecording();
    ROS_INFO("---- DONE ----");
  }

 private:
  ros::NodeHandle nh_;

  std::string save_dir_ = "~";
  bool is_start_recording_ = false;
  std::string output_format_ = "rosbag";
  boost::shared_ptr<rosbag::Bag> bag_;
  boost::shared_ptr<boost::mutex> bag_mutex_;

  // bool compress_image_ = false;

  ros::Publisher odom_pub_;
  ros::Publisher img_pub_;
  ros::ServiceServer toggle_recorder_srv_;


  boost::shared_ptr<MAVController> controller_;
  //boost::shared_ptr<BaseCameraManager> cam_manager_;
  //std::vector< boost::shared_ptr<BaseCameraManager> > cam_managers_;
  std::vector< boost::shared_ptr<CameraRecorder> > cam_recorders_;
  int record_duration_ = -1;
  ros::Time record_init_time_;
  boost::shared_ptr<boost::thread> record_timer_thread_;
  bool record_preempted_ = false;

  // Mavlink Stuffs
  std::string odom_topic_ = "/odom";
  bool publish_mav_data_ = false; // For debugging purpose
  double pose_update_rate_ = 50.0;
  // int imu_write_rate_ = 200; // Depending on Hz we configure on pixhawk
  boost::shared_ptr<boost::thread> mav_odom_write_thread_;
  //boost::shared_ptr<boost::thread> mav_imu_write_thread_;
  std::queue<nav_msgs::OdometryPtr> odom_queue_;
  boost::mutex odom_queue_mutex_;
  std::string imu_topic_ = "/mav/imu";
  std::string mag_topic_ = "/mav/mag";
  ros::Publisher imu_pub_;
  ros::Publisher mag_pub_;


  // External IMU!
  ros::Publisher ext_imu_pub_;
  ros::Publisher ext_mag_pub_;
  move_drone::PhidgetIMU ext_imu_conn_;
  std::string ext_imu_frame_id_ = "/external_imu_link";
  std::string ext_imu_topic_ = "/external_imu/data";
  std::string ext_mag_topic_ = "/external_imu/mag";
  int ext_imu_period_ = 4;
  bool ext_imu_publish_data_ = false;

  bool mav_enable_ = true;
  int num_cameras_ = 1;
  bool external_imu_enable_ = false;

  void initCameraRecorders(){
    cam_recorders_.clear();

    if(num_cameras_ < 1)
      return;

    cam_recorders_.resize(num_cameras_);
    ROS_INFO("Initializing %d camera managers", num_cameras_);
    for (int i=0; i < num_cameras_; i++){
      std::string cam_path, cam_params_file, camera_type, ros_topic;
      ros::NodeHandle cam_nh(nh_, "camera" + std::to_string(i));
      bool compress_image, is_ros_compress_image, publish_data;
      int image_q_size;
      cam_nh.param("queue_size", image_q_size ,10);
      cam_nh.param("compress_image", compress_image, false);
      cam_nh.param<std::string>("type", camera_type, "webcam");
      cam_nh.param<std::string>("webcam_path", cam_path, "/dev/video1");
      // If use ros camera manager
      cam_nh.param<std::string>("ros_topic", ros_topic,
                                "/external_camera" + std::to_string(i) + std::string(compress_image?"/image/compressed":"/image_raw"));
      cam_nh.param("ros_compress_image", is_ros_compress_image, false);
      cam_nh.param("publish_data", publish_data, false);

      // Create Recorders
      cam_recorders_[i] = boost::make_shared<CameraRecorder>(i, camera_type, compress_image);
      // Dirty creating instance details here!
      if((camera_type == "webcam")){
        cam_recorders_[i]->cam_manager_ = boost::make_shared<CameraManager>(cam_path, image_q_size);
      }
      else if (camera_type == "ros"){
        // cam_path = get_cam_path?cam_path:"/external_camera/image_raw";
        cam_recorders_[i]->cam_manager_ = boost::make_shared<RosCameraManager>(nh_, image_q_size,
                                                                              is_ros_compress_image, ros_topic);
      }else if(camera_type == "airsim"){
        //TODO implement AIRSIM
        ROS_FATAL("Airsim camera is not implemented");
        throw std::runtime_error("Camera type error");
      }else {
        ROS_FATAL("Camera type error, Abort!");
        throw std::runtime_error("Camera type error");
      }
      ROS_ASSERT(cam_recorders_[i]->cam_manager_);
      if(cam_nh.hasParam("camera_config_file")){
        cam_nh.param<std::string>("camera_config_file", cam_params_file, "");
        cam_recorders_[i]->cam_manager_->loadCameraInfo(cam_params_file);
      }
      else{
        ROS_WARN("No Camera Config Loaded!");
      }

      if(publish_data)
        cam_recorders_[i]->initROSPublisher(nh_);
    }

  }



  std::string startBagRecording(){
    uint64_t t_stamp = (uint64_t)(ros::Time::now().toBoost() - boost::posix_time::from_time_t(0)).total_nanoseconds();
    std::string out_dir = save_dir_ + "/drone_data_" + std::to_string(t_stamp) + ".bag";
    //boost::filesystem::create_directory(out_dir);
    ROS_INFO("Saving Bagfile at: %s", out_dir.c_str());
    bag_->open( out_dir, rosbag::bagmode::Write);
    is_start_recording_ = true;

    if(mav_enable_){
      controller_->registerHighResIMUCallback(std::bind(&LogRecorderNode::mavIMUCallback, this,
                                                        std::placeholders::_1, std::placeholders::_2));

      controller_->registerPoseVelCallback(std::bind(&LogRecorderNode::mavPoseVelCallback, this,
                                                     std::placeholders::_1, std::placeholders::_2,
                                                     std::placeholders::_3, std::placeholders::_4,
                                                     std::placeholders::_5));

      controller_->startController();
      mav_odom_write_thread_ =
          boost::make_shared<boost::thread>(boost::bind(&LogRecorderNode::mavOdomWriteThread, this));

    }
    // ----------------- Init camera threads ----------------
    if(num_cameras_ > 0){
      //cam_recorders_threads = boost::make_shared<boost::thread_group>();
      for (int i = 0 ; i < num_cameras_; i++){
        cam_recorders_[i]->startBagRecording(bag_, bag_mutex_);
      }
    }

    if(external_imu_enable_){
        ROS_INFO("Start Streaming External IMU");
        ext_imu_conn_.startStreaming();
    }

    std::stringstream ss;
    ss << "***Recording Details***" << std::endl
       << "output format: " << output_format_ << std::endl
       << "output file: " << out_dir << std::endl;
    return ss.str();
    //ROS_INFO(" ----------- Start Recording --------");
  }

  void stopBagRecording(){
    is_start_recording_ = false;
    if(mav_enable_){
      // Cleaning up IMU messages
      std::vector< std::shared_ptr<mavlink_msgs::HIGHRES_IMU> > imu_msgs;
      std::vector< std::shared_ptr<std::chrono::nanoseconds> > stamp_msgs;
      ros::Time now = ros::Time::now();
      controller_->getIMUMessagesUpToStamp(imu_msgs, stamp_msgs, move_drone_ros::ros_time_to_std(now));
      boost::mutex::scoped_lock lock(*bag_mutex_);
      ROS_INFO("Total size for MAV IMU Cleanup: %ld", imu_msgs.size());
      for(size_t i=0; i < imu_msgs.size(); i++){
        //bag_->write();
        sensor_msgs::Imu m_imu;
        sensor_msgs::MagneticField m_mag;
        createROSIMUMessage(m_imu, m_mag, *imu_msgs[i], *stamp_msgs[i]);
        bag_->write(imu_topic_, m_imu.header.stamp, m_imu);
        bag_->write(mag_topic_, m_mag.header.stamp, m_mag);
      }
        
      mav_odom_write_thread_->join();
      //mav_imu_write_thread_->join();
      // Clear IMU messages

    }

    if(num_cameras_ > 0){
      for (int i=0; i < num_cameras_; i++){
        cam_recorders_[i]->stopBagRecording();
      }
    }

    if(external_imu_enable_){
     // TODO -- there is some freezing in stop streaming
      ext_imu_conn_.stopStreaming();
    }

    bag_->close();
  }

  void mavIMUCallback(const mavlink_msgs::HIGHRES_IMU& m, const std::chrono::nanoseconds& stamp){
    //static int cleaning_up_state = 0; // start cleaning up
    //static ros::Time finishup_time;
    sensor_msgs::Imu m_imu;
    // body frame, x-front, y-right, z-axis down
    //m_imu->header.stamp = ros::Time::now();
    sensor_msgs::MagneticField m_mag;

    createROSIMUMessage(m_imu, m_mag, m, stamp);

    // ----- We will write to bag here ------!
    if(is_start_recording_)
    {
        boost::mutex::scoped_lock lock(*bag_mutex_);
        bag_->write(imu_topic_, m_imu.header.stamp, m_imu);
        bag_->write(mag_topic_, m_mag.header.stamp, m_mag);
    }


    if(publish_mav_data_){
      // TODO -- publish imu data
      imu_pub_.publish(m_imu);
      mag_pub_.publish(m_mag);
    }
  }

  void createROSIMUMessage(sensor_msgs::Imu& m_imu, sensor_msgs::MagneticField& m_mag,
                           const mavlink_msgs::HIGHRES_IMU& m, const std::chrono::nanoseconds& stamp){
    m_imu.header.stamp = std_to_ros_time(stamp);
    m_imu.header.frame_id = "imu_link";
    m_imu.orientation_covariance[0] = -1; // standard for telling that, imu is not providing the data
    m_imu.angular_velocity.x = m.xgyro;
    m_imu.angular_velocity.y = m.ygyro;
    m_imu.angular_velocity.z = m.zgyro;
    m_imu.linear_acceleration.x = m.xacc;
    m_imu.linear_acceleration.y = m.yacc;
    m_imu.linear_acceleration.z = m.zacc;
    m_mag.header = m_imu.header;
    m_mag.magnetic_field.x = m.xmag;
    m_mag.magnetic_field.y = m.ymag;
    m_mag.magnetic_field.z = m.zmag;
  }

  void mavPoseVelCallback(const Eigen::Vector3d& baselink_position, const Eigen::Quaterniond& baselink_q,
                          const Eigen::Vector3d&  baselink_linear, const Eigen::Vector3d& baselink_angular,
                          const std::chrono::nanoseconds& stamp){

    nav_msgs::OdometryPtr odom = boost::make_shared<nav_msgs::Odometry>();

    odom->header.stamp = move_drone_ros::std_to_ros_time(stamp);
    odom->header.frame_id = "/map_enu";
    odom->child_frame_id = "/base_link";

    tf::vectorEigenToMsg(baselink_linear, odom->twist.twist.linear);
    tf::vectorEigenToMsg(baselink_angular, odom->twist.twist.angular);
    tf::pointEigenToMsg(baselink_position, odom->pose.pose.position);
    tf::quaternionEigenToMsg(baselink_q, odom->pose.pose.orientation);

    for (int i = 0; i < 3; i++) {
      // linear velocity
      odom->twist.covariance[i + 6 * i] = 1e-4;
      // angular velocity
      odom->twist.covariance[(i + 3) + 6 * (i + 3)] = 1e-4;
      // position/ attitude
      if (i == 2) {
        // z
        odom->pose.covariance[i + 6 * i] = 1e-6;
        // yaw
        odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
      } else {
        // x, y
        odom->pose.covariance[i + 6 * i] = 1e-6;
        // roll, pitch
        odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
      }
    }

    //status_bag_.write(odom_topic_,odom->header.stamp, odom);
    if(is_start_recording_)
    {
      boost::mutex::scoped_lock lock(odom_queue_mutex_);
      odom_queue_.push(odom);
    }

    if (publish_mav_data_) odom_pub_.publish(odom);

    // TODO -- Publish TF here!
  }

  void externalIMUCallback(const move_drone::IMUData& msg){
    // Currently believe that phidgetlib will manage Queue for us (maybe in hardware)
    sensor_msgs::Imu imu_msg;
    imu_msg.header.stamp = move_drone_ros::std_to_ros_time(msg.stamp);
    imu_msg.header.frame_id = ext_imu_topic_;

    imu_msg.linear_acceleration.x = msg.accel[0];
    imu_msg.linear_acceleration.y = msg.accel[1];
    imu_msg.linear_acceleration.z = msg.accel[2];

    imu_msg.angular_velocity.x = msg.gyro[0];
    imu_msg.angular_velocity.y = msg.gyro[1];
    imu_msg.angular_velocity.z = msg.gyro[2];

    sensor_msgs::MagneticField mag_msg;

    mag_msg.header = imu_msg.header;
    mag_msg.magnetic_field.x = msg.mag[0];
    mag_msg.magnetic_field.y = msg.mag[1];
    mag_msg.magnetic_field.z = msg.mag[2];

    if(is_start_recording_)
    {
      boost::mutex::scoped_lock lock(*bag_mutex_);
      bag_->write(ext_imu_topic_, imu_msg.header.stamp, imu_msg);
      bag_->write(ext_mag_topic_, mag_msg.header.stamp, mag_msg);
    }

    if(ext_imu_publish_data_){
      ext_imu_pub_.publish(imu_msg);
      ext_mag_pub_.publish(mag_msg);
    }

  }

  void mavOdomWriteThread(){
    ros::Rate rate(pose_update_rate_);
    while (ros::ok() && is_start_recording_){
      nav_msgs::OdometryPtr odom;
      bool get_data = false;
      {
        boost::mutex::scoped_lock lock(odom_queue_mutex_);
        if(!odom_queue_.empty()){
          odom = odom_queue_.front();
          get_data = true;
          odom_queue_.pop();
        }
      }
      if(get_data){
        boost::mutex::scoped_lock lock(*bag_mutex_);
        bag_->write(odom_topic_, odom->header.stamp, odom);
      }

      rate.sleep();
    }
    /*
    TODO -- Some bug here, freezing when stop streaming
    ROS_INFO("Clearing MAV Odom Queue");
    while(!odom_queue_.empty()){
      nav_msgs::OdometryPtr odom;
      odom = odom_queue_.front();
      odom_queue_.pop();
      {
        boost::mutex::scoped_lock lock(*bag_mutex_);
        bag_->write(odom_topic_, odom->header.stamp, odom);
      }
    }
    */
  }


  std::string normalizePath(std::string path) {
    if(path[0] == '~') {
      char* home = getenv("HOME");
      return home + path.substr(1);
    }
    else {
      return path;
    }
  }

};

int main (int argc, char** argv){
  ros::init(argc, argv, "log_recorder_node");
  LogRecorderNode node;
  node.execute();
  return 0;
}
