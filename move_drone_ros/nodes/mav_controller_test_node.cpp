//
// Created by kandithws on 19/2/2561.
//

// This must be at the top
#include <move_drone/utils/eigen_disable_assert_odroid.h>

#include <ros/ros.h>
#include <move_drone_ros/common_utils.h>
#include <move_drone_ros/HeartBeatStatus.h>
#include <move_drone_ros/SystemStatus.h>
#include <move_drone/MAVController.h>
#include <move_drone/WayPointTrajectoryTracker.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/Int32.h>

#define HIGHRES_IMU_ACC_UPDATE_MASK uint16_t(0x0007)
#define HIGHRES_IMU_GYRO_UPDATE_MASK uint16_t(0x0038)
//#include <move_drone_ros/Path.h>

#include <std_srvs/Empty.h>

#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>

using namespace move_drone;
class MAVControllerTestNode {

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  MAVControllerTestNode() :
      nh_("~") {
    nh_.param<std::string>("port_url", port_url_, "/dev/ttyACM0:57600");
    nh_.param<std::string>("odom_frame", odom_frame_, "map_enu");
    nh_.param<std::string>("base_frame", base_frame_, "base_link");
    nh_.param<std::string>("imu_frame", imu_frame_, "imu_link");
    nh_.param("tf_pub_rate", tf_pub_rate_, 15.0);
    nh_.param("controller/pose_update_rate",
              pose_sync_rate_,
              30.0); // rate to synchornize tf data (local position and attitute)

    // ROS Pub
    hb_pub_ = nh_.advertise<move_drone_ros::HeartBeatStatus>("/heartbeat", 1, true);
    sys_stat_pub_ = nh_.advertise<move_drone_ros::SystemStatus>("/system_status", 1, true);
    odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/odom", 10);
    imu_pub_ = nh_.advertise<sensor_msgs::Imu>("/imu", 10);
    mag_pub_ = nh_.advertise<sensor_msgs::MagneticField>("/external_compass", 10);
    tracker_setpoint_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("/tracker_setpoint", 1, true);

    //imu_acc_update_pub_ = nh_.advertise<std_msgs::Int32>("/imu_acc_update", 1);
    //imu_gyro_update_pub_ = nh_.advertise<std_msgs::Int32>("/imu_gyro_update", 1);

    // ROS Sub
    point_cmd_sub_ = nh_.subscribe("/cmd_point", 1, &MAVControllerTestNode::pointCmdCallback, this);
    vel_cmd_sub_ = nh_.subscribe("/cmd_vel", 1, &MAVControllerTestNode::velCmdCallback, this);
    yaw_speed_cmd_sub_ = nh_.subscribe("/cmd_yaw_speed", 1, &MAVControllerTestNode::yawSpeedCmdCallback, this);
    pose_cmd_sub_ = nh_.subscribe("/cmd_pose", 1, &MAVControllerTestNode::poseCmdCallback, this);
    path_follower_sub_ = nh_.subscribe("/path", 1, &MAVControllerTestNode::pathCallback, this);

    // ROS Service

    take_off_srv_ = nh_.advertiseService("/take_off", &MAVControllerTestNode::srvTakeOff, this);
    land_srv_ = nh_.advertiseService("/land", &MAVControllerTestNode::srvLanding, this);
    stop_cmd_srv_ = nh_.advertiseService("/stop_cmd", &MAVControllerTestNode::srvStopCmd, this);
    stop_tracking_srv_ = nh_.advertiseService("/stop_tracking", &MAVControllerTestNode::srvStopTracking, this);
    toggle_arm_srv_ = nh_.advertiseService("/toggle_arm", &MAVControllerTestNode::srvToggleArm, this);
    rtl_srv_ = nh_.advertiseService("/return_to_launch", &MAVControllerTestNode::srvReturnToLaunch, this);

    // Initializing connector
    controller_ = std::make_shared<MAVController>(port_url_);
    // Note: This is an alias of std::shared_ptr<MAVController>(new MAVController(port_url_) );
    /*
    controller_->heartbeatCallback = std::bind(&MAVControllerTestNode::mavHeartBeatCB, this,
                                               std::placeholders::_1, std::placeholders::_2);
    controller_->sysStatusCallback = std::bind(&MAVControllerTestNode::mavSysStatusCB, this,
                                               std::placeholders::_1, std::placeholders::_2);
    controller_->localPositionNEDCallback = std::bind(&MAVControllerTestNode::mavLocalPositionCB, this,
                                                      std::placeholders::_1, std::placeholders::_2);
    controller_->imuCallback = std::bind(&MAVControllerTestNode::mavIMUCB, this,
                                         std::placeholders::_1, std::placeholders::_2);
    */
    controller_->registerHeartBeatCallback(std::bind(&MAVControllerTestNode::mavHeartBeatCB, this,
                                                     std::placeholders::_1, std::placeholders::_2));
    controller_->registerSystemStatusCallback(std::bind(&MAVControllerTestNode::mavSysStatusCB, this,
                                                        std::placeholders::_1, std::placeholders::_2));
    controller_->registerLocalPositionNEDCallback(std::bind(&MAVControllerTestNode::mavLocalPositionCB, this,
                                                            std::placeholders::_1, std::placeholders::_2));
    controller_->registerHighResIMUCallback(std::bind(&MAVControllerTestNode::mavIMUCB, this,
                                                      std::placeholders::_1, std::placeholders::_2));
    controller_->setPoseVelUpdateRate(pose_sync_rate_);
    //controller_->attituteCallback = std::bind(&MAVControllerTestNode::mavAttituteCB, this, std::placeholders::_1);
    //ROS_INFO("TEST UNIQUE %d : Should be 1", controller_.unique());
    waypts_tracker_ = std::make_shared<WayPointTrajectoryTracker>(controller_);
    //ROS_INFO("TEST UNIQUE %d : Should be 0", controller_.unique());

  }

  void executeNode() {
    controller_->startController();
    tf_thread_ = std::make_shared<std::thread>(&MAVControllerTestNode::tfPublisherThread, this);
    ros::spin();
  }

 private:

  std::shared_ptr<MAVController> controller_;
  std::shared_ptr<WayPointTrajectoryTracker> waypts_tracker_;
  ros::NodeHandle nh_;
  std::string port_url_;
  ros::Publisher hb_pub_;
  ros::Publisher sys_stat_pub_;
  ros::Publisher odom_pub_;
  ros::Publisher mag_pub_;
  ros::Publisher imu_pub_;
  ros::Publisher tracker_setpoint_pub_;

  // Debugging
  //ros::Publisher imu_acc_update_pub_;
  //ros::Publisher imu_gyro_update_pub_;

  ros::Subscriber point_cmd_sub_;
  ros::Subscriber pose_cmd_sub_; // x,y,z,yaw
  ros::Subscriber vel_cmd_sub_;
  ros::Subscriber yaw_speed_cmd_sub_;

  ros::Subscriber path_follower_sub_;

  ros::ServiceServer take_off_srv_;
  ros::ServiceServer land_srv_;
  ros::ServiceServer stop_cmd_srv_;
  ros::ServiceServer stop_tracking_srv_;
  ros::ServiceServer toggle_arm_srv_;
  ros::ServiceServer rtl_srv_;

  std::shared_ptr<std::thread> tf_thread_;
  tf::Transform tf_odom_to_base_;
  tf::TransformBroadcaster tf_bc_;
  tf::TransformListener tf_;

  std::mutex tf_ned_mutex_;
  ros::Time tf_ned_stamp_;

  bool tf_ned_trans_update_ = false;
  bool tf_ned_rot_update_ = false;
  bool path_recieved_ = false;

  std::string odom_frame_;
  std::string base_frame_;
  std::string imu_frame_;
  double tf_pub_rate_;
  double pose_sync_rate_;

  void mavHeartBeatCB(const MAVController::mav_heartbeat_status& st, const std::chrono::nanoseconds& stamp) {
    move_drone_ros::HeartBeatStatus hb_msg;
    hb_msg.arm = st.arm;
    hb_msg.in_hil = st.in_hil;
    hb_msg.manual_remote_enabled = st.manual_remote_enabled;
    hb_msg.auto_enable = st.auto_enable;
    hb_msg.guided_enable = st.guided_enable;
    hb_msg.system_status = st.system_status;
    hb_msg.frame_type = st.frame_type;
    hb_msg.autopilot_type = st.autopilot_type;
    hb_msg.mav_version = st.mav_version;
    hb_msg.header.stamp = ros::Time::now();
    hb_pub_.publish(hb_msg);
  }

  void mavSysStatusCB(const MAVController::mav_system_status& st, const std::chrono::nanoseconds& stamp) {
    move_drone_ros::SystemStatus sys_msg;
    sys_msg.batt_voltage = st.batt_voltage;
    sys_msg.batt_percent = st.batt_percent;
    sys_msg.load_percent = st.load_percent;
    sys_msg.header.stamp = ros::Time::now();
    sys_stat_pub_.publish(sys_msg);
  }

  void mavLocalPositionCB(const mavlink_msgs::LOCAL_POSITION_NED& m, const std::chrono::nanoseconds& stamp) {

    {
      std::lock_guard<std::mutex> lock(tf_ned_mutex_); // Same as boost scoped_lock
      ros::Time n = ros::Time::now();
      // ROS_INFO("Localposition MSG Time diff: %lf", (n - tf_ned_stamp_).toSec() );
      tf_ned_stamp_ = n;
    }
  }

  void mavIMUCB(const mavlink_msgs::HIGHRES_IMU& m, const std::chrono::nanoseconds& stamp) {
    sensor_msgs::Imu m_imu;
    // body frame, x-front, y-right, z-axis down
    m_imu.header.stamp = ros::Time::now();
    m_imu.header.frame_id = imu_frame_;
    m_imu.orientation_covariance[0] = -1; // standard for telling that, imu is not providing the data
    m_imu.angular_velocity.x = m.xgyro;
    m_imu.angular_velocity.y = m.ygyro;
    m_imu.angular_velocity.z = m.zgyro;
    m_imu.linear_acceleration.x = m.xacc;
    m_imu.linear_acceleration.y = m.yacc;
    m_imu.linear_acceleration.z = m.zacc;

    sensor_msgs::MagneticField m_mag;
    m_mag.header = m_imu.header;
    m_mag.magnetic_field.x = m.xmag;
    m_mag.magnetic_field.y = m.ymag;
    m_mag.magnetic_field.z = m.zmag;

    imu_pub_.publish(m_imu);
    mag_pub_.publish(m_mag);
    //  This part to ensure the data update for HIGHRES_IMU for each msgs
    // Note: Test is already passed, gyro + accel data are updated at 200 Hz
    // 7 means all fields is update
    // See --> mavlink HIGHRES_IMU protocol
    /*
    uint16_t acc_update = (m.fields_updated & HIGHRES_IMU_ACC_UPDATE_MASK);
    uint16_t gyro_update = ((m.fields_updated & HIGHRES_IMU_GYRO_UPDATE_MASK) >> 3);
    std_msgs::Int32 acc_update_msg, gyro_update_msg;
    acc_update_msg.data = (int32_t)acc_update;
    gyro_update_msg.data = (int32_t)gyro_update;
    imu_acc_update_pub_.publish(acc_update_msg);
    imu_gyro_update_pub_.publish(gyro_update_msg);
    */
  }



  void pointCmdCallback(const geometry_msgs::Point &cmd) {
    ROS_INFO("Sending Command Pose ENU: x:%lf y:%lf z:%lf", cmd.x, cmd.y, cmd.z);
    controller_->commandLocalPositionENU(cmd.x, cmd.y, cmd.z);
  }

  void velCmdCallback(const geometry_msgs::Twist &cmd) {
    ROS_INFO("Sending Command Vel: x:%lf y:%lf z:%lf, yaw:%lf",
             cmd.linear.x,
             cmd.linear.y,
             cmd.linear.z,
             cmd.angular.z);
    controller_->commandVelocity(cmd.linear.x, cmd.linear.y, cmd.linear.z, cmd.angular.z);
  }

  void yawSpeedCmdCallback(const std_msgs::Float32 &cmd) {
    ROS_INFO("Turning yaw at speed %f rad/sec", cmd.data);
    controller_->commandVelocity(0, 0, 0, cmd.data);
  }

  void poseCmdCallback(const geometry_msgs::PoseStamped &cmd) {
    geometry_msgs::PoseStamped odom_pose;
    try {
      tf_.transformPose(odom_frame_, cmd, odom_pose);
    }
    catch (tf::TransformException e) {
      ROS_WARN("Failed to transform to %s frame, skipping pose command (%s)", odom_frame_.c_str(), e.what());
      //return false;
      return;
    }
    double yaw = tf::getYaw(odom_pose.pose.orientation);
    ROS_INFO("Send pose x,y,z,yaw: %lf, %lf, %lf, %lf", cmd.pose.position.x, cmd.pose.position.y,
             10.0, yaw);
    // HACK FOR TEST
    controller_->commandLocalPositionENU(cmd.pose.position.x, cmd.pose.position.y, 10.0, yaw);
  }

  void pathCallback(const nav_msgs::Path &path_msg) {
    auto poses_array = path_msg.poses;

    WayPointTrajectoryTracker::Path path(poses_array.size());
    ROS_INFO("PATH RECIEVED !!!!!! size1: %d, size2: %d", (int) path.p.size(), (int) path.q.size());
    // Type conversion
    for (size_t i = 0; i < poses_array.size(); i++) {
      path.p[i] = Eigen::Vector3d(poses_array[i].pose.position.x, poses_array[i].pose.position.y,
                                  poses_array[i].pose.position.z);
      tf::quaternionMsgToEigen(poses_array[i].pose.orientation, path.q[i]);
    }

    waypts_tracker_->trackPath(path);
    path_recieved_ = true;



  }

  void tfPublisherThread() {
    ros::Rate tf_rate(tf_pub_rate_);
    //std::chrono::milliseconds period_millis((int)((1.0/tf_pub_rate_)*1000.0));;
    ROS_WARN("Start TF Publisher Thread");
    double pub_dur = 1.0 / tf_pub_rate_;
    while (ros::ok) {
      // Temporary borrow this thread to publish goal status
      if (path_recieved_) {
        if (waypts_tracker_->isGoalReached()) {
          ROS_INFO("GOAL REACHED ---- Ready for the next path");
          path_recieved_ = false;
        } else {
          // WayPointTrajectoryTracker::Pose p;
          Eigen::Vector3d pose;
          Eigen::Quaterniond q;
         
          if (waypts_tracker_->getCurrentSetPoint(pose, q)) {
            geometry_msgs::PoseStamped curr_waypts;
            curr_waypts.header.stamp = ros::Time::now();
            curr_waypts.header.frame_id = odom_frame_;
            tf::pointEigenToMsg(pose, curr_waypts.pose.position);
            tf::quaternionEigenToMsg(q, curr_waypts.pose.orientation);
            tracker_setpoint_pub_.publish(curr_waypts);
          }

        }
      }
      ros::Time start_time = ros::Time::now();
      auto odom = boost::make_shared<nav_msgs::Odometry>();
      odom->header.frame_id = odom_frame_;
      odom->child_frame_id = base_frame_;
      Eigen::Vector3d baselink_position, baselink_linear, baselink_angular;
      Eigen::Quaterniond baselink_q;
      {
        // TODO -- HACK TIME STAMP FOR NOW, fix later
        std::lock_guard<std::mutex> lock(tf_ned_mutex_);
        odom->header.stamp = tf_ned_stamp_;
      }

      controller_->getRobotFrameVelocity(baselink_linear, baselink_angular);
      bool st = controller_->getRobotFrameTransform(baselink_position, baselink_q);
      if (st) {
        tf::vectorEigenToMsg(baselink_linear, odom->twist.twist.linear);
        tf::vectorEigenToMsg(baselink_angular, odom->twist.twist.angular);
        tf::pointEigenToMsg(baselink_position, odom->pose.pose.position);
        tf::quaternionEigenToMsg(baselink_q, odom->pose.pose.orientation);

        for (int i = 0; i < 3; i++) {
          // linear velocity
          odom->twist.covariance[i + 6 * i] = 1e-4;
          // angular velocity
          odom->twist.covariance[(i + 3) + 6 * (i + 3)] = 1e-4;
          // position/ attitude
          if (i == 2) {
            // z
            odom->pose.covariance[i + 6 * i] = 1e-6;
            // yaw
            odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
          } else {
            // x, y
            odom->pose.covariance[i + 6 * i] = 1e-6;
            // roll, pitch
            odom->pose.covariance[(i + 3) + 6 * (i + 3)] = 1e-6;
          }
        }


        // ------ Publish TF and odometry msgs--------
        odom_pub_.publish(odom);
        tf_odom_to_base_.setOrigin(tf::Vector3(odom->pose.pose.position.x,
                                               odom->pose.pose.position.y, odom->pose.pose.position.z));
        tf_odom_to_base_.setRotation(tf::Quaternion(odom->pose.pose.orientation.x,
                                                    odom->pose.pose.orientation.y,
                                                    odom->pose.pose.orientation.z, odom->pose.pose.orientation.w));
        ros::Time tf_expiration = odom->header.stamp + ros::Duration(pub_dur);
        tf_bc_.sendTransform(tf::StampedTransform(tf_odom_to_base_, tf_expiration, odom_frame_, base_frame_));
      } else {
        //ROS_INFO("HELOO7");
        // MainTain TF
        ros::Time tf_expiration = ros::Time::now() + ros::Duration(pub_dur);
        tf_bc_.sendTransform(tf::StampedTransform(tf_odom_to_base_, tf_expiration, odom_frame_, base_frame_));
      }

      tf_rate.sleep();
      //std::this_thread::sleep_for(period_millis);
    }

  }

  bool srvTakeOff(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    ROS_WARN("Taking off!!");
    controller_->commandTakeOff();
    return true;
  }

  bool srvLanding(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    ROS_WARN("LANDING!!");
    controller_->commandLandAtCurrentLocation();
    return true;
  }

  bool srvStopCmd(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    ROS_WARN("STOP Commanding!!");
    controller_->stopCommand();
    return true;
  }

  bool srvStopTracking(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    ROS_WARN("STOP Trajectory Tracking");
    waypts_tracker_->stopTrackPath();
    return true;
  }

  bool srvToggleArm(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    if (controller_->isArmed()) {
      ROS_WARN("Disarming !");
      controller_->commandArm(false);
    } else {
      ROS_WARN("Arming !");
      controller_->commandArm(true);
    }
    return true;
  }

  bool srvReturnToLaunch(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    controller_->commandReturnToLaunch();
    return true;
  }

};

int main(int argc, char **argv) {
  ros::init(argc, argv, "mav_controller_test");
  MAVControllerTestNode node;
  ROS_INFO("********* Start MAV Controller Test Node ********");
  node.executeNode();
  return 0;
}
