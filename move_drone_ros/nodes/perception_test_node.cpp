//
// Created by kandithws on 2/4/2561.
//
#include <move_drone/utils/eigen_disable_assert_odroid.h>
#include <ros/ros.h>
#include <chrono>
#include <move_drone/CameraManager.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <move_drone/AprilTagObstacleDetector.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>

using namespace move_drone;

static inline ros::Time std_to_ros_time(std::chrono::nanoseconds std_t){
  ros::Time t;
  return t.fromNSec(static_cast<uint64_t >(std_t.count()));
}

class PerceptionTestNode{
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  PerceptionTestNode(): nh_("~"),
                        it_(nh_)
  {
    std::string cam_device, cam_params_file, obs_config_file;
    nh_.param<std::string>("camera_device", cam_device, "camera::/dev/video0");
    img_pub_ = it_.advertise("/camera/image", 1);
    marker_pub_ = nh_.advertise<visualization_msgs::Marker>("/obstacle_markers", 1);

    cam_manager_ = boost::make_shared<CameraManager>(cam_device, 2);


    if(nh_.hasParam("camera_config_file")){
      nh_.param<std::string>("camera_config_file", cam_params_file, "");
      cam_manager_->loadCameraInfo(cam_params_file);
    }
    else{
      ROS_WARN("No Camera Config Loaded!");
    }

    nh_.param<std::string>("obstacle_config_file", obs_config_file, "");
    obj_detector_ = boost::make_shared<AprilTagObstacleDetector>(obs_config_file);
    obj_detector_->setCameraParams(cam_manager_->camera_params_.getProjectedfx(),
                                   cam_manager_->camera_params_.getProjectedfy(),
                                   cam_manager_->camera_params_.getProjectedpx(),
                                   cam_manager_->camera_params_.getProjectedpy());

    cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/obstacle_cloud", 1);


  }

  void executeNode(){

    ros::Rate rate(30);
    cam_manager_->startStreaming();
    sensor_msgs::ImagePtr msg;
    std_msgs::Header header;
    //ros::Time last = ros::Time::now();
    while (ros::ok()){

      if(cam_manager_->getImage(current_img_ptr_)){
        header.stamp = std_to_ros_time(current_img_ptr_->stamp_);
        // ROS_INFO("STAMP: %lu", header.stamp.toNSec()  );
        // msg = cv_bridge::CvImage(header, "bgr8", current_img_ptr_->image_).toImageMsg();
        // img_pub_.publish(msg);
        //std::vector<AprilTagObstacle> dectected_obs;
        //obj_detector_->detect(current_img_ptr_->image_, dectected_obs, true);

        //msg = cv_bridge::CvImage(header, "bgr8", current_img_ptr_->image_).to



        /*
        for (auto &o : dectected_obs ){
          publishObstacleTransform(o, header.stamp);
        }
        */

        /*
        for (auto &o : dectected_obs ){
          //publishMarkerFromObstacle(o, header.stamp);
          publishObstacleTransform(o, header.stamp);

          if(o.tag_id_ == 22){ // select out

            publishCloud(o, header.stamp , single_cloud_pub_);
          }
        }
        */

        PointCloudT out_cloud;
        //ros::Time detect_start = ros::Time::now();
        obj_detector_->detect(current_img_ptr_->image_, out_cloud);
        //ros::Time now = ros::Time::now();
        //ROS_INFO("Detection Time: %lf", (now-detect_start).toSec() );
        //ROS_INFO("Total Time per frame: %lf", (now-last).toSec() );
        //ROS_INFO("------------------------------------------");
        //last = now;
        msg = cv_bridge::CvImage(header, "bgr8", current_img_ptr_->image_).toImageMsg();
        sensor_msgs::PointCloud2 cloud_msg;
        pcl::toROSMsg(out_cloud, cloud_msg);
        cloud_msg.header.frame_id = "/camera_optical_link";
        cloud_msg.header.stamp = header.stamp;
        cloud_pub_.publish(cloud_msg);

        img_pub_.publish(msg);
      }
      else{
        //ROS_INFO("----- SKIP FRAME ----");
      }
      ros::spinOnce();
      rate.sleep();
    }
    cam_manager_->stopStreaming();

  }

  void publishMarkerFromObstacle(AprilTagObstacle &obj, ros::Time stamp){
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/camera_optical_link";
    marker.header.stamp = stamp;
    marker.ns = "obstacle";
    marker.id = obj.tag_id_;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    // tf::Pose p;
    //tf::poseEigenToTF(obj.pose_, p);
    tf::poseEigenToMsg(obj.pose_, marker.pose);
    marker.scale.x = obj.size_x_;
    marker.scale.y = obj.size_y_;
    marker.scale.z = obj.size_z_;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();

    marker_pub_.publish(marker);

  }

  void publishObstacleTransform(AprilTagObstacle &obj, ros::Time stamp){
    tf::Transform transform;
    tf::transformEigenToTF(obj.pose_ ,transform);
    std::string frame("/obstacle_");
    tf_br_.sendTransform(tf::StampedTransform(transform, stamp, "/camera_optical_link", frame + std::to_string(obj.tag_id_)));
  }

  void publishCloud(AprilTagObstacle &obj, ros::Time stamp ,ros::Publisher& pub){
    sensor_msgs::PointCloud2 cloud_msg;
    PointCloudT out;
    obj_detector_->getObstaclesCacheCloud(obj.tag_id_, out);
    // pcl_conversions::t(cloud, cloud_msg);
    pcl::toROSMsg(out, cloud_msg);
    cloud_msg.header.frame_id = "/obstacle_" + std::to_string(obj.tag_id_);
    cloud_msg.header.stamp = stamp;
    //cloud_msg.
    pub.publish(cloud_msg);
  }





 private:
  boost::shared_ptr<ImageStamped> current_img_ptr_;
  boost::shared_ptr<CameraManager> cam_manager_;
  boost::shared_ptr<AprilTagObstacleDetector> obj_detector_;
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Publisher img_pub_;
  ros::Publisher marker_pub_;
  ros::Publisher cloud_pub_; // For debugging purpose only, fix tag id
  tf::TransformBroadcaster tf_br_;

};

int main(int argc, char **argv) {
  ros::init(argc, argv, "perception_test");
  PerceptionTestNode node;
  ROS_INFO("********* Start Perception Test Node ********");
  node.executeNode();
  return 0;
}
