//
// Created by kandithws on 25/7/2561.
//

#include <ros/ros.h>
#include <move_drone_ros/RosCameraManager.h>
//#include <move_drone/CameraManager.h>
#include <cv_bridge/cv_bridge.h>
#include <move_drone_ros/common_utils.h>
//#include <move_drone/BaseMAVDataInterface.h>
#include <boost/shared_ptr.hpp>

class RosPlaybackExampleNode {
 public:
  RosPlaybackExampleNode() : nh_("~") {
    cam_manager_ = boost::make_shared<move_drone::RosCameraManager>(nh_, 10);
    //cam_manager_ = boost::make_shared<move_drone::CameraManager>("camera::/dev/video1", 10);
    detected_pub_ = nh_.advertise<sensor_msgs::Image>("/detection_out", 1);
    cam_manager_->loadCameraInfo(
        "/home/kandithws/ait_workspace/catkin_ws/src/move_drone_ros_navigation/move_drone_ros/config/mobius_low_res.cv.yaml");
    cam_manager_->registerCallback(std::bind(&RosPlaybackExampleNode::imageCallback, this, std::placeholders::_1));
    cam_manager_->startStreaming();
    std::cout << cam_manager_->camera_params_.undistort << std::endl;
    std::cout << cam_manager_->camera_params_.K << std::endl;
//      ros::AsyncSpinner spinner(1);
//
//      spinner.start();
//      ros::Rate rate(20);
//      while (ros::ok()){
//        move_drone::ImageStamped::Ptr img_stamped;
//        if(cam_manager_->getImage(img_stamped)){
//          ROS_INFO("Hello World!");
//          //detected_pub_.publish();
//        }
//        ros::spinOnce();
//        rate.sleep();
//      }
//      spinner.stop();
    ros::spin();
    cam_manager_->stopStreaming();
  }

 private:
  ros::NodeHandle nh_;
  ros::Publisher detected_pub_;
  boost::shared_ptr<move_drone::BaseCameraManager> cam_manager_;

  void imageCallback(const move_drone::ImageStamped::ConstPtr &msg) {
    // Example, convert to gray scale here!
    //move_drone::ImageStamped img = *msg;
    //img.convertToEncoding(move_drone::ImageStamped::MONO8);
    std_msgs::Header header;
    header.frame_id = "camera_link";
    header.stamp = move_drone_ros::std_to_ros_time(msg->stamp_);
    //std::string encoding = img.getStringEncoding();
    cv_bridge::CvImage cv_img(header,msg->getStringEncoding(), msg->image_);

    sensor_msgs::Image ros_img_msg;
    cv_img.toImageMsg(ros_img_msg);
    detected_pub_.publish(ros_img_msg);
  }

};

int main(int argc, char **argv) {
  ros::init(argc, argv, "playback_example");
  RosPlaybackExampleNode node;
  return 0;
}

