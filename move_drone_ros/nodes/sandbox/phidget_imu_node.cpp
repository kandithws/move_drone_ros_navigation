//
// Created by kandithws on 11/9/2561.
//
#include <move_drone/external_imu/phidget/PhidgetIMU.h>
#include <move_drone_ros/common_utils.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>

class PhidgetIMUNode {
 public:
  PhidgetIMUNode(){
    imu_pub_ = nh_.advertise<sensor_msgs::Imu>("/imu/data",10);
    mag_pub_ = nh_.advertise<sensor_msgs::MagneticField>("/imu/mag",10);
    imu_conn_.setStreamingRate(4);
    imu_conn_.registerCallback(std::bind(&PhidgetIMUNode::imuDataCallback, this, std::placeholders::_1));
    imu_conn_.startStreaming();
    ros::spin();
    imu_conn_.stopStreaming();
  }

  void imuDataCallback(const move_drone::IMUData& msg){
    sensor_msgs::Imu imu_msg;
    imu_msg.header.stamp = move_drone_ros::std_to_ros_time(msg.stamp);
    imu_msg.header.frame_id = "/imu_link";

    imu_msg.linear_acceleration.x = msg.accel[0];
    imu_msg.linear_acceleration.y = msg.accel[1];
    imu_msg.linear_acceleration.z = msg.accel[2];

    imu_msg.angular_velocity.x = msg.gyro[0];
    imu_msg.angular_velocity.y = msg.gyro[1];
    imu_msg.angular_velocity.z = msg.gyro[2];

    sensor_msgs::MagneticField mag_msg;

    mag_msg.header = imu_msg.header;
    mag_msg.magnetic_field.x = msg.mag[0];
    mag_msg.magnetic_field.y = msg.mag[1];
    mag_msg.magnetic_field.z = msg.mag[2];

    imu_pub_.publish(imu_msg);
    mag_pub_.publish(mag_msg);

  }

 private:
  ros::NodeHandle nh_;
  ros::Publisher imu_pub_;
  ros::Publisher mag_pub_;
  move_drone::PhidgetIMU imu_conn_;

};

int main(int argc, char** argv){
  ros::init(argc,argv,"phidget_example");
  PhidgetIMUNode node;
  return 0;
}
