//
// Created by kandithws on 31/7/2561.
//

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <move_drone/external_imu/um6/UM6Driver.h>
#include <move_drone_ros/common_utils.h>
#include <boost/shared_ptr.hpp>

class UM6IMUROSNode{
 public:
  UM6IMUROSNode(): nh_("~")
  {
    nh_.param<std::string>("port", port_, "/dev/ttyUSB0");
    nh_.param("baud_rate", baud_rate_, 115200);
    nh_.param("callback_queue_size", queue_size_, 30);
    driver_ = boost::make_shared<move_drone::UM6Driver>(port_, baud_rate_, queue_size_);
    driver_->registerIMUCallback(std::bind(&UM6IMUROSNode::callBack, this, std::placeholders::_1));
    imu_pub_ = nh_.advertise<sensor_msgs::Imu>("/um6/imu", 10);
    driver_->setBoardcastRate(50);

  }

  void execute(){
    driver_->startStreaming(true);
    ros::spin();
    driver_->stopStreaming();
  }

  void callBack(move_drone::UM6Driver::IMUDataPtr msg){
    ROS_INFO("Receive IMU DATA CB");
    sensor_msgs::Imu imu_msg;
    imu_msg.header.stamp = move_drone_ros::std_to_ros_time(msg->stamp);
    imu_msg.orientation.x = msg->q[0];
    imu_msg.orientation.y = msg->q[1];
    imu_msg.orientation.z = msg->q[2];
    imu_msg.orientation.w = msg->q[3];

    imu_msg.orientation_covariance[0] = -1;
    imu_msg.linear_acceleration.x = msg->accel[0];
    imu_msg.linear_acceleration.y = msg->accel[1];
    imu_msg.linear_acceleration.z = msg->accel[2];

    imu_msg.angular_velocity.x = msg->gyro[0];
    imu_msg.angular_velocity.y = msg->gyro[1];
    imu_msg.angular_velocity.z = msg->gyro[2];
    imu_pub_.publish(imu_msg);
  }

 private:
  ros::NodeHandle nh_;
  ros::Publisher imu_pub_;
  std::string port_;
  int baud_rate_;
  int queue_size_;
  boost::shared_ptr<move_drone::UM6Driver> driver_;
};

int main(int argc, char** argv){
  ros::init(argc,argv,"um6_example");
  UM6IMUROSNode node;
  node.execute();
  return 0;
}