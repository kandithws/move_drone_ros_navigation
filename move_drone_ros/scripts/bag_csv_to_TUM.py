#!/usr/bin/env python2
import numpy as np
import sys
import os
from geometry_msgs.msg import Pose, PoseStamped, Point, PointStamped
from nav_msgs.msg import Odometry

# convert ROS's CSV to TUM (stamp tx ty tz qx qy qz qw)
class ROSCSVConverter:
    def __init__(self,msg_type, filename, outfile='out.txt'):
        self.ros_message_type = msg_type
        self.msg_extractors = {'Pose': None, 
                               'PoseStamped': None,
                                'Point': None,
                                'PointStamped': self.extract_point_stamped_msg,
                                'Odometry': self.extract_odom_msg,
                                'OdometryOffset': self.extract_odom_offset_msg
                                }
        if not self.ros_message_type in list(self.msg_extractors.keys()):
            print(self.ros_message_type + " not support, Abort!")
            exit(0)
        self.filename = filename
        
        if not os.path.isfile(self.filename):
            print(self.filename + " is not exist")
            exit(0)
        self.in_data = np.genfromtxt(self.filename, comments='%', delimiter=",")
        print("Extracting: " + self.ros_message_type + " messages")            
        self.out_data = None
        self.msg_extractors[self.ros_message_type]()
        assert(self.out_data.shape[1] == 8 and self.out_data.shape[0] == self.in_data.shape[0])
        np.savetxt(outfile, self.out_data, fmt=' '.join(['%.5f'] + ['%.7f']*7 ))
        print("Done, Saved to " + outfile) 
                
    
    def extract_point_stamped_msg(self):
        rows, cols = self.in_data.shape
        assert(cols == 7)
        stamps = self.in_data[:, 2].reshape((rows,1)) * 1e-9
        #point_x = self.in_data[:,4].reshape((rows,1)) 
        #point_y = self.in_data[:,5].reshape((rows,1)) 
        #point_z = self.in_data[:,6].reshape((rows,1)) 
        points = self.in_data[:, 4:7]
        qxyz = np.zeros((rows,3))
        qw = np.ones((rows,1))
        self.out_data = np.concatenate( (stamps, points, qxyz, qw), axis=1)
        
    def extract_odom_msg(self):
        rows, cols = self.in_data.shape
        assert(cols == 90)
        stamps = self.in_data[:, 2].reshape((rows,1)) * 1e-9
        dat = self.in_data[:, 5:12]
        self.out_data = np.concatenate((stamps, dat), axis=1) 
    
    def extract_odom_offset_msg(self):
        rows, cols = self.in_data.shape
        assert(cols == 90)
        stamps = self.in_data[:, 2].reshape((rows,1)) * 1e-9
        # do inefficient for now, offset only for position
        init_pos = np.tile(self.in_data[0, 5:8], (rows, 1))
        print(init_pos)
        dat = self.in_data[:, 5:12]
        dat[:, 0:3] = dat[:,0:3] - init_pos
        self.out_data = np.concatenate((stamps, dat), axis=1) 



if __name__ == '__main__':
    if len(sys.argv) == 3:
        ROSCSVConverter(sys.argv[1], sys.argv[2])
    elif len(sys.argv) > 3:
        ROSCSVConverter(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print "Usage: bag_csv_to_TUM.py ros_msg_type input_file [out_file]"
