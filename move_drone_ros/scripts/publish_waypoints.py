#!/usr/bin/env python
import rospy
import numpy as np
import rospkg
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler
from nav_msgs.msg import Path, Odometry


curr_odom = Odometry()
path_pub = None

def to_pose_msg(p_array):
    p = PoseStamped()
    p.header.frame_id = 'map_enu'
    p.header.stamp = rospy.Time.now()
    p.pose.position.x = p_array[0]
    p.pose.position.y = p_array[1]
    p.pose.position.z = p_array[2]
    q = quaternion_from_euler(0,0, p_array[3])
    p.pose.orientation.x = q[0]
    p.pose.orientation.y = q[1]
    p.pose.orientation.z = q[2]
    p.pose.orientation.w = q[3]
    return p


def generate_yaw_data(x_pts, y_pts):
    yaws = np.zeros(x_pts.shape[0])
    for i in range(0, x_pts.shape[0]-1):
        yaws[i] = np.arctan2( (y_pts[i+1] - y_pts[i]), (x_pts[i+1] - x_pts[i]) )
    return yaws


def generate_way_points(initial_x=0.0, initial_y=0.0):
    # Control at 4 Hz
    control_rate = 10.0 # grid resolution
    dist_x = 20.0
    x_pts = np.arange(0.0, dist_x, 1./control_rate, np.float32) + initial_x
    # depends on what u want
    y_pts = 10.0 * np.sin(2.0 * np.pi * 0.15 * x_pts ) + initial_y
    z_pts = np.ones((x_pts.shape[0])) * 5.0 # fix altitude
    yaw_pts = generate_yaw_data(x_pts, y_pts)
    return np.stack(( x_pts, y_pts, z_pts, yaw_pts), 1)


def odom_callback(msg):
    global curr_odom
    curr_odom = msg

def handle_generate_path(req):
    global curr_odom
    waypoints = generate_way_points(curr_odom.pose.pose.position.x, curr_odom.pose.pose.position.y)
    path = Path()
    path.header.frame_id = 'map_enu'
    path.header.stamp = rospy.Time.now()
    for i in range(waypoints.shape[0]):
        path.poses.append(to_pose_msg(waypoints[i,:]))
    rospy.loginfo('Total Trajectory Size: %d', waypoints.shape[0])
    rospy.loginfo('Publishing Trajectory ...')
    path_pub.publish(path)
    rospy.loginfo('Done!')
    return EmptyResponse()

def main():
    # load_from_file = True
    global curr_odom, path_pub
    rospy.init_node("path_publisher_node")
    path_pub = rospy.Publisher('/path', Path, queue_size=1, latch=True)
    sub = rospy.Subscriber('/odom', Odometry, odom_callback, queue_size=1)
    service = rospy.Service('/generate_path', Empty, handle_generate_path)
    rospy.loginfo('Start Path Publisher')
    # waypoints = None
    # if load_from_file:
    #     pkg_path = rospkg.RosPack().get_path('move_drone_ros')
    #     fpath = pkg_path + '/scripts/waypoints.csv'
    #     rospy.loginfo('Loading Waypoints from %s', fpath)
    #     waypoints = np.loadtxt(fpath, delimiter=',')
    # else:
    #     rospy.loginfo('Generating Waypoints')
    #     waypoints = generate_way_points()
    #
    # path = Path()
    # path.header.frame_id = 'map_enu'
    # path.header.stamp = rospy.Time.now()
    # for i in range(waypoints.shape[0]):
    #     path.poses.append(to_pose_msg(waypoints[i,:]))
    # rospy.loginfo('Total Trajectory Size: %d', waypoints.shape[0])
    # rospy.loginfo('Publishing Trajectory ...')
    # path_pub.publish(path)
    # rospy.loginfo('Done!')
    rospy.spin()




if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
