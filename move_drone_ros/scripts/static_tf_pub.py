#!/usr/bin/env python
import rospy
import sys
import numpy as np

#Because of transformations
import tf

import tf2_ros
import geometry_msgs.msg

# note fixed for mat as plain txtfile
# 1,2,3,4
# 5,6,7,8 
# 
def get_tf_mat_from_file(fname, inverse=False):
    tf_mat = np.matrix(np.loadtxt(fname, delimiter=','))
    assert(tf_mat.shape[0] == 4 and tf_mat.shape[1] == 4)
    if inverse:
        print('Warning: Inverse TF matrix')
        tf_mat = np.linalg.inv(tf_mat)
    _, _, angles, t, _ = tf.transformations.decompose_matrix(tf_mat) 
    return t, tf.transformations.quaternion_from_euler(*angles)

if __name__ == '__main__':
    if (len(sys.argv) < 9 and (len(sys.argv) != 5 and len(sys.argv) != 6) ) or ( (len(sys.argv) == 5 or len(sys.argv) == 6) and sys.argv[3] != '--matfile') :
        print('Invalid number of parameters\nusage: ./static_turtle_tf2_broadcaster.py frame_id child_frame_id x y z yaw pitch roll')
        print('or ./static_turtle_tf2_broadcaster.py frame_id child_frame_id --matfile tf_mat_file_name [--inverse]')
        sys.exit(0)
    else: 
        if sys.argv[1] == sys.argv[2]:
            print('Your frame_id should not be the same as child_frame_id')
            sys.exit(0)
    
    rospy.init_node('my_static_tf2_broadcaster')
    broadcaster = tf2_ros.StaticTransformBroadcaster()
    static_transformStamped = geometry_msgs.msg.TransformStamped()
    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = sys.argv[1]
    static_transformStamped.child_frame_id = sys.argv[2]

    trans = None
    quat = None
    if sys.argv[3] == '--matfile':
        if len(sys.argv) == 6 and sys.argv[5] == '--inverse':
            trans, quat = get_tf_mat_from_file(sys.argv[4], True) 
        else:
            trans, quat = get_tf_mat_from_file(sys.argv[4]) 

    else:
        trans = (float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]) )
        quat = tf.transformations.quaternion_from_euler(float(sys.argv[8]),float(sys.argv[7]),float(sys.argv[6]))

    static_transformStamped.transform.translation.x = trans[0]
    static_transformStamped.transform.translation.y = trans[1]
    static_transformStamped.transform.translation.z = trans[2]
    static_transformStamped.transform.rotation.x = quat[0]
    static_transformStamped.transform.rotation.y = quat[1]
    static_transformStamped.transform.rotation.z = quat[2]
    static_transformStamped.transform.rotation.w = quat[3]
    print("------ Publishing TF Info from %s to %s ------" % (sys.argv[1], sys.argv[2]))
    tf_ros = tf.TransformerROS()
    tf_mat = tf_ros.fromTranslationRotation(trans, quat)
    print("TF Matrix: ")
    print(tf_mat)
    print("TF Inverse: ")
    print(np.linalg.inv(tf_mat))
    _, _, angles, t, _ = tf.transformations.decompose_matrix(tf_mat) 
    R = tf.transformations.euler_matrix(*angles)[:3, :3]
    print("----")
    print("R: ")
    print(R)
    print("----")
    print("t (x,y,z): ")
    print(t)
    print("q: " + str(quat))
    print("euler RPY: %lf, %lf, %lf"  % (angles[0], angles[1], angles[2]) )
    angle_degs = np.rad2deg(angles)
    print("euler RPY Degree: %lf, %lf, %lf"  % (angle_degs[0], angle_degs[1], angle_degs[2]) )
    broadcaster.sendTransform(static_transformStamped)
    print("Done!")
    rospy.spin()



