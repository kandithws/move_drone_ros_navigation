#!/usr/bin/env python
import rospy


if __name__ == '__main__':
    rospy.init_node("path_publisher_node")

    while not rospy.is_shutdown():
        rospy.loginfo('Hello World!: ' + str(rospy.Time.now()))
        rospy.sleep(1)


