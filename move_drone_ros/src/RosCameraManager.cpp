//
// Created by kandithws on 25/7/2561.
//

#include <move_drone_ros/RosCameraManager.h>


namespace move_drone {

RosCameraManager::RosCameraManager(ros::NodeHandle& nh,
                                   size_t queue_size,
                                   bool is_compressed, std::string topic) :
     nh_(nh), BaseCameraManager(queue_size)
{

  is_compressed_ = is_compressed;
  ros_queue_size_ = queue_size;
  image_topic_ = topic;
  // initailize ros stuff
}


void RosCameraManager::initStartStreaming() {
  ROS_ASSERT_MSG(!camera_info_via_ros_, "Camera Info Via ROS is not yet implemented");

  if (is_compressed_){
    // use rosqueue == camera manager queue size
    img_sub_ = nh_.subscribe(image_topic_, ros_queue_size_, &RosCameraManager::imageCompressedCallback, this);
  }
  else{
    img_sub_ = nh_.subscribe(image_topic_, ros_queue_size_, &RosCameraManager::imageCallback, this);
  }
}


void RosCameraManager::initStopStreaming() {
  boost::mutex::scoped_lock lock(img_queue_mutex_);
  img_sub_.shutdown();
}

void RosCameraManager::setCameraInfoTopic(std::string topic) {
  camera_info_topic_ = topic;
  camera_info_via_ros_ = true;
}

void RosCameraManager::loadCameraInfo(std::string filename) {
  std::cout << filename << std::endl;
  camera_params_ = CameraInfo(filename);
  image_width_ = camera_params_.width;
  image_height_ = camera_params_.height;
  target_encoding_ = ImageStamped::encodingFromString(camera_params_.encoding);
  camera_info_via_ros_ = false;
}

void RosCameraManager::imageCallback(const sensor_msgs::ImagePtr &msg) {

  cv_bridge::CvImagePtr img_tmp = cv_bridge::toCvCopy(msg, msg->encoding);

  ImageStamped::Ptr frame_stamped =
      boost::make_shared<ImageStamped>(img_tmp->image,
                                       move_drone_ros::ros_time_to_std(msg->header.stamp),
                                       ImageStamped::encodingFromString(msg->encoding));
  pushImageQueue(frame_stamped);
}

void RosCameraManager::imageCompressedCallback(const sensor_msgs::CompressedImagePtr &msg) {
  // ToDo : Recheck this flow, might be incorrect
  cv_bridge::CvImagePtr img_tmp = cv_bridge::toCvCopy(msg, camera_params_.encoding);
  //msg->data
  ImageStamped::Ptr frame_stamped =
      boost::make_shared<ImageStamped>(img_tmp->image,
                                       move_drone_ros::ros_time_to_std(msg->header.stamp),
                                       ImageStamped::encodingFromString(camera_params_.encoding));
  pushImageQueue(frame_stamped);
}


}